<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Status
 *
 * @ORM\Table(name="status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatusRepository")
 */
class Status
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, unique=true)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="token_expiration", type="datetime")
     */
    private $tokenExpiration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="product_sync_date", type="datetime", nullable=true)
     */
    private $productSyncDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $expiration = new \DateTime('now');
        $expiration->modify('+2 days');
        $this->tokenExpiration = $expiration;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Status
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set tokenExpiration
     *
     * @param \DateTime $tokenExpiration
     *
     * @return Status
     */
    public function setTokenExpiration($tokenExpiration)
    {
        $this->tokenExpiration = $tokenExpiration;

        return $this;
    }

    /**
     * Get tokenExpiration
     *
     * @return \DateTime
     */
    public function getTokenExpiration()
    {
        return $this->tokenExpiration;
    }

    /**
     * Set productSyncDate
     *
     * @param \DateTime $productSyncDate
     *
     * @return Status
     */
    public function setProductSyncDate($productSyncDate)
    {
        $this->productSyncDate = $productSyncDate;

        return $this;
    }

    /**
     * Get productSyncDate
     *
     * @return \DateTime
     */
    public function getProductSyncDate()
    {
        return $this->productSyncDate;
    }
}
