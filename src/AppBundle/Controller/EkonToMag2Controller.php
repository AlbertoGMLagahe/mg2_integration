<?php

namespace AppBundle\Controller;

use AppBundle\Shared\Shared;
use Doctrine\DBAL\DBALException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\ClientException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EkonToMag2Controller
 * @package AppBundle\Controller
 */
class EkonToMag2Controller extends Controller
{
    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/ekon/sync-categories")
     */
    public function syncCategoriesAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'ekon') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Categories could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'erp_categories_data' => false,
                    'mg2_categories' => false,
                    'message' => 'Categories could not be synchronized')
            );
        } else {
            $db = $this->get('doctrine.dbal.integration_connection');
            $client = new Client(['base_uri' => $uri]);

            /**
             * DELETE CATEGORIES **********************************************
             */

            // Get ERP categories to delete
            $query = "SELECT
                        sc.id AS id,
                        sc.magento_categoria_id AS mag_id,
                        sc.xcategoria_id AS erp_id,
                        sc.name AS name,
                        mc.xcategoria_id AS erp_real_id
                      FROM sync_categorias sc
                      LEFT JOIN mag_categorias mc ON sc.xcategoria_id = mc.xcategoria_id;";
            $erp_delete_categories = $db->fetchAll($query);

            $ddl_query = "";
            $params = [];
            $delete_promises = [];
            foreach ($erp_delete_categories as $erp_delete_category) {
                if (($erp_delete_category['erp_id'] !== null) && ($erp_delete_category['erp_real_id'] === null)) {
                    // Remove category from Magento
                    $delete_promises[strval($erp_delete_category['mag_id'])] = $client->deleteAsync('categories/' . $erp_delete_category['mag_id'], [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ]
                    ]);
                }
            }

            // Send delete promises
            $delete_results = Promise\settle($delete_promises)->wait();

            foreach ($delete_results as $mag_id => $delete_response) {
                if ($delete_response->getStatusCode() === 200) {
                    // Remove category from sync_categorias
                    $ddl_query .= "DELETE FROM sync_categorias WHERE magento_categoria_id = ?";
                    $params[] = $mag_id;
                }
            }

            // Execute SQL delete
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'erp_categories_data' => [],
                            'mg2_categories' => [],
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Get current Magento categories
            $api_response = $client->request('GET', 'categories', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_categories = json_decode($api_response->getBody()->getContents(), true);
            $root_id_category = $api_categories['id'];

            /**
             * NEW MAIN CATEGORIES **********************************************
             *
             * Category "Para El Motorista" ID is 3 and "Para Tu Moto" ID is 4
             * Field xgrupo (INT) values 1 – "Para tu moto" and 2 – "Para el motorista"
             *
             */

            $para_el_motorista_id = 3;
            $para_tu_moto_id = 4;

            // Get ERP categories to sync
            $query = "SELECT
                        mc.xcategoria_id AS xcategoria_id,
                        mc.xdescripcion AS xdescripcion,
                        mc.xcat_padre AS xcat_padre,
                        mc.xgrupo AS xgrupo,
                        sc.xcategoria_id AS erp_id
                      FROM mag_categorias mc
                      LEFT JOIN sync_categorias sc ON mc.xcategoria_id = sc.xcategoria_id;";
            $erp_categories = $db->fetchAll($query);

            // Loop categories from ERP
            $post_promises = [];
            foreach ($erp_categories as $erp_category) {
                // Create new category in Magento
                if (
                    ($erp_category['erp_id'] === null) &&
                    ($erp_category['xcat_padre'] === null) &&
                    ($erp_category['xgrupo'] !== null)
                ) {

                    $main_parent_id = $root_id_category;
                    if (intval($erp_category['xgrupo']) === 1) {
                        $main_parent_id = $para_tu_moto_id;
                    }
                    elseif (intval($erp_category['xgrupo']) === 2) {
                        $main_parent_id = $para_el_motorista_id;
                    }

                    $post_promises[$erp_category['xcategoria_id']] = $client->postAsync('categories', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'category' => [
                                'parent_id' => $main_parent_id,
                                'name' => $erp_category['xdescripcion'],
                                'is_active' => true,
                                'custom_attributes' => [
                                    [
                                        'attribute_code' => 'grid_activate',
                                        'value' => 0
                                    ]
                                ]
                            ],
                            'saveOptions' => true
                        ]
                    ]);
                }
            }

            // Send post promises
            $post_results = Promise\settle($post_promises)->wait();

            foreach ($post_results as $xcategoria_id => $post_response) {
                if ($post_response['state'] === "fulfilled") {
                    $result = json_decode($post_response['value']->getBody()->getContents());

                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_categorias (name, is_active, magento_parent_id, descripcion, xcategoria_id, magento_categoria_id, magento_categoria_id_outlet) 
                                        VALUES (?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $result->name,
                        true,
                        $result->parent_id,
                        $result->name,
                        $xcategoria_id,
                        $result->id,
                        0);
                } else {
                    $errors['ERROR creating category with ID ' . $xcategoria_id] = $post_response['reason']->getMessage();
                }
            }

            // Execute SQL delete
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'erp_categories_data' => $erp_categories,
                            'mg2_categories' => $api_categories,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            /**
             * NEW SUBCATEGORIES *********************************************
             */
            $finished = false;
            do {
                // Get ERP categories to sync
                $query = "SELECT
                            mc.xcategoria_id AS xcategoria_id,
                            mc.xdescripcion AS xdescripcion,
                            mc.xcat_padre AS xcat_padre,
                            sc.xcategoria_id AS erp_id,
                            sc.magento_categoria_id AS mag_id
                          FROM mag_categorias mc
                          LEFT JOIN sync_categorias sc ON mc.xcategoria_id = sc.xcategoria_id;";
                $erp_categories = $db->fetchAll($query);

                // Loop categories from ERP
                $post_promises = [];
                $parent_not_found = 0;
                foreach ($erp_categories as $erp_category) {
                    // Create new category in Magento
                    if (
                        ($erp_category['erp_id'] === null) &&
                        ($erp_category['xgrupo'] === null) &&
                        ($erp_category['xcat_padre'] !== null)
                    ) {
                        /**
                         * We suppose that all the categories are already in Magento when we find a category
                         * with a parent.
                         */
                        $mag_parent_id = null;
                        foreach ($erp_categories as $erp_parent_category) {
                            if ($erp_category['xcat_padre'] === $erp_parent_category['xcategoria_id']) {
                                $mag_parent_id = $erp_parent_category['mag_id'];
                                break;
                            }
                        }

                        if ($mag_parent_id === null) {
                            $parent_not_found++;
                        } else {
                            $post_promises[$erp_category['xcategoria_id']] = $client->postAsync('categories', [
                                'headers' => [
                                    'Authorization' => 'Bearer ' . $status->getToken()
                                ],
                                'json' => [
                                    'category' => [
                                        'parent_id' => ($mag_parent_id !== null) ?
                                            $mag_parent_id :
                                            $root_id_category,
                                        'name' => $erp_category['xdescripcion'],
                                        'is_active' => true,
                                        'custom_attributes' => [
                                            [
                                                'attribute_code' => 'grid_activate',
                                                'value' => 0
                                            ]
                                        ]
                                    ],
                                    'saveOptions' => true
                                ]
                            ]);
                        }
                    }
                }

                // Send post promises
                $post_results = Promise\settle($post_promises)->wait();

                foreach ($post_results as $xcategoria_id => $post_response) {
                    if ($post_response['state'] === "fulfilled") {
                        $result = json_decode($post_response['value']->getBody()->getContents());
                        // Add insert query and params
                        $ddl_query .= "INSERT INTO sync_categorias (name, is_active, magento_parent_id, descripcion, xcategoria_id, magento_categoria_id, magento_categoria_id_outlet) 
                                        VALUES (?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $result->name,
                            true,
                            $result->parent_id,
                            $result->name,
                            $xcategoria_id,
                            $result->id,
                            0);
                    } else {
                        $errors['ERROR creating category with ID ' . $xcategoria_id] = $post_response['reason']->getMessage();
                    }
                }

                // Execute SQL delete
                if ($ddl_query !== "") {
                    try {
                        $db->executeQuery($ddl_query, $params);
                    } catch (DBALException $exception) {
                        $response->setData(array(
                                'success' => false,
                                'errors' => $errors,
                                'erp_categories_data' => $erp_categories,
                                'mg2_categories' => $api_categories,
                                'message' => $exception->getMessage())
                        );

                        return $response;
                    }
                }

                // Exit do ... while if parent not found is 0
                if ($parent_not_found === 0) {
                    $finished = true;
                }
            } while (!$finished);

            /**
             * UPDATE CATEGORIES **********************************************
             */

            // Get ERP categories to sync
            $ddl_query = "";
            $params = [];
            $query = "SELECT
                        mc.xcategoria_id AS xcategoria_id,
                        mc.xdescripcion AS xdescripcion,
                        mc.xcat_padre AS xcat_padre,
                        mc.xgrupo AS xgrupo,
                        sc.xcategoria_id AS erp_id,
                        sc.magento_categoria_id AS mag_id
                      FROM mag_categorias mc
                      LEFT JOIN sync_categorias sc ON mc.xcategoria_id = sc.xcategoria_id;";
            $erp_categories = $db->fetchAll($query);

            // Loop categories from ERP
            $update_promises = [];
            $mag_parent_id = null;
            foreach ($erp_categories as $erp_category) {
                if ($erp_category['xcategoria_id'] === $erp_category['erp_id']) {
                    // Get parent id
                    if ($erp_category['xcat_padre'] !== null) {
                        foreach ($erp_categories as $erp_parent_category) {
                            if ($erp_category['xcat_padre'] === $erp_parent_category['xcategoria_id']) {
                                $mag_parent_id = intval($erp_parent_category['mag_id']);
                                break;
                            }
                        }
                    } else {
                        $mag_parent_id = $root_id_category;
                        if (intval($erp_category['xgrupo']) === 1) {
                            $mag_parent_id = $para_tu_moto_id;
                        }
                        elseif (intval($erp_category['xgrupo']) === 2) {
                            $mag_parent_id = $para_el_motorista_id;
                        }
                    }

                    // Recursive function to find and update changed categories
                    $api_categories_find = function ($api_categories, $erp_categories, $erp_category, $client, $status, $root_id_category, $mag_parent_id) use (&$api_categories_find, &$update_promises) {
                        foreach ($api_categories as $api_category) {
                            if (isset($api_category['children_data'])) {
                                if ((count($api_category['children_data']) > 0)) {
                                    $api_categories_find($api_category['children_data'], $erp_categories, $erp_category, $client, $status, $root_id_category, $mag_parent_id);
                                }

                                if ($api_category['id'] === intval($erp_category['mag_id'])) {

                                    // Check name change
                                    if ($api_category['name'] !== $erp_category['xdescripcion']) {
                                        // Update category in Magento
                                        $update_promises[$erp_category['erp_id'] . ':info'] = $client->postAsync('categories', [
                                            'headers' => [
                                                'Authorization' => 'Bearer ' . $status->getToken()
                                            ],
                                            'json' => [
                                                'category' => [
                                                    'id' => $erp_category['mag_id'],
                                                    'name' => $erp_category['xdescripcion'],
                                                    'is_active' => true,
                                                    'custom_attributes' => [
                                                        [
                                                            'attribute_code' => 'grid_activate',
                                                            'value' => 0
                                                        ]
                                                    ]
                                                ],
                                                'saveOptions' => true
                                            ]
                                        ]);
                                    }

                                    // Check parent change
                                    if ($api_category['parent_id'] !== $mag_parent_id) {
                                        // Update category in Magento
                                        $update_promises[$erp_category['erp_id'] . ':parent_id:' . $mag_parent_id . ':' . $erp_category['mag_id']] = $client->putAsync('categories/' . $erp_category['mag_id'] . '/move', [
                                            'headers' => [
                                                'Authorization' => 'Bearer ' . $status->getToken()
                                            ],
                                            'json' => [
                                                'parentId' => $mag_parent_id,
                                                'afterId' => $erp_category['mag_id']
                                            ],
                                            'saveOptions' => true
                                        ]);
                                    }

                                    return true;
                                }
                            }
                        }

                        return false;
                    };

                    $api_categories_find($api_categories['children_data'], $erp_categories, $erp_category, $client, $status, $root_id_category, $mag_parent_id);
                }
            }

            // Send put promises
            $update_results = Promise\settle($update_promises)->wait();

            foreach ($update_results as $xcategoria_id_action => $update_response) {
                // Get xcategoria id and the update action
                $xcategoria_id_action_array = explode(':', $xcategoria_id_action);
                $xcategoria_id = $xcategoria_id_action_array[0];
                $update_reason = $xcategoria_id_action_array[1];

                if ($update_reason === 'parent_id') {
                    if ($update_response['state'] === "fulfilled") {
                        $new_parent_id = $xcategoria_id_action_array[2];
                        $mag_id = $xcategoria_id_action_array[3];

                        // Add insert query and params
                        $ddl_query .= "UPDATE sync_categorias SET magento_parent_id = ? WHERE magento_categoria_id = ?;";
                        array_push($params,
                            $new_parent_id,
                            $mag_id);
                    } else {
                        $errors['ERROR updating parent of category with ID ' . $xcategoria_id] = $update_response['reason']->getMessage();
                    }
                }

                if ($update_reason === 'info') {
                    if ($update_response['state'] === "fulfilled") {
                        $result = json_decode($update_response['value']->getBody()->getContents());
                        // Add insert query and params
                        $ddl_query .= "UPDATE sync_categorias 
                                        SET 
                                          name = ?, 
                                          is_active = ?,
                                          magento_parent_id = ?,
                                          descripcion = ?,
                                          xcategoria_id = ?,
                                          magento_categoria_id_outlet = ?
                                        WHERE
                                          magento_categoria_id = ?;";
                        array_push($params,
                            $result->name,
                            true,
                            $result->parent_id,
                            $result->name,
                            $xcategoria_id,
                            0,
                            $result->id);
                    } else {
                        $errors['ERROR updating info of category with ID ' . $xcategoria_id] = $update_response['reason']->getMessage();
                    }
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'erp_categories_data' => $erp_categories,
                            'mg2_categories' => $api_categories,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'erp_categories_data' => $erp_categories,
                    'mg2_categories' => $api_categories,
                    'message' => count($errors) === 0 ?
                        'Categories synchronized successfully' :
                        'Categories synchronized with errors')
            );
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/ekon/sync-attributes")
     */
    public function syncAttributesAction()
    {
        $time = time();
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'ekon') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Attributes could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'num_errors' => count($errors),
                    'new_brands' => 0,
                    'new_sizes' => 0,
                    'new_colors' => 0,
                    'message' => $errors[0],
                    'duration (s)' => time() - $time)
            );
        } else {
            // Database and http client
            $db = $this->get('doctrine.dbal.integration_connection');
            $client = new Client(['base_uri' => $uri]);

            // ********** Sync attributes ******************************************************************************

            $post_promises = [];
            $token = $status->getToken();

            // Get current Magento brands
            $api_response = $client->get('brands?searchCriteria=', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);
            $api_brand_options = json_decode($api_response->getBody()->getContents(), true)['items'];

            // Get ERP brands
            $query = "SELECT * FROM adv_marcas";
            $erp_brands = $db->fetchAll($query);

            // Get new brands
            $new_brands = [];
            foreach ($erp_brands as $erp_brand) {
                $found = false;

                // Find null or not applicable values
                Shared::fixNullAttributeValue($erp_brand['xdescripcion']);

                foreach ($api_brand_options as $option) {
                    if (mb_strtolower(trim($erp_brand['xdescripcion'])) === mb_strtolower(trim($option['name']))) {
                        $found = true;
                        break;
                    }
                }

                if (!$found && trim($erp_brand['xdescripcion'] !== '-')) {
                    $new_brand = trim($erp_brand['xdescripcion']);
                    $new_brands[] = $new_brand;
                    $post_promises['brand:'.$new_brand] = $client->postAsync('brands', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $token
                        ],
                        'json' => [
                            "brand" => [
                                "name" => $erp_brand['xdescripcion'],
                                "url_page" => Shared::getSlug($erp_brand['xdescripcion']),
                                "page_title" => $erp_brand['xdescripcion'],
                                "status" => 1
                            ]
                        ]
                    ]);
                }
            }

            // Get current Magento sizes
            $api_response = $client->get('products/attributes/talla/options', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);
            $api_size_options = json_decode($api_response->getBody()->getContents(), true);

            // Get ERP sizes
            $query = "SELECT * FROM adv_tallas";
            $erp_sizes = $db->fetchAll($query);

            // Get new sizes
            $new_sizes = [];
            foreach ($erp_sizes as $erp_size) {
                $found = false;

                // Find null or not applicable values
                Shared::fixNullAttributeValue($erp_size['xdescripcion']);

                foreach ($api_size_options as $option) {
                    if (mb_strtolower(trim($erp_size['xdescripcion'])) === mb_strtolower(trim($option['label']))) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $new_size = trim($erp_size['xdescripcion']);
                    $new_sizes[] = $new_size;
                    $post_promises['size:'.$new_size] = $client->postAsync('products/attributes/talla/options', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $token
                        ],
                        'json' => [
                            "option" => [
                                "label" => $new_size,
                                "is_default" => false
                            ]
                        ]
                    ]);
                }
            }

            // Get current Magento colors
            $api_response = $client->get('products/attributes/color/options', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);
            $api_color_options = json_decode($api_response->getBody()->getContents(), true);

            // Get ERP colors
            $query = "SELECT * FROM adv_color";
            $erp_colors = $db->fetchAll($query);

            // Get new colors
            $new_colors = [];
            foreach ($erp_colors as $erp_color) {
                $found = false;

                // Find null or not applicable values
                Shared::fixNullAttributeValue($erp_color['xdescripcion']);

                foreach ($api_color_options as $option) {
                    if (mb_strtolower(trim($erp_color['xdescripcion'])) === mb_strtolower(trim($option['label']))) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $new_color = trim($erp_color['xdescripcion']);
                    $new_colors[] = $new_color;
                    $post_promises['color:'.$new_color] = $client->postAsync('products/attributes/color/options', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $token
                        ],
                        'json' => [
                            "option" => [
                                "label" => $new_color,
                                "is_default" => false
                            ]
                        ]
                    ]);
                }
            }

            // Send post promises
            $post_results = Promise\settle($post_promises)->wait();

            foreach ($post_results as $attr_option => $post_response) {
                if ($post_response['state'] !== "fulfilled") {
                    $errors['ERROR adding attribute option ' . $attr_option] = $post_response['reason']->getMessage();
                }
            }

            $response->setData(array(
                    'success'    => count($errors) === 0 ? true : false,
                    'errors'     => $errors,
                    'num_errors' => count($errors),
                    'new_brands' => $new_brands,
                    'new_sizes'  => $new_sizes,
                    'new_colors' => $new_colors,
                    'message'    => 'Attributes synchronized succesfully',
                    'duration (s)' => time() - $time)
            );
        }

        return $response;
    }

    /**
     * @param \Swift_Mailer $mailer
     * @param string $mode
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/ekon/sync-products/{mode}")
     */
    public function syncProductsAction(\Swift_Mailer $mailer, $mode = 'normal')
    {

        $time = time();
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // Max num of transactions
        $max_transactions = $this->getParameter('max_transactions');
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'ekon') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Products could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'num_errors' => count($errors),
                    'new_products' => 0,
                    'updated_products' => 0,
                    'message' => $errors[0],
                    'duration (s)' => time() - $time)
            );
        } else {
            // Get last product sync date
            $last_products_sync_date = $status->getProductSyncDate();
            $last_products_sync_date = $last_products_sync_date->format('Y-m-d H:i:s');

            // Database and http client
            $db = $this->get('doctrine.dbal.integration_connection');
            $db_mg2 = $this->get('doctrine.dbal.mg2_connection');
            $client = new Client(['base_uri' => $uri]);

            // ********** Sync products ********************************************************************************

            // Get synced Magento brands
            $api_response = $client->get('products/attributes/brand_id', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_brand = json_decode($api_response->getBody()->getContents(), true);
            $api_brand_id = $api_brand['attribute_id'];
            $api_brand_options = $api_brand['options'];

            // Fix translated names
            foreach ($api_brand_options as &$option) {
                if ($option['label'] === "a la mañana") {
                    $option['label'] = "AM";
                }

                if ($option['label'] === "Sí") {
                    $option['label'] = "Yes";
                }
            }

            // Get synced Magento sizes
            $api_response = $client->request('GET', 'products/attributes/talla', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_size = json_decode($api_response->getBody()->getContents(), true);
            $api_size_id = $api_size['attribute_id'];
            $api_size_options = $api_size['options'];

            // Get synced Magento colors
            $api_response = $client->request('GET', 'products/attributes/color', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_color = json_decode($api_response->getBody()->getContents(), true);
            $api_color_id = $api_color['attribute_id'];
            $api_color_options = $api_color['options'];

            // Get synced Magento sex
            $api_response = $client->request('GET', 'products/attributes/sexo', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_sexo = json_decode($api_response->getBody()->getContents(), true);
            $api_sexo_id = $api_sexo['attribute_id'];
            $api_sexo_options = $api_sexo['options'];

            // Get synced Magento genders
            $api_response = $client->request('GET', 'products/attributes/genero', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_gender = json_decode($api_response->getBody()->getContents(), true);
            $api_gender_id = $api_gender['attribute_id'];
            $api_gender_options = $api_gender['options'];

            // Counters
            $new_count = 0;
            $updated_count = 0;

            // DDL query string and params array
            $ddl_query = "";
            $params = [];

            $product_ids_string = "";
            $product_ids_array = [];
            if ($mode !== 'massive') {
                // Get ERP product ids recently updated
                $query = "SELECT 
                        SUBSTR(valor,4) as articulo_id,
                        MAX(modified) as ultima_modificacion
                      FROM pl_operations 
                      WHERE 
                        tabla = 'mag_articulos' AND modified > '".$last_products_sync_date."'
                      GROUP BY valor;";
                $product_ids = $db->fetchAll($query);
                foreach ($product_ids as $product_id) {
                    $product_ids_array[] = $product_id['articulo_id'];
                }
                $product_ids_string = join("','", $product_ids_array);
            }

            // Get ERP products
            $query = "SELECT
                        ma.xarticulo_id AS art_id,
                        ma.xvisible_web AS visible,
                        sp.xarticulo_id AS sync_art_id,
                        sp.magento_producto_id AS mag_id,
                        sp.existencias AS mag_existencias,
                        sp.talla AS mag_talla,
                        sp.color AS mag_color,
                        sp.marca AS mag_marca,
                        sp.sexo AS mag_sexo_code, 
                        sp.category_ids AS mag_category_ids,
                        sp.genero_ids AS mag_gender_ids,
                        pa.xdescripcion AS descripcion,
                        pa.xproducto AS producto,
                        pa.axdescripcion_web AS nombre,
                        pa.axdesc_amp_web AS descripcion_larga,
                        col.xcolor_id AS color_id,
                        col.xdescripcion AS color,
                        talla.xtalla_id AS talla_id,
                        talla.xdescripcion AS talla,
                        marca.xmarca_id AS marca_id,
                        marca.xdescripcion AS marca,
                        pa.axexistencias_web AS tipo_stock,
                        pa.axsexo AS sexo_code,
                        pa.axhombre AS hombre,
                        pa.axmujer AS mujer,
                        pa.axunisex AS unisex,
                        pap.xprec_venta AS precio_venta,
                        pap.xprec_agrup AS precio_agrup,
                        pap.xprec_ttc AS precio_ttc,
                        palm.xexistencia AS total 
                      FROM mag_articulos ma 
                      /* LEFT JOIN mag_art_categorias mac ON ma.xarticulo_id = mac.xarticulo_id */
                      JOIN pl_articulos pa ON ma.xarticulo_id = pa.xarticulo_id
                      LEFT JOIN adv_color col ON pa.axcolor = col.xcolor_id
                      LEFT JOIN adv_tallas talla ON pa.axtalla = talla.xtalla_id
                      LEFT JOIN adv_marcas marca ON pa.axmarca = marca.xmarca_id
                      LEFT JOIN sync_productos sp ON ma.xarticulo_id = sp.xarticulo_id
                      LEFT JOIN pl_artprec pap ON ma.xarticulo_id = pap.xarticulo_id
                      LEFT JOIN adv_existalm2 palm ON ma.xarticulo_id = palm.xarticulo_id
                      ";

            switch ($mode) {
                case 'massive':
                    $query .= "WHERE pa.axvisible_web = -1;";
                    break;
                case 'normal':
                    $query .= "WHERE ma.xarticulo_id IN ('".$product_ids_string."');";
                    break;
                default:
                    $query .= "WHERE ma.xarticulo_id IN ('".$product_ids_string."');";
                    break;
            }
            $erp_products = $db->fetchAll($query);
            $num_erp_products = count($erp_products);

            // Get Magento 2 products
            $query = "SELECT * FROM catalog_product_flat_1 catalog 
                      JOIN cataloginventory_stock_item stock ON catalog.entity_id = stock.product_id
                      JOIN cataloginventory_stock_status status ON catalog.entity_id = status.product_id";
            $api_products = $db_mg2->fetchAll($query);
            $db_mg2->close();

            // Promises and results arrays
            $post_results = [];
            $update_results = [];
            $sku_list = [];
            $count = 0;

            // Create simple and virtual products //////////////////////////////////////////////////////////////////////
            $i = 0;
            foreach ($erp_products as &$erp_product) {
                // If xproduct is null skip this product
                if (!$erp_product['producto']) {
                    continue;
                }

                // Find null or not applicable values
                Shared::fixNullAttributeValue($erp_product['talla']);
                Shared::fixNullAttributeValue($erp_product['color']);

                // Get attributes values
                $color_value = Shared::getAttributeOptionsValue($api_color_options, $erp_product['color']);
                $size_value = Shared::getAttributeOptionsValue($api_size_options, $erp_product['talla']);
                $brand_value = Shared::getAttributeOptionsValue($api_brand_options, $erp_product['marca']);

                $gender_ids_array = Shared::getProductGenderIds($api_gender_options, $erp_product);
                $gender_ids = join(',',$gender_ids_array);

                $sex_value = 0;
                $sexo_code = intval($erp_product['sexo_code']);
                switch ($sexo_code) {
                    case 1: // Unisex
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Unisex') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    case 2: // Lady
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Lady') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    case 3: // Kid
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Kid') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    case 4: // Hombre
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Hombre') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    default:
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Unisex') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                }

                // Get new SKU for simple products and products without size
                $new_sku = $erp_product['talla'] === '-' ?
                    $erp_product['producto'].'-'.$erp_product['color_id'].'-SKU-'.$erp_product['art_id'] :
                    $erp_product['producto'].'-'.$erp_product['color_id'].'-SKU-'.$erp_product['art_id'].'-'.$erp_product['talla'];

                // Check product is not already registered in Magento
                $product_magento = Shared::getProductBySku($api_products, $new_sku);

                // Get product categories from ERP
                $category_ids_array = Shared::getProductCategories($db, $erp_product['art_id']);
                $category_ids = join(',', $category_ids_array);
                $db->close();

                // Get stock, price, name and description
                $stock = (intval($erp_product["total"]) >= 0) ? intval($erp_product["total"]) : 0;
                $price = isset($erp_product['precio_ttc']) ? floatval($erp_product['precio_ttc']) : 0.0;
                $name = $erp_product['nombre'] ? $erp_product['nombre'] : $erp_product['descripcion'];
                $description = $erp_product['descripcion_larga'] ? $erp_product['descripcion_larga'] : $name;

                if (
                    $erp_product['mag_id'] === null &&
                    $erp_product['visible'] === '-1' &&
                    !$product_magento
                ) {
                    $options = [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'product' => [
                                // Add product field data.
                                'sku' => $new_sku,
                                "name" => $name,
                                "price" => $price,
                                "status" => 1,
                                "visibility"        => (
                                    intval($erp_product['visible']) === -1 &&
                                    $erp_product['talla'] === '-'
                                ) ? 4 : 1,
                                "type_id" => ($erp_product['talla'] === '-') ? "simple" : "virtual",
                                "attribute_set_id" => 4,           // Default Attribute Set is ID 4
                                "weight" => 0,                     // Not defined anywhere
                                "extension_attributes" => [
                                    "stock_item" => [
                                        "manage_stock" => true,
                                        "qty" => $stock,
                                        "is_in_stock" => true,
                                        "use_config_backorders" => true
                                    ]
                                ],
                                "custom_attributes" => ($erp_product['talla'] === '-') ? [
                                    [
                                        'attribute_code' => 'description',
                                        'value' => $description
                                    ],
                                    [
                                        "attribute_code" => "category_ids",
                                        "value" => $category_ids_array
                                    ],
                                    [
                                        "attribute_code" => "url_key",
                                        "value" => $erp_product['talla'] === '-' ?
                                            Shared::getSlug($name . '-' . $erp_product['color_id']) :
                                            Shared::getSlug($name . '-' . $erp_product['color_id'] . '-' . $erp_product['talla'])
                                    ],
                                    [
                                        "attribute_code" => "color",
                                        "value" => $color_value
                                    ],
                                    [
                                        "attribute_code" => "talla",
                                        "value" => $size_value
                                    ],
                                    [
                                        "attribute_code" => "brand_id",
                                        "value" => $brand_value
                                    ],
                                    [
                                        "attribute_code" => "sexo",
                                        "value" => $sex_value
                                    ],
                                    [
                                        "attribute_code" => "genero",
                                        "value" => $gender_ids_array
                                    ],
                                    [
                                        "attribute_code" => "aw_arp_override_native",
                                        "value" => 0
                                    ]
                                ] : [
                                    [
                                        'attribute_code' => 'description',
                                        'value' => $description
                                    ],
                                    [
                                        "attribute_code" => "category_ids",
                                        "value" => $category_ids_array
                                    ],
                                    [
                                        "attribute_code" => "url_key",
                                        "value" => $erp_product['talla'] === '-' ?
                                            Shared::getSlug($name . '-' . $erp_product['color_id']) :
                                            Shared::getSlug($name . '-' . $erp_product['color_id'] . '-' . $erp_product['talla'])
                                    ],
                                    [
                                        "attribute_code" => "color",
                                        "value" => $color_value
                                    ],
                                    [
                                        "attribute_code" => "talla",
                                        "value" => $size_value
                                    ],
                                    [
                                        "attribute_code" => "brand_id",
                                        "value" => $brand_value
                                    ],
                                    [
                                        "attribute_code" => "sexo",
                                        "value" => $sex_value
                                    ],
                                    [
                                        "attribute_code" => "genero",
                                        "value" => $gender_ids_array
                                    ]
                                ]
                            ],
                            'saveOptions' => true
                        ]
                    ];

                    // Send POST request to create simple/virtual product
                    try {
                        $post_results[$new_sku] = $client->post('products', $options);
                        $sku_list[] = $new_sku;
                        $count++;
                    } catch (ClientException $e) {
                        $error_response = $e->getResponse();
                        $errors['ERROR creating product with SKU ' . $new_sku] = $error_response->getBody()->getContents();
                    }

                } elseif (
                    $erp_product['mag_id'] === null &&
                    $product_magento &&
                    $product_magento['type_id'] !== 'configurable'
                ) {
                    $sku_list[] = $product_magento['sku'];
                    // Add pending synced products to database
                    $keys = explode('-', $product_magento['sku']);
                    $product_stock = isset($product_magento['qty']) ?
                        intval($product_magento['qty']) :
                        0;
                    $product_is_in_stock = isset($product_magento['is_in_stock']) ?
                        intval($product_magento['is_in_stock']) :
                        0;

                    $ddl_query .= "INSERT INTO sync_productos (name,short_description,weight,sku,sync_iva_id,existencias,status,visibility,is_in_stock,magento_producto_id,price,cost,descripcion,xarticulo_id,nuevo,promocion,producto_web,ficha_tecnica,category_ids,talla,color,marca,sexo,genero_ids) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $product_magento['name'],                                                   // name
                        $product_magento['name'],                                                   // short_description
                        0,                                                                          // weight
                        $product_magento['sku'],                                                    // sku
                        0,                                                                          // sync_iva_id
                        $product_stock,                                                             // existencias
                        1,                                                                          // status
                        $product_magento['visibility'],                                             // visibility
                        $product_is_in_stock,                                                       // is_in_stock
                        $product_magento['entity_id'],                                              // magento_producto_id
                        $product_magento['price'],                                                  // price
                        0,                                                                          // cost
                        "",                                                                         // descripcion
                        $keys[3],                                                                   // xarticulo_id
                        0,                                                                          // nuevo
                        0,                                                                          // promocion
                        0,                                                                          // producto_web
                        0,                                                                          // ficha_tecnica
                        $category_ids,                                                              // category_ids
                        $erp_product['talla'],                                                      // talla
                        $erp_product['color'],                                                      // color
                        $erp_product['marca'],                                                      // marca
                        $erp_product['sexo_code'],                                                  // sexo
                        $gender_ids                                                                 // genero_ids
                    );

                    // Increment transactions counter
                    $count++;
                } elseif (
                    $erp_product['mag_id'] !== null &&
                    $product_magento
                ) {
                    // Get product category ids from Magento
                    $mag_category_ids = explode(',', $erp_product['mag_category_ids']);

                    // Get product gender ids from Magento
                    $mag_gender_ids = explode(',', $erp_product['mag_gender_ids']);

                    // Get visibility value from ERP product
                    $visibility = (
                        intval($erp_product['visible']) === -1 &&
                        $erp_product['talla'] === '-'
                    ) ? 4 : 1;

                    // Check if product info has changed
                    if (
                        $name != $product_magento['name'] ||
                        $product_magento['description'] != $erp_product['descripcion_larga'] ||
                        $stock != intval($erp_product['mag_existencias']) ||
                        $visibility != $product_magento['visibility'] ||
                        $price != $product_magento['price'] ||
                        $mag_category_ids != $category_ids_array ||
                        $erp_product['talla'] != $erp_product['mag_talla'] ||
                        $erp_product['color'] != $erp_product['mag_color'] ||
                        mb_strtolower($erp_product['marca']) != mb_strtolower($erp_product['mag_marca']) ||
                        $erp_product['sexo_code'] != $erp_product['mag_sexo_code'] ||
                        $mag_gender_ids != $gender_ids_array
                    ) {
                        // Create PUT request
                        $options = [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $status->getToken()
                            ],
                            'json' => [
                                'product' => [
                                    // Add product field data.
                                    "id"                => $product_magento['entity_id'],
                                    "sku"               => $new_sku,    // Update SKU
                                    "name"              => $name,
                                    "price"             => $price,
                                    "status"            => 1,
                                    "visibility"        => (
                                        intval($erp_product['visible']) === -1 &&
                                        $erp_product['talla'] === '-'
                                    ) ? 4 : 1,
                                    "type_id"           => ($erp_product['talla'] === '-') ? "simple" : "virtual",
                                    "attribute_set_id"  => 4,           // Default Attribute Set is ID 4
                                    "weight"            => 0,           // Not defined anywhere
                                    "extension_attributes" => [
                                        "stock_item" => [
                                            "manage_stock" => true,
                                            "qty" => $stock,
                                            "is_in_stock" => true,
                                            "use_config_backorders" => true
                                        ]
                                    ],
                                    "custom_attributes" => ($erp_product['talla'] === '-') ? [
                                        [
                                            'attribute_code' => 'description',
                                            'value' => $description
                                        ],
                                        [
                                            "attribute_code" => "category_ids",
                                            "value" => $category_ids_array
                                        ],
                                        [
                                            "attribute_code" => "url_key",
                                            "value" => $erp_product['talla'] === '-' ?
                                                Shared::getSlug($name . '-' . $erp_product['color_id']) :
                                                Shared::getSlug($name . '-' . $erp_product['color_id'] . '-' . $erp_product['talla'])
                                        ],
                                        [
                                            "attribute_code" => "color",
                                            "value" => $color_value
                                        ],
                                        [
                                            "attribute_code" => "talla",
                                            "value" => $size_value
                                        ],
                                        [
                                            "attribute_code" => "brand_id",
                                            "value" => $brand_value
                                        ],
                                        [
                                            "attribute_code" => "sexo",
                                            "value" => $sex_value
                                        ],
                                        [
                                            "attribute_code" => "genero",
                                            "value" => $gender_ids_array
                                        ],
                                        [
                                            "attribute_code" => "aw_arp_override_native",
                                            "value" => 0
                                        ]
                                    ] : [
                                        [
                                            'attribute_code' => 'description',
                                            'value' => $description
                                        ],
                                        [
                                            "attribute_code" => "category_ids",
                                            "value" => $category_ids_array
                                        ],
                                        [
                                            "attribute_code" => "url_key",
                                            "value" => $erp_product['talla'] === '-' ?
                                                Shared::getSlug($name . '-' . $erp_product['color_id']) :
                                                Shared::getSlug($name . '-' . $erp_product['color_id'] . '-' . $erp_product['talla'])
                                        ],
                                        [
                                            "attribute_code" => "color",
                                            "value" => $color_value
                                        ],
                                        [
                                            "attribute_code" => "talla",
                                            "value" => $size_value
                                        ],
                                        [
                                            "attribute_code" => "brand_id",
                                            "value" => $brand_value
                                        ],
                                        [
                                            "attribute_code" => "sexo",
                                            "value" => $sex_value
                                        ],
                                        [
                                            "attribute_code" => "genero",
                                            "value" => $gender_ids_array
                                        ]
                                    ]
                                ],
                                'saveOptions' => true
                            ]
                        ];

                        // Send POST request to update simple/virtual product
                        try {
                            $update_results[$new_sku] = $client->post('products', $options);
                            $sku_list[] = $new_sku;
                            $count++;
                        } catch (ClientException $e) {
                            $error_response = $e->getResponse();
                            $errors['ERROR updating product with SKU ' . $new_sku] = $error_response->getBody()->getContents();
                        }
                    }
                }

                if (
                    $count >= $max_transactions ||
                    (++$i === $num_erp_products && $count > 0)
                ) {
                    // Sync simple and virtual products into database //////////////////////////////////////////////////
                    foreach ($post_results as $sku => $post_response) {
                        if ($post_response->getStatusCode() === 200) {
                            $result = json_decode($post_response->getBody()->getContents(), true);
                            $new_count++;

                            // Only sync simple products
                            if ($result['type_id'] !== "configurable") {
                                // Get keys from SKU
                                $keys = explode('-', $sku);

                                // Get product categories from ERP
                                $category_ids_array = Shared::getProductCategories($db, $keys[3]);
                                $category_ids = join(',', $category_ids_array);
                                $db->close();

                                // Get size "talla" code
                                $size_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "talla");
                                $size = Shared::getAttributeValueLabel($api_size_options, $size_value);

                                // Get color code
                                $color_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "color");
                                $color = Shared::getAttributeValueLabel($api_color_options, $color_value);

                                // Get brand code
                                $brand_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "brand_id");
                                $brand = Shared::getAttributeValueLabel($api_brand_options, $brand_value);

                                // Get short_description value
                                $short_description = Shared::getAttributeOptionsValue($result['custom_attributes'], "short_description");

                                // Get sexo_code from result
                                $sex_result_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "sexo");
                                $sex_result = Shared::getAttributeValueLabel($api_sexo_options, $sex_result_value);
                                switch ($sex_result) {
                                    case 'Unisex':
                                        $sexo = '1';
                                        break;
                                    case 'Lady':
                                        $sexo = '2';
                                        break;
                                    case 'Kid':
                                        $sexo = '3';
                                        break;
                                    case 'Hombre':
                                        $sexo = '4';
                                        break;
                                    default:
                                        $sexo = '1';
                                        break;
                                }

                                // Add insert query and params
                                $ddl_query .= "INSERT INTO sync_productos (name,short_description,weight,sku,sync_iva_id,existencias,status,visibility,is_in_stock,magento_producto_id,price,cost,descripcion,xarticulo_id,nuevo,promocion,producto_web,ficha_tecnica,category_ids,talla,color,marca,sexo,genero_ids) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                array_push($params,
                                    $result['name'],                                                        // name
                                    $short_description,                                                     // short_description
                                    0,                                                                      // weight
                                    $sku,                                                                   // sku
                                    0,                                                                      // sync_iva_id
                                    $result['extension_attributes']['stock_item']['qty'],                   // existencias
                                    $result['status'],                                                      // status
                                    $result['visibility'],                                                  // visibility
                                    $result['extension_attributes']['stock_item']['is_in_stock'] ? 1 : 0,   // is_in_stock
                                    $result['id'],                                                          // magento_producto_id
                                    $result['price'],                                                       // price
                                    0,                                                                      // cost
                                    "",                                                                     // descripcion
                                    $keys[3],                                                               // xarticulo_id
                                    0,                                                                      // nuevo
                                    0,                                                                      // promocion
                                    0,                                                                      // producto_web
                                    0,                                                                      // ficha_tecnica
                                    $category_ids,                                                          // category_ids
                                    $size,                                                                  // talla
                                    $color,                                                                 // color
                                    $brand ? $brand : "",                                                   // marca
                                    $sexo,                                                                  // sexo
                                    $gender_ids                                                             // genero_ids
                                );
                            }
                        }
                    }

                    // Sync updated products ///////////////////////////////////////////////////////////////////////////
                    foreach ($update_results as $sku => $update_response) {
                        if ($update_response->getStatusCode() === 200) {
                            $result = json_decode($update_response->getBody()->getContents(), true);
                            $updated_count++;

                            if ($result['type_id'] !== "configurable") {

                                // Get keys from SKU
                                $keys = explode('-', $sku);

                                // Get product categories from ERP
                                $category_ids_array = Shared::getProductCategories($db, $keys[3]);
                                $category_ids = join(',', $category_ids_array);
                                $db->close();

                                // Get "talla" codes
                                $size_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "talla");
                                $size = Shared::getAttributeValueLabel($api_size_options, $size_value);

                                // Get "color" codes
                                $color_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "color");
                                $color = Shared::getAttributeValueLabel($api_color_options, $color_value);

                                // Get brand code
                                $brand_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "brand_id");
                                $brand = Shared::getAttributeValueLabel($api_brand_options, $brand_value);

                                // Get short_description value
                                $short_description = Shared::getAttributeOptionsValue($result['custom_attributes'], "short_description");

                                // Get sexo_code from result
                                $sex_result_value = Shared::getAttributeOptionsValue($result['custom_attributes'], "sexo");
                                $sex_result = Shared::getAttributeValueLabel($api_sexo_options, $sex_result_value);
                                switch ($sex_result) {
                                    case 'Unisex':
                                        $sexo = '1';
                                        break;
                                    case 'Lady':
                                        $sexo = '2';
                                        break;
                                    case 'Kid':
                                        $sexo = '3';
                                        break;
                                    case 'Hombre':
                                        $sexo = '4';
                                        break;
                                    default:
                                        $sexo = '1';
                                        break;
                                }

                                // Add insert query and params
                                $ddl_query .= "UPDATE sync_productos SET name = ?, short_description = ?, sku = ?, existencias = ?, status = ?, visibility = ?, is_in_stock = ?, price = ?, descripcion = ?, category_ids = ?, talla = ?, color = ?, marca = ?, sexo = ?, genero_ids = ? WHERE magento_producto_id = ?;";
                                array_push($params,
                                    $result['name'],                                                        // name
                                    $short_description,                                                     // short_description
                                    $sku,                                                                   // sku
                                    $result['extension_attributes']['stock_item']['qty'],                   // existencias
                                    $result['status'],                                                      // status
                                    $result['visibility'],                                                  // visibility
                                    $result['extension_attributes']['stock_item']['is_in_stock'] ? 1 : 0,   // is_in_stock
                                    $result['price'],                                                       // price
                                    "",                                                                     // descripcion
                                    $category_ids,                                                          // category_ids
                                    $size,                                                                  // talla
                                    $color,                                                                 // color
                                    $brand ? $brand : "",                                                   // marca
                                    $sexo,                                                                  // sexo
                                    $gender_ids,                                                            // genero_ids
                                    $result['id']                                                           // magento_producto_id
                                );
                            }
                        }
                    }

                    // Execute SQL query
                    if ($ddl_query !== "") {
                        try {
                            $db->executeQuery($ddl_query, $params);
                            $db->close();
                            $ddl_query = "";
                        } catch (DBALException $exception) {
                            $response->setData(array(
                                    'success' => false,
                                    'errors' => $errors,
                                    'new_products' => $new_count,
                                    'updated_products' => $updated_count,
                                    'message' => $exception->getMessage(),
                                    'duration (s)' => time() - $time
                                )
                            );

                            return $response;
                        }
                    }

                    // Reset counter
                    $count = 0;
                    $post_results = [];
                    $update_results = [];
                    $params = [];
                }
            }

            // Get Magento 2 products updated
            $query = "SELECT * FROM catalog_product_flat_1 catalog 
                      JOIN cataloginventory_stock_item stock ON catalog.entity_id = stock.product_id
                      JOIN cataloginventory_stock_status status ON catalog.entity_id = status.product_id";
            $api_products = $db_mg2->fetchAll($query);
            $db_mg2->close();

            // Get the product configurable information array
            $product_information_array = [];
            foreach ($api_products as $api_product) {
                if ($api_product['type_id'] === 'virtual') {
                    $keys = explode('-', $api_product['sku']);

                    // Get size "talla" code
                    if (isset($api_product['talla'])) {
                        $size_value = $api_product['talla'];
                        $size = $api_product['talla_value'];

                        // Create product configurable info
                        if ($size !== '-') {
                            $parent_sku = $keys[0].'-'.$keys[1].'-SKU';

                            if (!array_key_exists($parent_sku, $product_information_array)) {
                                $product_information_array[$parent_sku] = [
                                    'options' => [],
                                    'children' => []
                                ];
                            }

                            if (!in_array(intval($size_value), $product_information_array[$parent_sku]['options'])) {
                                $product_information_array[$parent_sku]['options'][] = intval($size_value);
                            }

                            if (!in_array($api_product['entity_id'], $product_information_array[$parent_sku]['children'])) {
                                $product_information_array[$parent_sku]['children'][] = $api_product['entity_id'];
                            }
                        }
                    } else {
                        $errors['Error getting configurable info array for '.$api_product['sku']] = $api_product;
                    }
                }
            }

            // Create configurable products ////////////////////////////////////////////////////////////////////////////
            $i = 0;
            $post_results = [];
            $update_results = [];
            $configurable_products = [];
            foreach ($erp_products as &$erp_product) {
                // If xproduct is null skip this product
                if (!$erp_product['producto']) {
                    continue;
                }

                // Fix null or not applicable values
                Shared::fixNullAttributeValue($erp_product['talla']);
                Shared::fixNullAttributeValue($erp_product['color']);

                // Get parent SKU
                $new_sku = $erp_product['producto'].'-'.$erp_product['color_id'].'-SKU';

                // Continue to next iteration if product is not configurable or is already processed
                if ($erp_product['talla'] === '-' || in_array($new_sku, $configurable_products)) {
                    continue;
                } else {
                    $configurable_products[] = $new_sku;
                }

                // Get product color value
                $color_value = Shared::getAttributeOptionsValue($api_color_options, $erp_product['color']);

                // Get product brand value
                $brand_value = Shared::getAttributeOptionsValue($api_brand_options, $erp_product['marca']);

                // Get product genders
                $gender_ids_array = Shared::getProductGenderIds($api_gender_options, $erp_product);
                $gender_ids = join(',', $gender_ids_array);

                // Get product sex value
                $sex_value = 0;
                $sexo_code = intval($erp_product['sexo_code']);
                switch ($sexo_code) {
                    case 1: // Unisex
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Unisex') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    case 2: // Lady
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Lady') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    case 3: // Kid
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Kid') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    case 4: // Hombre
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Hombre') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                    default:
                        foreach ($api_sexo_options as $sexo) {
                            if ($sexo['label'] === 'Unisex') {
                                $sex_value = $sexo['value'];
                                break;
                            }
                        }
                        break;
                }

                // Get price
                $price = isset($erp_product['precio_ttc']) ? floatval($erp_product['precio_ttc']) : 0.0;

                // Get name and description
                $name = $erp_product['nombre'] ? $erp_product['nombre'] : $erp_product['descripcion'];
                $description = $erp_product['descripcion_larga'] ? $erp_product['descripcion_larga'] : $name;

                // Get child SKU for simple products and products without size
                $child_sku = $erp_product['talla'] === '-' ?
                    $erp_product['producto'].'-'.$erp_product['color_id'].'-SKU-'.$erp_product['art_id'] :
                    $erp_product['producto'].'-'.$erp_product['color_id'].'-SKU-'.$erp_product['art_id'].'-'.$erp_product['talla'];

                // Check product is not already registered in Magento
                $product_magento = Shared::getProductBySku($api_products, $new_sku);

                // Get product categories from ERP
                $category_ids_array = Shared::getProductCategories($db, $erp_product['art_id']);
                $db->close();

                // Get configuration info
                $values = [];
                $children_ids = [];
                if (array_key_exists($new_sku, $product_information_array)) {
                    $info = $product_information_array[$new_sku];
                    foreach ($info['options'] as $option) {
                        if ($option) {
                            $values[] = [
                                'value_index' => $option
                            ];
                        }
                    }

                    foreach ($info['children'] as $child) {
                        if ($child) {
                            $children_ids[] = intval($child);
                        }
                    }
                }

                if (
                    !$product_magento &&
                    $erp_product['visible'] === '-1'
                ) {
                    // Create new configurable product
                    $options = [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'product' => [
                                // Add product field data.
                                'sku' => $new_sku,
                                "name" => $name,
                                "price" => $price,
                                "status" => 1,
                                "visibility" => (intval($erp_product['visible']) === -1) ? 4 : 1,
                                "type_id" => "configurable",
                                "attribute_set_id" => 4,           // Default Attribute Set is ID 4
                                "weight" => 0,                     // Not defined anywhere
                                "extension_attributes" => [
                                    "configurable_product_options" => [
                                        [
                                            'attribute_id'      => $api_size_id,
                                            'label'             => "Talla",
                                            'position'          => 0,
                                            'is_use_default'    => true,
                                            'values'            => $values
                                        ]
                                    ],
                                    "configurable_product_links"   => $children_ids,
                                    "stock_item" => [
                                        "is_in_stock" => true
                                    ]
                                ],
                                "custom_attributes" => [
                                    [
                                        'attribute_code' => 'description',
                                        'value' => $description
                                    ],
                                    [
                                        "attribute_code" => "category_ids",
                                        "value" => $category_ids_array
                                    ],
                                    [
                                        "attribute_code" => "url_key",
                                        "value" => Shared::getSlug($name . '-' . $erp_product['color_id'])
                                    ],
                                    [
                                        "attribute_code" => "color",
                                        "value" => $color_value
                                    ],
                                    [
                                        "attribute_code" => "brand_id",
                                        "value" => $brand_value
                                    ],
                                    [
                                        "attribute_code" => "sexo",
                                        "value" => $sex_value
                                    ],
                                    [
                                        "attribute_code" => "genero",
                                        "value" => $gender_ids_array
                                    ],
                                    [
                                        "attribute_code" => "aw_arp_override_native",
                                        "value" => 0
                                    ]
                                ]
                            ],
                            'saveOptions' => true
                        ]
                    ];

                    // Send POST request to create configurable product
                    try {
                        $post_results[$new_sku] = $client->post('products', $options);
                        $count++;
                    } catch (ClientException $e) {
                        $error_response = $e->getResponse();
                        $errors['ERROR creating configurable product with SKU ' . $new_sku] = $error_response->getBody()->getContents();
                    }

                } elseif ($product_magento['entity_id']) {
                    // Get configurable info
                    $query = "SELECT * FROM `catalog_product_flat_1` catalog
                                JOIN catalog_product_relation AS relation ON catalog.entity_id = relation.child_id
                                WHERE relation.parent_id = ".$product_magento['entity_id'].";";
                    $children = $db_mg2->fetchAll($query);
                    $db_mg2->close();

                    $old_values = [];
                    $old_children_ids = [];
                    foreach ($children as $child) {
                        $old_values[] = [
                            'value_index' => intval($child['talla'])
                        ];

                        $old_children_ids[] = $child['entity_id'];
                    }

                    // Get sexo_code from product_magento
                    $sex_result = Shared::getAttributeValueLabel($api_sexo_options, $product_magento['sexo']);
                    switch ($sex_result) {
                        case 'Unisex':
                            $sexo = '1';
                            break;
                        case 'Lady':
                            $sexo = '2';
                            break;
                        case 'Kid':
                            $sexo = '3';
                            break;
                        case 'Hombre':
                            $sexo = '4';
                            break;
                        default:
                            $sexo = '1';
                            break;
                    }

                    // Get visibility
                    $visibility = (intval($erp_product['visible']) === -1) ? 4 : 1;

                    // Get product gender ids from Magento
                    $mag_gender_ids = explode(',', $erp_product['mag_gender_ids']);

                    // Only update configurable product if there are changes
                    if (
                        $values != $old_values ||
                        $children_ids != $old_children_ids ||
                        $price != $product_magento['price'] ||
                        $name != $product_magento['name'] ||
                        $sexo != $erp_product['sexo_code'] ||
                        $visibility != $product_magento['visibility'] ||
                        $product_magento['description'] != $erp_product['descripcion_larga'] ||
                        $gender_ids_array != $mag_gender_ids
                    ) {
                        $options = [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $status->getToken()
                            ],
                            'json' => [
                                'product' => [
                                    // Add product field data.
                                    'id'  => $product_magento['entity_id'],
                                    'sku' => $product_magento['sku'],
                                    "name" => $name,
                                    "price" => $price,
                                    "status" => 1,
                                    "visibility" => $visibility,
                                    "type_id" => "configurable",
                                    "attribute_set_id" => 4,           // Default Attribute Set is ID 4
                                    "weight" => 0,                     // Not defined anywhere
                                    "extension_attributes" => [
                                        "configurable_product_options" => [
                                            [
                                                'attribute_id'      => $api_size_id,
                                                'label'             => "Talla",
                                                'position'          => 0,
                                                'is_use_default'    => true,
                                                'values'            => $values
                                            ]
                                        ],
                                        "configurable_product_links"   => $children_ids,
                                        "stock_item" => [
                                            "is_in_stock" => true
                                        ]
                                    ],
                                    "custom_attributes" => [
                                        [
                                            'attribute_code' => 'description',
                                            'value' => $description
                                        ],
                                        [
                                            "attribute_code" => "category_ids",
                                            "value" => $category_ids_array
                                        ],
                                        [
                                            "attribute_code" => "url_key",
                                            "value" => Shared::getSlug($name . '-' . $erp_product['color_id'])
                                        ],
                                        [
                                            "attribute_code" => "color",
                                            "value" => $color_value
                                        ],
                                        [
                                            "attribute_code" => "brand_id",
                                            "value" => $brand_value
                                        ],
                                        [
                                            "attribute_code" => "sexo",
                                            "value" => $sex_value
                                        ],
                                        [
                                            "attribute_code" => "genero",
                                            "value" => $gender_ids_array
                                        ],
                                        [
                                            "attribute_code" => "aw_arp_override_native",
                                            "value" => 0
                                        ]
                                    ]
                                ],
                                'saveOptions' => true
                            ]
                        ];

                        // Send POST request to update configurable product
                        try {
                            $update_results[$new_sku] = $client->post('products', $options);
                            $count++;
                        } catch (ClientException $e) {
                            $error_response = $e->getResponse();
                            $errors['ERROR updating configurable product with SKU ' . $new_sku] = $error_response->getBody()->getContents();
                        }
                    }
                }

                if (
                    $count >= $max_transactions ||
                    (++$i === $num_erp_products && $count > 0)
                ) {
                    // Collect POST errors
                    foreach ($post_results as $sku => $post_response) {
                        if ($post_response->getStatusCode() === 200) {
                            $new_count++;
                            $result = json_decode($post_response->getBody()->getContents(), true);
                        }
                    }

                    // Collect POST update errors
                    foreach ($update_results as $sku => $update_response) {
                        if ($update_response->getStatusCode() === 200) {
                            $updated_count++;
                            $result = json_decode($update_response->getBody()->getContents(), true);
                        }
                    }

                    // Reset counter
                    $count = 0;
                    $post_results = [];
                    $update_results = [];
                }
            }

            // Only perform relationships in case there are new products or some that have changed
            if (count($product_ids_array) > 0 || $mode === 'massive') {
                $code_list = [];

                // Only get code_list if mode is not massive
                if ($mode !== 'massive') {
                    foreach ($sku_list as $item_sku) {
                        $keys = explode('-', $item_sku);
                        $code_list[] = $keys[0];
                    }
                }

                // Get updated Magento 2 products
                $query = "SELECT * FROM catalog_product_flat_1 catalog 
                      JOIN cataloginventory_stock_item stock ON catalog.entity_id = stock.product_id
                      JOIN cataloginventory_stock_status status ON catalog.entity_id = status.product_id";
                if (count($code_list) > 0 && $mode !== 'massive') {
                    $query .= " WHERE ";
                    for ($i = 0; $i < count($code_list); $i++) {
                        if ($i < (count($code_list)-1)) {
                            $query .= "catalog.sku LIKE '".$code_list[$i]."%' OR ";
                        } else {
                            $query .= "catalog.sku LIKE '".$code_list[$i]."%';";
                        }
                    }
                } else {
                    $query .= ';';
                }

                $api_products = $db_mg2->fetchAll($query);
                $db_mg2->close();

                // Product links ///////////////////////////////////////////////////////////////////////////////////////
                $product_links = [];
                foreach ($api_products as $api_product) {
                    if ($api_product['type_id'] === 'configurable' || $api_product['type_id'] === 'simple') {
                        // Get related products using SKU
                        $sku = $api_product['sku'];
                        $keys = explode('-', $sku);
                        $erp_product_code = $keys[0];
                        $product_links[$sku] = [];

                        foreach ($api_products as $product) {
                            if ($product['type_id'] === $api_product['type_id'] && $sku !== $product['sku']) {
                                $keys2 = explode('-', $product['sku']);
                                $erp_product_code2 = $keys2[0];
                                if ($erp_product_code === $erp_product_code2) {
                                    $product_links[$sku][] = $product['sku'];
                                }
                            }
                        }

                        // Add product links
                        $product_links_count = count($product_links[$sku]);
                        if ($product_links_count > 0) {
                            $links_response = $client->get('products/'.$sku.'/links/related', [
                                'headers' => [
                                    'Authorization' => 'Bearer ' . $status->getToken()
                                ]
                            ]);

                            if ($links_response->getStatusCode() === 200) {
                                $api_links = json_decode($links_response->getBody()->getContents(), true);
                                $api_links_count = count($api_links);
                                if ($api_links_count > 0 && ($api_links_count < $product_links_count)) {
                                    // PUT new links
                                    $update_links_promises = [];
                                    $i = $api_links_count;
                                    foreach ($product_links[$sku] as $link_sku) {
                                        if (!Shared::productLinkExists($api_links, $link_sku)) {
                                            $update_links_promises[$sku] = $client->putAsync('products/'.$sku.'/links', [
                                                'headers' => [
                                                    'Authorization' => 'Bearer ' . $status->getToken()
                                                ],
                                                'json' => [
                                                    'entity' => [
                                                        'sku' => $sku,
                                                        'link_type' => 'related',
                                                        'linked_product_sku' => $link_sku,
                                                        'linked_product_type' => $api_product['type_id'],
                                                        'position' => $i++,
                                                    ]
                                                ]
                                            ]);
                                        }
                                    }

                                    if (count($update_links_promises) > 0) {
                                        $update_links_results = Promise\settle($update_links_promises)->wait();

                                        foreach ($update_links_results as $result) {
                                            if ($result['state'] !== 'fulfilled') {
                                                $errors['ERROR updating links for product with SKU ' . $sku] = $result['reason']->getMessage();
                                            }
                                        }
                                    }
                                } else {
                                    // POST new links
                                    $items = [];
                                    $i = 0;
                                    foreach ($product_links[$sku] as $product_link) {
                                        $items[] = [
                                            'sku' => $sku,
                                            'link_type' => 'related',
                                            'linked_product_sku' => $product_link,
                                            'linked_product_type' => $api_product['type_id'],
                                            'position' => $i++,
                                        ];
                                    }

                                    if (count($items) > 0) {
                                        try {
                                            $create_links_response = $client->post('products/'.$sku.'/links', [
                                                'headers' => [
                                                    'Authorization' => 'Bearer ' . $status->getToken()
                                                ],
                                                'json' => [
                                                    'items' => $items
                                                ]
                                            ]);
                                        } catch (ClientException $e) {
                                            $error_response = $e->getResponse();
                                            $errors['ERROR creating links for product SKU ' . $sku] = $error_response->getBody()->getContents();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $response->setData(array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'num_errors' => count($errors),
                    'new_products' => $new_count,
                    'updated_products' => $updated_count,
                    'message' => count($errors) === 0 ?
                        'Products synchronized successfully' :
                        'Products synchronized with errors',
                    'duration (s)' => time() - $time)
            );
        }

        // Save current datetime as latest product sync date
        $now = new \DateTime('now');
        $status->setProductSyncDate($now);
        $em->persist($status);
        $em->flush();

        // Send email with errors info
        if (count($errors) > 0) {
            $topics = '<ul>';
            foreach ($errors as $topic => $error) {
                $topics .= '<li><strong>'.$topic.':</strong><pre>'.$error.'</pre></li>';
            }
            $topics .= '</ul>';

            $email_body = <<<EOT
<p>Se han encontrado los siguientes errores de sincronización de productos:<br></p>
$topics
EOT;

            $message = (new \Swift_Message('Errores de sincronización de productos'))
                ->setFrom('sincronizacion@autoserviciomotorista.com.es')
                ->setTo(['santos@lagahe.com', 'alberto@lagahe.com'])
                ->setBody(
                    $email_body,
                    'text/html'
                )
            ;

            $mailer->send($message);
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/ekon/sync-customers")
     */
    public function syncCustomersAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'ekon') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get customers and related data from ERP
        $db = $this->get('doctrine.dbal.integration_connection');
        $query = "SELECT DISTINCT 
                      mu.xusuario_id AS email,
                      mu.xpassword AS pass,
                      mu.xnombre AS nombre,
                      mu.xapellidos AS apellidos,
                      pc.xnif AS nif,
                      pc.xdomicilio AS domicilio,
                      pc.xcod_postal AS cpostal,
                      pc.xpoblacion AS poblacion,
                      pc.xprovincia_id AS provincia_id,
                      pp.xnombre AS provincia,
                      pa.xcodigo_isoa AS pais,
                      pl.xtelefono AS telefono,
                      pl.xinternet AS website,
                      pl.xfax AS fax,
                      pl.xemail AS email2,
                      ple.xnombre AS env_nombre,
                      ple.aapellidos AS env_apellidos,
                      ple.xdomicilio AS env_domicilio,
                      ple.xpoblacion AS env_poblacion,
                      ple.xprovincia_id AS env_provincia_id,
                      pp_e.xnombre AS env_provincia,
                      ple.xtelefono AS env_telefono,
                      ple.xfax AS env_fax,
                      ple.xcod_postal AS env_cpostal,
                      pa_e.xcodigo_isoa AS env_pais,
                      plf.xnombre AS fac_nombre,
                      plf.aapellidos AS fac_apellidos,
                      plf.xdomicilio AS fac_domicilio,
                      plf.xpoblacion AS fac_poblacion,
                      plf.xprovincia_id AS fac_provincia_id,
                      pp_f.xnombre AS fac_provincia,
                      plf.xtelefono AS fac_telefono,
                      plf.xfax AS fac_fax,
                      plf.xcod_postal AS fac_cpostal,
                      pa_f.xcodigo_isoa AS fac_pais,
                      mpc.xpuntos AS puntos,
                      sync.dni AS sync_nif,
                      sync.magento_cliente_id AS mag_id
                  FROM mag_usuarios mu
                  LEFT JOIN pc_clientes pc ON mu.xcliente_id = pc.xcliente_id
                  LEFT JOIN pc_paises pa ON pc.xpais_id = pa.xpais_id
                  LEFT JOIN pc_provincias pp ON pc.xprovincia_id = pp.xprovincia_id AND pc.xpais_id = pp.xpais_id
                  LEFT JOIN pl_clientes pl ON mu.xcliente_id = pl.xcliente_id
                  LEFT JOIN pl_cli_env ple ON mu.xcliente_id = ple.xcliente_id
                  LEFT JOIN pc_paises pa_e ON ple.xpais_id = pa_e.xpais_id
                  LEFT JOIN pc_provincias pp_e ON ple.xprovincia_id = pp_e.xprovincia_id AND ple.xpais_id = pp_e.xpais_id
                  LEFT JOIN pl_cli_fact plf ON mu.xcliente_id = plf.xcliente_id
                  LEFT JOIN pc_paises pa_f ON plf.xpais_id = pa_f.xpais_id
                  LEFT JOIN pc_provincias pp_f ON plf.xprovincia_id = pp_f.xprovincia_id AND plf.xpais_id = pp_f.xpais_id
                  LEFT JOIN mag_puntos_cliente mpc ON mu.xcliente_id = mpc.xcliente_id
                  LEFT JOIN sync_clientes sync ON mu.xusuario_id = sync.email;";
        $erp_customers = $db->fetchAll($query);

        // Get synced customers
        $query_sync_customers = "SELECT * FROM sync_clientes";
        $sync_customers = $db->fetchAll($query_sync_customers);

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Customers could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'new_erp_customers' => false,
                    'new_mg2_customers' => false,
                    'message' => $errors[0])
            );
        } else {
            // Get Magento 2 database
            $db_mg2 = $this->get('doctrine.dbal.mg2_connection');

            // DDL query string and params array
            $ddl_query = "";
            $params = [];

            // Get Magento customers
            $query_api_customers = "SELECT * FROM customer_entity";
            $api_customers = $db_mg2->fetchAll($query_api_customers);

            // Get DNI attribute info
            $query_dni_attribute = "SELECT * FROM eav_attribute WHERE attribute_code = 'dni'";
            $dni_attribute = $db_mg2->fetchAssoc($query_dni_attribute);

            // Get Guzzle Client
            $client = new Client(['base_uri' => $uri]);

            // Get countries info
            $api_response = $client->request('GET', 'directory/countries', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_countries = [];
            if ($api_response->getStatusCode() === 200) {
                $api_countries = json_decode($api_response->getBody()->getContents());
            }

            // New customers
            $new_customers_from_erp = [];
            $pending_customers = [];

            // Sync new customers from ERP ###########################################################################
            $post_promises = [];
            foreach ($erp_customers as $erp_customer) {
                // Check user is not already registered in Magento
                $customer_magento = null;
                foreach ($api_customers as $api_customer) {
                    if ($api_customer['email'] === $erp_customer['email']) {
                        $customer_magento = $api_customer;
                        break;
                    }
                }

                if (($erp_customer['mag_id'] === null) && ($customer_magento === null)) {
                    // Get regions for each address
                    $customer_region = [];
                    $customer_region_env = [];
                    $customer_region_fac = [];

                    foreach ($api_countries as $country) {
                        if (strtolower($country->id) === strtolower($erp_customer['pais'])) {
                            if (isset($country->available_regions)) {
                                foreach ($country->available_regions as $region) {
                                    if ($region->name === $erp_customer['provincia']) {
                                        $customer_region['region'] = $region->name;
                                        $customer_region['region_id'] = $region->id;
                                        $customer_region['region_code'] = $region->code;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }

                    if (isset($erp_customer['env_provincia'])) {
                        foreach ($api_countries as $country) {
                            if (strtolower($country->id) === strtolower($erp_customer['pais'])) {
                                if (isset($country->available_regions)) {
                                    foreach ($country->available_regions as $region) {
                                        if ($region->name === $erp_customer['env_provincia']) {
                                            $customer_region_env['region'] = $region->name;
                                            $customer_region_env['region_id'] = $region->id;
                                            $customer_region_env['region_code'] = $region->code;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        $customer_region_env = $customer_region;
                    }

                    if (isset($erp_customer['fac_provincia'])) {
                        foreach ($api_countries as $country) {
                            if (strtolower($country->id) === strtolower($erp_customer['pais'])) {
                                if (isset($country->available_regions)) {
                                    foreach ($country->available_regions as $region) {
                                        if ($region->name === $erp_customer['fac_provincia']) {
                                            $customer_region_fac['region'] = $region->name;
                                            $customer_region_fac['region_id'] = $region->id;
                                            $customer_region_fac['region_code'] = $region->code;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        $customer_region_fac = $customer_region;
                    }

                    // Fix empty fields from database
                    if (!isset($erp_customer['apellidos']) || $erp_customer['apellidos'] === '') {
                        $erp_customer['apellidos'] = 'No info ERP';
                    }

                    $post_promises[$erp_customer['email'].':'.$erp_customer['pass']] = $client->postAsync('customers', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'customer' => [
                                // Add customer field data.
                                'email' => $erp_customer['email'],
                                'firstname' => $erp_customer['nombre'],
                                'lastname' => $erp_customer['apellidos'],
                                'addresses' => [
                                    [
                                        'defaultShipping' => false,
                                        'defaultBilling' => false,
                                        'firstname' => $erp_customer['nombre'],
                                        'lastname' => $erp_customer['apellidos'],
                                        'postcode' => $erp_customer['cpostal'],
                                        'street' => [
                                            $erp_customer['domicilio']
                                        ],
                                        'city' => $erp_customer['poblacion'],
                                        'telephone' => $erp_customer['telefono'],
                                        'fax' => $erp_customer['fax'],
                                        'countryId' => $erp_customer['pais'],
                                        'region' => (count($customer_region) > 0) ? $customer_region : null
                                    ],
                                    [
                                        'defaultShipping' => true,
                                        'defaultBilling' => false,
                                        'firstname' => isset($erp_customer['env_nombre']) ? $erp_customer['env_nombre'] : $erp_customer['nombre'],
                                        'lastname' => isset($erp_customer['env_apellidos']) ? $erp_customer['env_apellidos'] : $erp_customer['apellidos'],
                                        'postcode' => isset($erp_customer['env_cpostal']) ? $erp_customer['env_cpostal'] : $erp_customer['cpostal'],
                                        'street' => [
                                            isset($erp_customer['env_domicilio']) ? $erp_customer['env_domicilio'] : $erp_customer['domicilio']
                                        ],
                                        'city' => isset($erp_customer['env_poblacion']) ? $erp_customer['env_poblacion'] : $erp_customer['poblacion'],
                                        'telephone' => isset($erp_customer['env_telefono']) ? $erp_customer['env_telefono'] : $erp_customer['telefono'],
                                        'fax' => isset($erp_customer['env_fax']) ? $erp_customer['env_fax'] : $erp_customer['fax'],
                                        'countryId' => isset($erp_customer['env_pais']) ? $erp_customer['env_pais'] : $erp_customer['pais'],
                                        'region' => (count($customer_region_env) > 0) ? $customer_region_env : null
                                    ],
                                    [
                                        'defaultShipping' => false,
                                        'defaultBilling' => true,
                                        'firstname' => isset($erp_customer['fac_nombre']) ? $erp_customer['fac_nombre'] : $erp_customer['nombre'],
                                        'lastname' => isset($erp_customer['fac_apellidos']) ? $erp_customer['fac_apellidos'] : $erp_customer['apellidos'],
                                        'postcode' => isset($erp_customer['fac_cpostal']) ? $erp_customer['fac_cpostal'] : $erp_customer['cpostal'],
                                        'street' => [
                                            isset($erp_customer['fac_domicilio']) ? $erp_customer['fac_domicilio'] : $erp_customer['domicilio']
                                        ],
                                        'city' => isset($erp_customer['fac_poblacion']) ? $erp_customer['fac_poblacion'] : $erp_customer['poblacion'],
                                        'telephone' => isset($erp_customer['fac_telefono']) ? $erp_customer['fac_telefono'] : $erp_customer['telefono'],
                                        'fax' => isset($erp_customer['fac_fax']) ? $erp_customer['fac_fax'] : $erp_customer['fax'],
                                        'countryId' => isset($erp_customer['fac_pais']) ? $erp_customer['fac_pais'] : $erp_customer['pais'],
                                        'region' => (count($customer_region_fac) > 0) ? $customer_region_fac : null
                                    ]
                                ],
                                'custom_attributes' => [
                                    [
                                        'attribute_code' => 'customer_points',
                                        'value' => isset($erp_customer['puntos']) ? $erp_customer['puntos'] : '0'
                                    ],
                                    [
                                        'attribute_code' => 'dni',
                                        'value' => isset($erp_customer['nif']) ? $erp_customer['nif'] : ''
                                    ]
                                ]
                            ],
                            'password' => $erp_customer['pass']
                        ]
                    ]);
                } elseif (($erp_customer['mag_id'] === null) && ($customer_magento !== null)) {
                    $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push(
                        $params,
                        $customer_magento['firstname'].' '.$customer_magento['lastname'],
                        $customer_magento['email'],
                        $erp_customer['pass'],
                        $erp_customer['nif'] ?
                            $erp_customer['nif'] :
                            "",
                        $customer_magento['group_id'],
                        $customer_magento['entity_id']
                    );

                    // Store pending customer to check its addresses
                    if (!in_array($customer_magento, $pending_customers)) {
                        $pending_customers[] = $customer_magento;
                    }
                }
            }

            // Send post promises
            $post_results = Promise\settle($post_promises)->wait();
            $fulfilled_responses = [];

            foreach ($post_results as $email_pass => $post_response) {
                $email_pass = explode(':', $email_pass);
                $email = $email_pass[0];
                $password = $email_pass[1];
                if ($post_response['state'] === "fulfilled") {
                    $result = json_decode($post_response['value']->getBody()->getContents());
                    $fulfilled_responses[$email] = $result;
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                    $dni = "";
                    foreach ($erp_customers as $erp_customer) {
                        if ($erp_customer['email'] === $result->email) {
                            $dni = $erp_customer['nif'];
                            break;
                        }
                    }
                    array_push($params, $result->firstname.' '.$result->lastname, $result->email, $password, $dni, $result->group_id, $result->id);
                    $new_customers_from_erp[] = $result;
                } else {
                    $errors['ERROR creating customer with email address ' . $email] = $post_response['reason']->getMessage();
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'new_erp_customers' => $new_customers_from_erp,
                            'new_mg2_customers' => false,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync pending addresses
            foreach ($pending_customers as $pending_customer) {
                // Get customer from ERP
                $query_erp_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$pending_customer['entity_id'].";";
                $pending_erp_customer = $db->fetchAssoc($query_erp_customer);

                if ($pending_erp_customer) {
                    // Check if there are pending addresses to sync into ERP database
                    $query_api_addresses = "SELECT * FROM customer_address_entity WHERE parent_id = ".$pending_customer['entity_id'].";";
                    $pending_addresses = $db_mg2->fetchAll($query_api_addresses);
                    $db_mg2->close();

                    if (count($pending_addresses) > 0) {
                        foreach ($pending_addresses as $pending_address) {
                            $query_erp_address = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$pending_address['entity_id'].";";
                            $erp_address = $db->fetchAssoc($query_erp_address);

                            if (!$erp_address) {
                                $pending_address_type = Shared::getAddressType($pending_address, $pending_customer['default_shipping'], $pending_customer['default_billing']);

                                $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                array_push(
                                    $params,
                                    $pending_address['firstname'],
                                    $pending_address['street'],
                                    $pending_address['city'],
                                    $pending_address['region'],
                                    $pending_address['country_id'],
                                    $pending_address['postcode'],
                                    $pending_address['telephone'],
                                    $pending_erp_customer['id'],
                                    $pending_address_type,
                                    $pending_address['entity_id']
                                );
                            }
                        }
                    }
                }
            }

            // Sync new addresses
            foreach ($fulfilled_responses as $email => $result) {
                $addresses = $result->addresses;
                if (count($addresses) > 0) {
                    $id_default_shipping = intval($result->default_shipping);
                    $id_default_billing = intval($result->default_billing);

                    // Get customer synchronized
                    $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = " . $result->id;
                    $sync_customer = $db->fetchAssoc($query_sync_customer);

                    // Create SQL INSERTS for addresses
                    foreach ($addresses as $address) {
                        // Addresses types
                        // 0 -> none, 1 -> billing, 2 -> shipping, 3 -> both
                        $address_type = Shared::getAddressType($address, $id_default_shipping, $id_default_billing);

                        // Add insert query and params
                        $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $address->firstname,
                            $address->street[0],
                            $address->city,
                            $address->region->region,
                            $address->country_id,
                            $address->postcode,
                            $address->telephone,
                            $sync_customer['id'],
                            $address_type,
                            $address->id);
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'new_erp_customers' => $new_customers_from_erp,
                            'new_mg2_customers' => false,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync customers from Magento 2 ###########################################################################
            $magento_api_customers_to_sync = [];
            $magento_api_customers_found = [];
            foreach ($api_customers as $api_customer) {
                $found = false;
                foreach ($sync_customers as $sync_customer) {
                    if ($api_customer['entity_id'] === $sync_customer['magento_cliente_id']) {
                        $magento_api_customers_found[] = $api_customer;
                        $found = true;
                        break;
                    }
                }

                // Check if the customer has orders
                $query_customer_orders = "SELECT * FROM sync_pedidos sp JOIN sync_clientes sc ON sp.sync_cliente_id = sc.id WHERE sc.magento_cliente_id = ".$api_customer['entity_id'].";";
                $orders = $db->fetchAll($query_customer_orders);

                if (
                    !$found &&
                    (count($orders) > 0)
                ) {
                    $magento_api_customers_to_sync[] = $api_customer;
                }
            }

            foreach ($magento_api_customers_to_sync as $api_customer_to_sync) {
                // Get DNI from Magento 2 database
                $dni = "";
                $query_dni = "SELECT * FROM customer_entity_varchar WHERE attribute_id = ".$dni_attribute['attribute_id']." AND entity_id = ".$api_customer_to_sync['entity_id'].";";
                $dni_info = $db_mg2->fetchAssoc($query_dni);
                if ($dni_info) {
                    $dni = $dni_info['value'];
                }

                // INSERT new customer
                $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params, $api_customer_to_sync['firstname'].' '.$api_customer_to_sync['lastname'], $api_customer_to_sync['email'], "-", $dni, $api_customer_to_sync['group_id'], $api_customer_to_sync['entity_id']);
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'new_erp_customers' => $new_customers_from_erp,
                            'new_mg2_customers' => $magento_api_customers_to_sync,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new addresses from Magento users
            foreach ($magento_api_customers_to_sync as $api_customer_to_sync) {
                $query_api_addresses = "SELECT * FROM customer_address_entity WHERE parent_id = ".$api_customer_to_sync['entity_id'].";";
                $addresses = $db_mg2->fetchAll($query_api_addresses);
                $db_mg2->close();

                if (count($addresses) > 0) {
                    $id_default_shipping = intval(isset($api_customer_to_sync['default_shipping']) ? $api_customer_to_sync['default_shipping'] : -1);
                    $id_default_billing = intval(isset($api_customer_to_sync['default_billing']) ? $api_customer_to_sync['default_billing'] : -1);

                    // Get customer synchronized
                    $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$api_customer_to_sync['entity_id'];
                    $sync_customer = $db->fetchAssoc($query_sync_customer);

                    // Create SQL INSERTS for addresses
                    foreach ($addresses as $address) {
                        // Addresses types
                        // 0 -> none, 1 -> billing, 2 -> shipping, 3 -> both
                        $address_type = Shared::getAddressType($address, $id_default_shipping, $id_default_billing);

                        // Add insert query and params
                        $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        array_push(
                            $params,
                            $address['firstname'],
                            $address['street'],
                            $address['city'],
                            $address['region'],
                            $address['country_id'],
                            $address['postcode'],
                            $address['telephone'],
                            $sync_customer['id'],
                            $address_type,
                            $address['entity_id']
                        );
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'new_erp_customers' => $new_customers_from_erp,
                            'new_mg2_customers' => $magento_api_customers_to_sync,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new customers and their addresses in sync_operations
            foreach ($magento_api_customers_to_sync as $api_customer_to_sync) {
                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes sc
                                        LEFT JOIN mag_usuarios mu ON mu.xusuario_id = sc.email
                                        WHERE magento_cliente_id = ".$api_customer_to_sync->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Check if customer already exists in ERP
                if ($sync_customer && !$sync_customer['xusuario_id']) {
                    // Get customer addresses
                    $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = " . $sync_customer['id'];
                    $sync_addresses = $db->fetchAll($query_customer_addresses);

                    // Insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_clientes',
                        'id',
                        $sync_customer['id'],
                        1,                  // New
                        0,
                        0);

                    // New addresses in sync_operations
                    foreach ($sync_addresses as $sync_address) {
                        // Insert the new operations into sync_operations
                        $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            'sync_direccions',
                            'id',
                            $sync_address['id'],
                            1,                  // New
                            0,
                            0);
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'new_erp_customers' => $new_customers_from_erp,
                            'new_mg2_customers' => $magento_api_customers_to_sync,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Update customers from Magento 2 ###################################################################

            // Arrays for insert customer's addresses operations into sync_operations
            $updated_customers_ids = [];
            $updated_customer_new_addresses_ids = [];
            $updated_customer_updated_addresses_ids = [];

            foreach ($magento_api_customers_found as $api_customer_found) {
                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$api_customer_found['entity_id'];
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Continue if customer is not synced yet
                if (!$sync_customer) {
                    continue;
                }

                // Get customer info to be checked
                $query_api_addresses = "SELECT * FROM customer_address_entity WHERE parent_id = ".$api_customer_found['entity_id'].";";
                $addresses = $db_mg2->fetchAll($query_api_addresses);
                $db_mg2->close();

                $id_default_shipping = 0;
                $id_default_billing = 0;
                if (count($addresses) > 0) {
                    $id_default_shipping = intval($api_customer_found['default_shipping']);
                    $id_default_billing = intval($api_customer_found['default_billing']);
                }

                // Get DNI from Magento 2 database
                $dni = "";
                $query_dni = "SELECT * FROM customer_entity_varchar WHERE attribute_id = ".$dni_attribute['attribute_id']." AND entity_id = ".$api_customer_found['entity_id'].";";
                $dni_info = $db_mg2->fetchAssoc($query_dni);
                if ($dni_info) {
                    $dni = $dni_info['value'];
                }

                if (
                    $sync_customer['name'] !== $api_customer_found['firstname'].' '.$api_customer_found['lastname'] ||
                    $sync_customer['email'] !== $api_customer_found['email'] ||
                    $sync_customer['dni'] !== $dni ||
                    intval($sync_customer['sync_grupo_id']) !== intval($api_customer_found['group_id'])
                ) {
                    // Update customer synced info
                    $ddl_query .= "UPDATE sync_clientes SET 
                                    name = ?,
                                    email = ?,
                                    dni = ?,
                                    sync_grupo_id = ? 
                                    WHERE magento_cliente_id = ?;";
                    array_push($params,
                        $api_customer_found['firstname'].' '.$api_customer_found['lastname'],
                        $api_customer_found['email'],
                        $dni,
                        $api_customer_found['group_id'],
                        $api_customer_found['entity_id']);

                    $updated_customers_ids[] = $sync_customer['id'];
                }

                // Create or update addresses
                foreach ($addresses as $address) {
                    $query_sync_address = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$address['entity_id'];
                    $sync_address = $db->fetchAssoc($query_sync_address);
                    $db->close();

                    // Addresses types
                    // 0 -> none, 1 -> billing, 2 -> shipping, 3 -> both
                    $address_type = Shared::getAddressType($address, $id_default_shipping, $id_default_billing);

                    if (!$sync_address) {
                        // Insert new address query
                        $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $address['firstname'],
                            $address['street'],
                            $address['city'],
                            $address['region'],
                            $address['country_id'],
                            $address['postcode'],
                            $address['telephone'],
                            $sync_customer['id'],
                            $address_type,
                            $address['entity_id']);

                        // Array for sync_operations
                        $updated_customer_new_addresses_ids[] = $address['entity_id'];
                    } elseif (
                        $sync_address['name'] !== $address['firstname'] ||
                        $sync_address['street'] !== $address['street'] ||
                        $sync_address['city'] !== $address['city'] ||
                        $sync_address['region'] !== $address['region'] ||
                        $sync_address['country'] !== $address['country_id'] ||
                        $sync_address['zip'] !== $address['postcode'] ||
                        $sync_address['telephone'] !== $address['telephone'] ||
                        intval($sync_address['defecto']) !== $address_type
                    ) {
                        // Update address query
                        $ddl_query .= "UPDATE sync_direccions SET
                                        name = ?, 
                                        street = ?,
                                        city = ?,
                                        region = ?,
                                        country = ?,
                                        zip = ?,
                                        telephone = ?,
                                        defecto = ?
                                        WHERE magento_direccion_id = ?;";

                        array_push($params,
                            $address['firstname'],
                            $address['street'],
                            $address['city'],
                            $address['region'],
                            $address['country_id'],
                            $address['postcode'],
                            $address['telephone'],
                            $address_type,
                            $address['entity_id']);

                        // Array for sync_operations
                        $updated_customer_updated_addresses_ids[] = $address['entity_id'];
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'new_erp_customers' => $new_customers_from_erp,
                            'new_mg2_customers' => $magento_api_customers_to_sync,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Insert sync_operations with customer updates
            foreach ($updated_customers_ids as $id) {
                // Update customer synced info
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_clientes',
                    'id',
                    $id,
                    2,                  // Update
                    0,
                    0);
            }

            // New addresses in sync_operations
            foreach ($updated_customer_new_addresses_ids as $id) {
                $query_sync_addresses = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$id;
                $sync_address = $db->fetchAssoc($query_sync_addresses);

                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_direccions',
                    'id',
                    $sync_address['id'],
                    1,                  // New
                    0,
                    0);
            }

            // Updated addresses in sync_operations
            foreach ($updated_customer_updated_addresses_ids as $id) {
                $query_sync_addresses = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$id;
                $sync_address = $db->fetchAssoc($query_sync_addresses);

                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_direccions',
                    'id',
                    $sync_address['id'],
                    2,                  // Update
                    0,
                    0);
            }


            // Execute SQL sync_operations
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'new_erp_customers' => $new_customers_from_erp,
                            'new_mg2_customers' => $magento_api_customers_to_sync,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync reward points
            $query = "SELECT DISTINCT 
                          sync.dni AS sync_nif,
                          sync.magento_cliente_id AS mag_id,
                          sync.`name` AS name,
                          pc.xnif AS nif,
                          pc.xdomicilio AS domicilio,
                          pc.xcod_postal AS cpostal,
                          pc.xpoblacion AS poblacion,
                          pc.xprovincia_id AS provincia_id,
                          mpc.xpuntos AS puntos
                      FROM sync_clientes sync
                      LEFT JOIN mag_usuarios mu ON mu.xusuario_id = sync.email
                      LEFT JOIN pc_clientes pc ON mu.xcliente_id = pc.xcliente_id
                      LEFT JOIN mag_puntos_cliente mpc ON mu.xcliente_id = mpc.xcliente_id;";
            $erp_points_customers = $db->fetchAll($query);

            foreach ($erp_points_customers as $erp_points_customer) {
                $api_response = $client->get('rewards/customer/'.$erp_points_customer['mag_id'].'/transactions', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $status->getToken()
                    ]
                ]);
                $customer_transactions = json_decode($api_response->getBody()->getContents());

                $total_transactions = 0;
                foreach ($customer_transactions as $transaction) {
                    $total_transactions += $transaction->amount;
                }

                $erp_points = $erp_points_customer['puntos'] ?
                    $erp_points_customer['puntos'] :
                    null;

                if (
                    $erp_points !== null &&
                    $erp_points !== $total_transactions
                ) {
                    // Create transaction to update points in Magento
                    $api_response = $client->post('rewards/transaction', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            "transaction" => [
                                "customer_id" => intval($erp_points_customer['mag_id']),
                                "amount" => $erp_points - $total_transactions,
                                "comment" => "Puntos actualizados desde el ERP",
                                "code" => "erp-update-1",
                                "is_expired" => 0,
                                "is_expiration_email_sent" => 0,
                                "expires_at" => null
                            ]
                        ]
                    ]);
                    $transaction_result = json_decode($api_response->getBody()->getContents());
                }
            }

            $response->setData(array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'new_erp_customers' => $new_customers_from_erp,
                    'new_mg2_customers' => $magento_api_customers_to_sync,
                    'message' => count($errors) === 0 ?
                        'Customers synchronized successfully' :
                        'Customers synchronized with errors')
            );
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/ekon/sync-orders")
     */
    public function syncOrdersAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'ekon') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get orders data from ERP
        $db = $this->get('doctrine.dbal.integration_connection');
        $db_mg2 = $this->get('doctrine.dbal.mg2_connection');

        $query = "SELECT * FROM sync_pedidos";
        $erp_orders = array();
        $erp_orders_db = $db->fetchAll($query);
        if (is_array($erp_orders_db)) {
            foreach ($erp_orders_db as $order) {
                $query = "SELECT * FROM sync_lineas WHERE sync_pedido_id = " . $order['id'];
                $lines = $db->fetchAll($query);
                $order['lines'] = $lines;
                $erp_orders[] = $order;
            }
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Orders could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'new_orders' => [],
                    'message' => $errors[0])
            );
        } else {
            // Get Guzzle client
            $client = new Client(['base_uri' => $uri]);

            // New orders to sync
            $new_orders = [];

            $post_promises = [];
            $pending_orders = [];           // Pending orders to be synced of new customers
            $new_customer_addresses = [];   // New addresses for existing customers

            // Orders already synced
            $already_sync_orders = [];

            // Get current Magento orders
            $query = "SELECT * FROM sales_order";
            $api_orders = $db_mg2->fetchAll($query);
            $db_mg2->close();

            // Get Magento customers
            $query_api_customers = "SELECT * FROM customer_entity";
            $magento_customers = $db_mg2->fetchAll($query_api_customers);
            $pending_registered_customers = [];

            // Get DNI attribute info
            $query_dni_attribute = "SELECT * FROM eav_attribute WHERE attribute_code = 'dni'";
            $dni_attribute = $db_mg2->fetchAssoc($query_dni_attribute);

            // Sync orders
            $ddl_query = "";
            $params = [];
            $customer_points_transactions = [];

            foreach ($api_orders as $api_order) {
                $mag_order_id = $api_order['entity_id'];
                $mag_order_increment_id = '#'.$api_order['increment_id'];
                $query_order = "SELECT * FROM sync_pedidos sp 
                                LEFT JOIN pl_hpedcli_cab hp ON sp.magento_orderid = hp.xreferencia 
                                WHERE sp.magento_pedido_id = ".$mag_order_id.";";
                $order_synced = $db->fetchAssoc($query_order);
                $db->close();

                if (!$order_synced) {
                    // Save new API order
                    $new_orders[] = $api_order;

                    // Get customer from order
                    $query_customer = "SELECT * FROM sync_clientes sc 
                                                LEFT JOIN mag_usuarios ma ON ma.xusuario_id = sc.email
                                                WHERE sc.email LIKE '".$api_order['customer_email']."'";
                    $customer = $db->fetchAssoc($query_customer);

                    // Get addresses from order
                    $query_addresses = "SELECT * FROM sales_order_address WHERE parent_id = ".$api_order['entity_id'].";";
                    $order_addresses = $db_mg2->fetchAll($query_addresses);
                    $db_mg2->close();

                    // Get billing and shipping addresses from order
                    $api_order_addresses = [];
                    foreach ($order_addresses as $address) {
                        if ($address['address_type'] === 'billing') {
                            $api_order_addresses['billing'] = $address;
                        }
                        if ($address['address_type'] === 'shipping') {
                            $api_order_addresses['shipping'] = $address;
                        }
                    }

                    // Get customer synchronized addresses ids
                    if ($customer) {
                        $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = ".$customer['id'];
                        $customer_addresses = $db->fetchAll($query_customer_addresses);

                        // Check if a default address already exists
                        $defecto_exists = false;
                        foreach ($customer_addresses as $customer_address) {
                            if (isset($customer_address['defecto']) === 3) {
                                $defecto_exists = true;
                            }
                        }

                        $customer_billing_address_id = 0;
                        $customer_shipping_address_id = 0;
                        foreach ($customer_addresses as $customer_address) {
                            $mag_id_address = intval($customer_address['magento_direccion_id']);

                            if (
                                isset($api_order_addresses['billing']['entity_id']) &&
                                $mag_id_address === intval($api_order_addresses['billing']['entity_id'])
                            ) {
                                $customer_billing_address_id = intval($customer_address['id']);
                            } elseif (
                                $customer_address['name'] === $api_order_addresses['billing']['firstname'] &&
                                $customer_address['street'] === $api_order_addresses['billing']['street'] &&
                                $customer_address['city'] === $api_order_addresses['billing']['city'] &&
                                $customer_address['region'] === $api_order_addresses['billing']['region'] &&
                                $customer_address['country'] === $api_order_addresses['billing']['country_id'] &&
                                $customer_address['zip'] === $api_order_addresses['billing']['postcode'] &&
                                $customer_address['telephone'] === $api_order_addresses['billing']['telephone']
                            ) {
                                $customer_billing_address_id = intval($customer_address['id']);
                            }

                            if (
                                isset($api_order_addresses['shipping']['entity_id']) &&
                                $mag_id_address === intval($api_order_addresses['shipping']['entity_id'])
                            ) {
                                $customer_shipping_address_id = intval($customer_address['id']);
                            } elseif (
                                isset($api_order_addresses['shipping']) && // Shipping address could not be defined in the order
                                $customer_address['name'] === $api_order_addresses['shipping']['firstname'] &&
                                $customer_address['street'] === $api_order_addresses['shipping']['street'] &&
                                $customer_address['city'] === $api_order_addresses['shipping']['city'] &&
                                $customer_address['region'] === $api_order_addresses['shipping']['region'] &&
                                $customer_address['country'] === $api_order_addresses['shipping']['country_id'] &&
                                $customer_address['zip'] === $api_order_addresses['shipping']['postcode'] &&
                                $customer_address['telephone'] === $api_order_addresses['shipping']['telephone']
                            ) {
                                $customer_shipping_address_id = intval($customer_address['id']);
                            }
                        }

                        // Sync new addresses
                        $ddl_query_addresses = "";
                        $params_query_addresses = [];
                        if ($customer_billing_address_id === 0) {
                            // Insert new address query
                            $ddl_query_addresses .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            array_push($params_query_addresses,
                                $api_order_addresses['billing']['firstname'],
                                $api_order_addresses['billing']['street'],
                                $api_order_addresses['billing']['city'],
                                isset($api_order_addresses['billing']['region']) ? $api_order_addresses['billing']['region'] : "-",
                                $api_order_addresses['billing']['country_id'],
                                $api_order_addresses['billing']['postcode'],
                                $api_order_addresses['billing']['telephone'],
                                $customer['id'],
                                isset($api_order_addresses['shipping']) ?
                                    1 :
                                    ($defecto_exists ? 0 : 3),
                                $api_order_addresses['billing']['entity_id']);

                            $new_customer_addresses[$customer['email'].':billing'] = $api_order_addresses['billing'];
                        }

                        if ($customer_shipping_address_id === 0) {
                            // Insert new address query
                            $ddl_query_addresses .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            array_push($params_query_addresses,
                                $api_order_addresses['shipping']['firstname'],
                                $api_order_addresses['shipping']['street'],
                                $api_order_addresses['shipping']['city'],
                                isset($api_order_addresses['shipping']['region']) ? $api_order_addresses['shipping']['region'] : "-",
                                $api_order_addresses['shipping']['country_id'],
                                $api_order_addresses['shipping']['postcode'],
                                $api_order_addresses['shipping']['telephone'],
                                $customer['id'],
                                2,
                                $api_order_addresses['shipping']['entity_id']);

                            $new_customer_addresses[$customer['email'].':shipping'] = $api_order_addresses['shipping'];
                        }

                        // Execute SQL
                        if ($ddl_query_addresses !== "") {
                            try {
                                $db->executeQuery($ddl_query_addresses, $params_query_addresses);
                                $db->close();
                            } catch (DBALException $exception) {
                                $response->setData(array(
                                        'success' => false,
                                        'new_orders' => $new_orders,
                                        'message' => $exception->getMessage())
                                );

                                return $response;
                            }
                        }

                        if ($customer_billing_address_id === 0) {
                            $query_address = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$api_order_addresses['billing']['entity_id'];
                            $address = $db->fetchAssoc($query_address);

                            if ($address) {
                                $customer_billing_address_id = $address['id'];
                            }
                        }

                        if ($customer_shipping_address_id === 0) {
                            $query_address = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$api_order_addresses['shipping']['entity_id'];
                            $address = $db->fetchAssoc($query_address);

                            if ($address) {
                                $customer_shipping_address_id = $address['id'];
                            }
                        }

                        // Get transaction reward points
                        if (!array_key_exists($customer['email'], $customer_points_transactions)) {
                            $api_response = $client->get('rewards/customer/'.$customer['magento_cliente_id'].'/transactions', [
                                'headers' => [
                                    'Authorization' => 'Bearer ' . $status->getToken()
                                ]
                            ]);
                            $customer_points_transactions[$customer['email']] = json_decode($api_response->getBody()->getContents(), true);
                        }

                        $spent_points = 0;
                        foreach ($customer_points_transactions[$customer['email']] as $transaction) {
                            if (strpos($transaction['comment'], $mag_order_increment_id) !== false) {
                                $spent_points += $transaction['amount'];
                            }
                        }

                        // Get comments
                        $comments = "";
                        $query_order_history = "SELECT * FROM sales_order_status_history WHERE parent_id = ".$api_order['entity_id'].";";
                        $order_history = $db_mg2->fetchAll($query_order_history);
                        foreach ($order_history as $status_history) {
                            $comments .= $status_history['comment']." | ";
                        }

                        // Get payment
                        $payment_method = "";
                        $query_payment = "SELECT * FROM sales_order_payment WHERE parent_id = ".$api_order['entity_id'].";";
                        $payment = $db_mg2->fetchAssoc($query_payment);
                        if ($payment) {
                            $payment_method = $payment['method'];
                        }

                        // We only store orders with the following conditions
                        if (
                            $payment_method !== 'banktransfer' ||
                            $payment_method !== 'phoenix_cashondelivery' ||
                            ($payment_method === 'banktransfer' && $api_order['status'] === 'processing') ||
                            ($payment_method === 'phoenix_cashondelivery' && $api_order['status'] === 'processing')
                        ) {
                            $ddl_query .= "INSERT INTO sync_pedidos (xcliente_id, email, sync_cliente_id, sync_direccion_facturacion_id, sync_direccion_envio_id, metodo_pago, metodo_envio, estado, gastos_envio, subtotal, subtotal_base, subtotal_impuestos, magento_pedido_id, magento_orderid, comentarios, fecha, comentarios_pedido, dni, recargoPaypal, puntos_gastados, puntos_ganados, contrarreembolso, descuento, cupon_id, cupon_tipo, cupon_descuento) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

                            // Get coupon
                            $coupon = false;
                            if (isset($api_order['coupon_code'])) {
                                $query_coupon = "SELECT * FROM salesrule WHERE name = '".$api_order['coupon_code']."';";
                                $coupon = $db_mg2->fetchAssoc($query_coupon);
                            }
                            $db_mg2->close();

                            array_push($params,
                                isset($customer['xcliente_id']) ? $customer['xcliente_id'] : '',
                                $api_order['customer_email'],
                                $customer['id'],
                                $customer_billing_address_id > 0 ? $customer_billing_address_id : $customer_shipping_address_id,
                                $customer_shipping_address_id > 0 ? $customer_shipping_address_id : $customer_billing_address_id,
                                $payment_method,
                                isset($api_order['shipping_description']) ?
                                    $api_order['shipping_description']:
                                    "Sin descripción de envío",
                                $api_order['status'],
                                $api_order['shipping_incl_tax'],
                                $api_order['grand_total'],
                                floatval($api_order['grand_total']) - floatval($api_order['tax_amount']),
                                $api_order['tax_amount'],
                                $api_order['entity_id'],
                                $api_order['increment_id'],
                                $comments,
                                $api_order['created_at'],
                                $comments,
                                $customer['dni'] ? $customer['dni'] : "",
                                0,
                                abs($spent_points),
                                0,
                                isset($api_order['cod_fee_incl_tax']) ?
                                    $api_order['cod_fee_incl_tax'] :
                                    0,
                                $api_order['base_discount_amount'],
                                $coupon ?
                                    $coupon['name'] :
                                    "",
                                $coupon ? (
                                $coupon['simple_action'] === 'by_percent' ?
                                    1 :
                                    0
                                ) : null,
                                $coupon ?
                                    $coupon['discount_amount'] :
                                    0
                            );
                        }
                    } else {
                        // Save pending order to later processing
                        $random_pass = "Rnd_" . Shared::randomCode(8);
                        $pending_orders[$api_order['customer_email'].':'.$random_pass] = $api_order;

                        // Check if user is already registered in Magento
                        $registered = false;
                        $registered_customer = false;
                        foreach ($magento_customers as $magento_customer) {
                            if ($magento_customer['email'] == $api_order['customer_email']) {
                                $registered = true;
                                $registered_customer = $magento_customer;
                                break;
                            }
                        }

                        // Register customer
                        if (!$registered) {
                            // Check if a promise with the new customer info is already pending
                            // in order not to create it again (imagine multiple orders sent before sync)
                            $already_sync_customer = false;
                            foreach ($post_promises as $email_pass => $promise) {
                                $email_pass = explode(':', $email_pass);
                                $email = $email_pass[0];

                                if ($email == $api_order['customer_email']) {
                                    $already_sync_customer = true;
                                }
                            }

                            // Ensure that there is a shipping address
                            if (!isset($api_order_addresses['shipping'])) {
                                $api_order_addresses['shipping'] = $api_order_addresses['billing'];
                            }

                            if (
                                !$already_sync_customer &&
                                isset($api_order_addresses['billing']['region_id']) &&
                                isset($api_order_addresses['shipping']['region_id'])
                            ) {
                                $post_promises[$api_order['customer_email'].':'.$random_pass] = $client->postAsync('customers', [
                                    'headers' => [
                                        'Authorization' => 'Bearer ' . $status->getToken()
                                    ],
                                    'json' => [
                                        'customer' => [
                                            // Add customer field data.
                                            'email' => $api_order['customer_email'],
                                            'firstname' => $api_order_addresses['billing']['firstname'],
                                            'lastname' => $api_order_addresses['billing']['lastname'],
                                            'addresses' => [
                                                [
                                                    'defaultShipping' => false,
                                                    'defaultBilling' => true,
                                                    'firstname' => $api_order_addresses['billing']['firstname'],
                                                    'lastname' => $api_order_addresses['billing']['lastname'],
                                                    'postcode' => $api_order_addresses['billing']['postcode'],
                                                    'street' => [
                                                        $api_order_addresses['billing']['street']
                                                    ],
                                                    'city' => $api_order_addresses['billing']['city'],
                                                    'telephone' => $api_order_addresses['billing']['telephone'],
                                                    'fax' => isset($api_order_addresses['billing']['fax']) ? $api_order_addresses['billing']['fax']: '',
                                                    'countryId' => $api_order_addresses['billing']['country_id'],
                                                    'regionId' => $api_order_addresses['billing']['region_id']
                                                ],
                                                [
                                                    'defaultShipping' => true,
                                                    'defaultBilling' => false,
                                                    'firstname' => $api_order_addresses['shipping']['firstname'],
                                                    'lastname' => $api_order_addresses['shipping']['lastname'],
                                                    'postcode' => $api_order_addresses['shipping']['postcode'],
                                                    'street' => [
                                                        $api_order_addresses['shipping']['street']
                                                    ],
                                                    'city' => $api_order_addresses['shipping']['city'],
                                                    'telephone' => $api_order_addresses['shipping']['telephone'],
                                                    'fax' => isset($api_order_addresses['shipping']['fax']) ? $api_order_addresses['shipping']['fax']: '',
                                                    'countryId' => $api_order_addresses['shipping']['country_id'],
                                                    'regionId' => $api_order_addresses['shipping']['region_id']
                                                ]
                                            ],
                                            'custom_attributes' => [
                                                [
                                                    'attribute_code' => 'customer_points',
                                                    'value' => '0'
                                                ],
                                                [
                                                    'attribute_code' => 'dni',
                                                    'value' => ''
                                                ]
                                            ]
                                        ],
                                        'password' => $random_pass
                                    ]
                                ]);
                            }
                        } else {
                            // Store registered customer that does not exist in sync_clientes to sync it later
                            if (!in_array($registered_customer, $pending_registered_customers)) {
                                $pending_registered_customers[] = $registered_customer;
                            }
                        }
                    }
                } else {
                    // Check if order status changed
                    if ($order_synced['estado'] !== $api_order['status']) {
                        // Update order if status changed
                        $ddl_query .= "UPDATE sync_pedidos SET estado = ? WHERE id = " . $order_synced['id'] . ";";
                        array_push($params, $api_order['status']);

                        // At the same time we insert the new operations into sync_operations
                        $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            'sync_pedidos',
                            'id',
                            $order_synced['id'],
                            2,                  // Update
                            0,
                            0);
                    }

                    // Check if ERP order is completed
                    if (
                        intval($order_synced['axcompletado']) === -1 &&
                        $order_synced['estado'] !== 'complete'
                    ) {
                        // Change order status to processing
                        try {
                            $post_order_status_response = $client->post('orders', [
                                'headers' => [
                                    'Authorization' => 'Bearer ' . $status->getToken()
                                ],
                                'json' => [
                                    'entity' => [
                                        'entity_id' => intval($order_synced['magento_pedido_id']),
                                        'increment_id' => $order_synced['magento_orderid'],
                                        'status' => 'complete'
                                    ]
                                ]
                            ]);

                            // Update orders in ERP database
                            if ($post_order_status_response->getStatusCode() === 200) {
                                $ddl_query .= "UPDATE sync_pedidos SET estado = ? WHERE magento_pedido_id = " . $order_synced['magento_pedido_id'];
                                array_push($params,
                                    'complete'
                                );
                            }
                        } catch (ClientException $e) {
                            $error_response = $e->getResponse();
                            $errors['ERROR setting status to completed for Order ID ' . $order_synced['magento_pedido_id']] = $error_response->getBody()->getContents();
                        }
                    }
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync registered customers that does not exist in sync_clientes
            foreach ($pending_registered_customers as $pending_registered_customer) {
                // Get DNI
                $dni = "";
                $query_dni = "SELECT * FROM customer_entity_varchar WHERE attribute_id = ".$dni_attribute['attribute_id']." AND entity_id = ".$pending_registered_customer['entity_id'].";";
                $dni_info = $db_mg2->fetchAssoc($query_dni);
                $db_mg2->close();
                if ($dni_info) {
                    $dni = $dni_info['value'];
                }

                // Insert pending customer
                $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                array_push(
                    $params,
                    $pending_registered_customer['firstname'].' '.$pending_registered_customer['lastname'],
                    $pending_registered_customer['email'],
                    "",
                    $dni,
                    $pending_registered_customer['group_id'],
                    $pending_registered_customer['entity_id']
                );
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $db->close();
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync addresses of registered customers that does not exist in sync_clientes
            foreach ($pending_registered_customers as $pending_registered_customer) {
                // Get customer info to be checked
                $query_api_addresses = "SELECT * FROM customer_address_entity WHERE parent_id = ".$pending_registered_customer['entity_id'].";";
                $addresses = $db_mg2->fetchAll($query_api_addresses);
                $db_mg2->close();

                $id_default_shipping = intval($pending_registered_customer['default_shipping']);
                $id_default_billing = intval($pending_registered_customer['default_billing']);

                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$pending_registered_customer['entity_id'];
                $sync_customer = $db->fetchAssoc($query_sync_customer);
                $db->close();

                // Create SQL INSERTS for addresses
                foreach ($addresses as $address) {
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $address['firstname'],
                        $address['street'],
                        $address['city'],
                        $address['region'],
                        $address['country_id'],
                        $address['postcode'],
                        $address['telephone'],
                        $sync_customer['id'],
                        ($id_default_shipping === intval($address['entity_id'])) ? 2 : (($id_default_billing === intval($address['entity_id'])) ? 1 : 0), // 0 -> none, 1 -> billing, 2 -> shipping
                        $address['entity_id']);
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Send new customers post promises
            $post_results = Promise\settle($post_promises)->wait();
            $fulfilled_responses = [];

            foreach ($post_results as $email_pass => $post_response) {
                $email_pass = explode(':', $email_pass);
                $email = $email_pass[0];
                $password = $email_pass[1];
                if ($post_response['state'] === "fulfilled") {
                    $result = json_decode($post_response['value']->getBody()->getContents());
                    $fulfilled_responses[$email] = $result;
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                    $dni = "";
                    // DNI does not exist in checkout order
                    array_push($params, $result->firstname.' '.$result->lastname, $result->email, $password, $dni, $result->group_id, $result->id);
                } else {
                    $errors['ERROR creating new customer from order with email address ' . $email] = $post_response['reason']->getMessage();
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new customers addresses
            foreach ($fulfilled_responses as $email => $result) {
                $addresses = $result->addresses;
                $id_default_shipping = intval($result->default_shipping);
                $id_default_billing = intval($result->default_billing);

                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$result->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);
                $db->close();

                // Create SQL INSERTS for addresses
                foreach ($addresses as $address) {
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $address->firstname,
                        $address->street[0],
                        $address->city,
                        $address->region->region,
                        $address->country_id,
                        $address->postcode,
                        $address->telephone,
                        $sync_customer['id'],
                        ($id_default_shipping === $address->id) ? 2 : (($id_default_billing === $address->id) ? 1 : 0), // 0 -> none, 1 -> billing, 2 -> shipping
                        $address->id);
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new customers orders
            foreach ($pending_orders as $email_pass => $pending_order) {
                $email_pass = explode(':', $email_pass);
                $email = $email_pass[0];
                $mag_order_increment_id = '#'.$pending_order['increment_id'];

                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE email LIKE '".$email."'";
                $sync_customer = $db->fetchAssoc($query_sync_customer);
                $db->close();

                // Get customer addresses
                if ($sync_customer) {
                    $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = " . $sync_customer['id'];
                    $customer_addresses = $db->fetchAll($query_customer_addresses);
                    $db->close();

                    $customer_billing_address_id = 0;
                    $customer_shipping_address_id = 0;
                    foreach ($customer_addresses as $customer_address) {
                        $defecto = intval($customer_address['defecto']);
                        if ($defecto === 1) {
                            $customer_billing_address_id = $customer_address['id'];
                        }

                        if ($defecto === 2) {
                            $customer_shipping_address_id = $customer_address['id'];
                        }

                        if ($defecto === 3) {
                            $customer_billing_address_id = $customer_address['id'];
                            $customer_shipping_address_id = $customer_address['id'];
                        }
                    }

                    // Get transaction reward points
                    if (!array_key_exists($sync_customer['email'], $customer_points_transactions)) {
                        $api_response = $client->get('rewards/customer/' . $sync_customer['magento_cliente_id'] . '/transactions', [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $status->getToken()
                            ]
                        ]);
                        $customer_points_transactions[$sync_customer['email']] = json_decode($api_response->getBody()->getContents(), true);
                    }

                    $spent_points = 0;
                    foreach ($customer_points_transactions[$sync_customer['email']] as $transaction) {
                        if (strpos($transaction['comment'], $mag_order_increment_id) !== false) {
                            $spent_points += $transaction['amount'];
                        }
                    }

                    // Get comments
                    $comments = "";
                    $query_order_history = "SELECT * FROM sales_order_status_history WHERE parent_id = " . $pending_order['entity_id'] . ";";
                    $order_history = $db_mg2->fetchAll($query_order_history);
                    foreach ($order_history as $status_history) {
                        $comments .= $status_history['comment'] . " | ";
                    }

                    // Get payment
                    $payment_method = "";
                    $query_payment = "SELECT * FROM sales_order_payment WHERE parent_id = " . $pending_order['entity_id'] . ";";
                    $payment = $db_mg2->fetchAssoc($query_payment);
                    if ($payment) {
                        $payment_method = $payment['method'];
                    }

                    // Get coupon
                    $coupon = false;
                    if (isset($pending_order['coupon_code'])) {
                        $query_coupon = "SELECT * FROM salesrule WHERE name = '" . $pending_order['coupon_code'] . "';";
                        $coupon = $db_mg2->fetchAssoc($query_coupon);
                    }
                    $db_mg2->close();

                    // We only store orders with the following conditions
                    if (
                        $payment_method !== 'banktransfer' ||
                        $payment_method !== 'phoenix_cashondelivery' ||
                        ($payment_method === 'banktransfer' && $pending_order['status'] === 'processing') ||
                        ($payment_method === 'phoenix_cashondelivery' && $pending_order['status'] === 'processing')
                    ) {
                        $ddl_query .= "INSERT INTO sync_pedidos (xcliente_id, email, sync_cliente_id, sync_direccion_facturacion_id, sync_direccion_envio_id, metodo_pago, metodo_envio, estado, gastos_envio, subtotal, subtotal_base, subtotal_impuestos, magento_pedido_id, magento_orderid, comentarios, fecha, comentarios_pedido, dni, recargoPaypal, puntos_gastados, puntos_ganados, contrarreembolso, descuento, cupon_id, cupon_tipo, cupon_descuento) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

                        array_push($params,
                            isset($sync_customer['xcliente_id']) ? $sync_customer['xcliente_id'] : '',
                            $pending_order['customer_email'],
                            $sync_customer['id'],
                            $customer_billing_address_id > 0 ? $customer_billing_address_id : $customer_shipping_address_id,
                            $customer_shipping_address_id > 0 ? $customer_shipping_address_id : $customer_billing_address_id,
                            $payment_method,
                            isset($pending_order['shipping_description']) ?
                                $pending_order['shipping_description'] :
                                "Sin descripción de envío",
                            $pending_order['status'],
                            $pending_order['shipping_incl_tax'],
                            $pending_order['grand_total'],
                            floatval($pending_order['grand_total']) - floatval($pending_order['tax_amount']),
                            $pending_order['tax_amount'],
                            $pending_order['entity_id'],
                            $pending_order['increment_id'],
                            $comments,
                            $pending_order['created_at'],
                            $comments,
                            $sync_customer['dni'] ? $sync_customer['dni'] : "",
                            0,
                            abs($spent_points),
                            0,
                            isset($pending_order['cod_fee_incl_tax']) ?
                                $pending_order['cod_fee_incl_tax'] :
                                0,
                            $pending_order['base_discount_amount'],
                            $coupon ?
                                $coupon['name'] :
                                "",
                            $coupon ? (
                            $coupon['simple_action'] === 'by_percent' ?
                                1 :
                                0
                            ) : null,
                            $coupon ?
                                $coupon['discount_amount'] :
                                0
                        );
                    }
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync order lines
            $check_stock_skus = [];
            $all_orders_pending = array_unique(array_merge($new_orders, $pending_orders), SORT_REGULAR);
            foreach ($all_orders_pending as $order_data) {
                // Get order items
                $query_order_items = "SELECT * FROM sales_order_item WHERE order_id = ".$order_data['entity_id'].";";
                $order_items = $db_mg2->fetchAll($query_order_items);
                $db_mg2->close();

                // Get order from ERP database
                $query_order = "SELECT * FROM sync_pedidos WHERE magento_pedido_id = ".$order_data['entity_id'];
                $order = $db->fetchAssoc($query_order);
                $db->close();

                foreach ($order_items as $item) {
                    // Only save virtual items with price
                    if (
                        $item['product_type'] === 'configurable' ||
                        $item['product_type'] === 'simple'
                    ) {
                        // Get $product_id
                        $product_id = null;
                        $item_id = $item['item_id'];
                        if ($item['product_type'] === 'configurable') {
                            $sku = $item['sku'];
                            foreach ($order_items as $order_item) {
                                if ($order_item['sku'] === $sku && $order_item['product_type'] === 'virtual') {
                                    $product_id = $order_item['product_id'];
                                    break;
                                }
                            }
                        }

                        if ($item['product_type'] === 'simple') {
                            $product_id = $item['product_id'];
                        }

                        $ddl_query .= "INSERT INTO sync_lineas (magento_producto_id, sync_pedido_id, cantidad, sku, precio, descuento, magento_item_order_id) VALUES (?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $product_id,
                            $order['id'],
                            $item['qty_ordered'],
                            $item['sku'],
                            floatval($item['price_incl_tax']),
                            floatval($item['discount_percent']),
                            $item_id
                        );

                        // Add SKU to array in order to hide products with no stock
                        $check_stock_skus[] = $item['sku'];
                    }
                }
            }

            // Hide products with no stock
            $query = "SELECT * FROM catalog_product_flat_1 catalog
                      JOIN cataloginventory_stock_item stock ON catalog.entity_id = stock.product_id
                      WHERE catalog.sku IN ('".join("','", $check_stock_skus)."')";
            $erp_products = $db_mg2->fetchAll($query);
            $db_mg2->close();

            $checked_skus = [];
            $update_results = [];
            foreach ($erp_products as $erp_product) {
                $keys = explode('-', $erp_product['sku']);
                $parent_sku = join('-', array_slice($keys,0, 3));

                if ($erp_product['type_id'] === 'virtual' && !in_array($parent_sku, $checked_skus)) {
                    $query_family = "SELECT * FROM catalog_product_flat_1 catalog
                              JOIN cataloginventory_stock_item stock ON catalog.entity_id = stock.product_id
                              WHERE catalog.sku LIKE '".$parent_sku."%' AND catalog.type_id = 'virtual'";
                    $family_products = $db_mg2->fetchAll($query_family);
                    $db_mg2->close();

                    $stock = false;
                    $backorders = false;
                    foreach ($family_products as $product) {
                        if (!$backorders && boolval($product['use_config_backorders'])) {
                            $backorders = true;
                            if (intval($product['qty']) <> 0) {
                                $stock = true;
                                break;
                            }
                        } elseif (intval($product['qty']) > 0) {
                            $stock = true;
                            break;
                        }
                    }

                    if (!$stock && !$backorders) {
                        $options = [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $status->getToken()
                            ],
                            'json' => [
                                'product' => [
                                    // Hide product
                                    'sku' => $parent_sku,
                                    "status" => 1,
                                    "visibility" => 1,
                                    "type_id" => "configurable",
                                ],
                                'saveOptions' => true
                            ]
                        ];

                        // Send POST request to update configurable product
                        try {
                            $update_results[$parent_sku] = $client->put('products/'.$parent_sku, $options);
                        } catch (ClientException $e) {
                            $error_response = $e->getResponse();
                            $errors['ERROR updating product with SKU ' . $parent_sku] = $error_response->getBody()->getContents();
                        }
                    }

                    $checked_skus[] = $parent_sku;
                }

                if ($erp_product['type_id'] === 'simple' &&
                    !in_array($erp_product['sku'], $checked_skus) &&
                    !boolval($product['use_config_backorders']) &&
                    $erp_product['qty'] == 0) {

                    $options = [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'product' => [
                                // Hide product
                                'sku' => $erp_product['sku'],
                                "status" => 1,
                                "visibility" => 1,
                                "type_id" => "simple",
                            ],
                            'saveOptions' => true
                        ]
                    ];

                    // Send POST request to update configurable product
                    try {
                        $update_results[$erp_product['sku']] = $client->put('products/'.$erp_product['sku'], $options);
                    } catch (ClientException $e) {
                        $error_response = $e->getResponse();
                        $errors['ERROR updating product with SKU ' . $parent_sku] = $error_response->getBody()->getContents();
                    }

                    $checked_skus[] = $erp_product['sku'];
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync pending customers into sync_operations
            foreach ($pending_registered_customers as $pending_registered_customer) {
                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = " . $pending_registered_customer['entity_id'];
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Get customer addresses
                $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = ".$sync_customer['id'].";";
                $sync_addresses = $db->fetchAll($query_customer_addresses);

                // Insert the new operations into sync_operations
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_clientes',
                    'id',
                    $sync_customer['id'],
                    1,                  // New
                    0,
                    0);

                // New addresses in sync_operations
                foreach ($sync_addresses as $sync_address) {
                    // Insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_direccions',
                        'id',
                        $sync_address['id'],
                        1,                  // New
                        0,
                        0);
                }
            }

            // Sync new customers into sync_operations
            foreach ($fulfilled_responses as $email => $result) {
                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = " . $result->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Get customer addresses
                $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = " . $sync_customer['id'];
                $sync_addresses = $db->fetchAll($query_customer_addresses);

                // Insert the new operations into sync_operations
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_clientes',
                    'id',
                    $sync_customer['id'],
                    1,                  // New
                    0,
                    0);

                // New addresses in sync_operations
                foreach ($sync_addresses as $sync_address) {
                    // Insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_direccions',
                        'id',
                        $sync_address['id'],
                        1,                  // New
                        0,
                        0);
                }
            }

            // Sync new addresses of existing customers into sync_operations
            foreach ($new_customer_addresses as $key => $new_customer_address) {
                // Get customer addresses
                $query_customer_addresses = "SELECT * FROM sync_direccions WHERE magento_direccion_id = " . $new_customer_address->entity_id;
                $sync_address = $db->fetchAssoc($query_customer_addresses);

                // Insert the new operations into sync_operations
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_direccions',
                    'id',
                    $sync_address['id'],
                    1,                  // New
                    0,
                    0);
            }

            // Sync orders and lines into sync_operations
            $orders_not_synced = [];
            foreach ($new_orders as $new_order) {
                // Get order from ERP database
                $query_order = "SELECT * FROM sync_pedidos WHERE magento_pedido_id = ".$new_order['entity_id'];
                $order = $db->fetchAssoc($query_order);

                if ($order) {
                    // At the same time we insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_pedidos',
                        'id',
                        $order['id'],
                        1,                  // New
                        0,
                        0);

                    // Get lines from ERP database
                    $query_lines = "SELECT * FROM sync_lineas WHERE sync_pedido_id = ".$order['id'];
                    $lines = $db->fetchAll($query_lines);

                    foreach ($lines as $line) {
                        // At the same time we insert the new operations into sync_operations
                        $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            'sync_lineas',
                            'id',
                            $line['id'],
                            1,                  // New
                            0,
                            0);
                    }
                } else {
                    $orders_not_synced[] = $new_order;
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_orders' => $new_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(
                array(
                    'success' => (count($errors) === 0) ? true : false,
                    'new_orders' => $new_orders,
                    'errors' => $errors,
                    'message' => (count($errors) === 0) ?
                        'Orders synchronized successfully' :
                        'Orders synchronized with errors'
                )
            );
        }

        return $response;
    }


    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/ekon/sync-invoices")
     */
    public function syncInvoicesAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'ekon') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get invoices data from ERP
        $db = $this->get('doctrine.dbal.integration_connection');
        $query = "SELECT
                    fcab.xnumdoc_id AS numdoc,
                    fcab.xcliente_id AS cliente_id,
                    fcab.xfecha AS fecha,
                    fcab.xseccion_id AS ejercicio,
                    fcab.xporc_dto AS dcto,
                    fcab.xbase_imp AS base_imp,
                    fcab.xcuota_impt AS impuestos,
                    fcab.xtotal_fra AS total,
                    sf.magento_factura_id AS mfid,
                    sp.magento_pedido_id AS mpid,
                    sp.magento_orderid AS mp_order_id,
                    sp.sync_direccion_envio_id AS env_id,
                    sp.sync_direccion_facturacion_id AS fac_id,
                    sp.metodo_envio AS metodo_envio,
                    sc.magento_cliente_id AS mag_cli_id
                  FROM pl_fracli_cab fcab
                  LEFT JOIN sync_pedidos sp ON fcab.xreferencia = sp.magento_orderid
                  LEFT JOIN sync_clientes sc ON sp.sync_cliente_id = sc.id
                  LEFT JOIN sync_facturas sf ON fcab.xnumdoc_id = sf.xnumdoc_id
                  WHERE fcab.xreferencia IS NOT NULL;";
        $erp_invoices = $db->fetchAll($query);

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Invoices could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'erp_invoices' => false,
                    'mg2_invoices' => false,
                    'message' => $errors[0])
            );
        } else {
            // Get current Magento invoices
            $client = new Client(['base_uri' => $uri]);
            // Get invoices after someone successfully purchases a product with their credit card
            $api_response = $client->request('GET', 'invoices?searchCriteria=', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_invoices = json_decode($api_response->getBody()->getContents())->items;

            $new_invoices = [];
            foreach ($erp_invoices as $erp_invoice) {
                if (!$erp_invoice['mfid']) {
                    $new_invoices[] = $erp_invoice;
                }
            }

            // Sync invoices
            $ddl_query = "";
            $params = [];
            $post_results = [];
            foreach ($new_invoices as $new_invoice) {
                $query_invoice_lines = "SELECT * FROM pl_fracli_lin WHERE xnumdoc_id = ".$new_invoice['numdoc'];
                $invoice_lines = $db->fetchAll($query_invoice_lines);
                $db->close();

                // Build items array for new invoice
                $items = [];
                foreach ($invoice_lines as $invoice_line) {
                    if ($invoice_line['xarticulo_id']) {
                        // Get product info
                        $query_product = "SELECT
                            ma.xarticulo_id AS art_id,
                            sp.xarticulo_id AS sync_art_id,
                            sp.sku AS sku,
                            sp.magento_producto_id AS mag_id,
                            sp.category_ids AS mag_category_ids,
                            pa.axdescripcion_web AS nombre,
                            pa.xdescripcion AS descripcion,
                            pa.xproducto AS producto,
                            pap.xprec_venta AS precio_venta,
                            pap.xprec_agrup AS precio_agrup,
                            pap.xprec_ttc AS precio_ttc,
                            palm.xexistencia AS total 
                          FROM mag_articulos ma 
                          JOIN pl_articulos pa ON ma.xarticulo_id = pa.xarticulo_id
                          LEFT JOIN sync_productos sp ON ma.xarticulo_id = sp.xarticulo_id
                          LEFT JOIN pl_artprec pap ON ma.xarticulo_id = pap.xarticulo_id
                          LEFT JOIN adv_existalm2 palm ON ma.xarticulo_id = palm.xarticulo_id
                          LEFT JOIN pl_operations ops ON SUBSTR(ops.valor,4) = ma.xarticulo_id
                          WHERE ma.xarticulo_id = " . $invoice_line['xarticulo_id'] . ";";
                        $product = $db->fetchAssoc($query_product);

                        // Get order line info
                        $query_line = "SELECT * FROM sync_lineas WHERE sku = '" . $product['sku'] . "' AND sync_pedido_id = " . $new_invoice['mpid'];
                        $line = $db->fetchAssoc($query_line);
                        $db->close();

                        // Get product id depending on the type of product
                        $product_id = null;
                        $sku_parts = explode('-', $product['sku']);
                        if (count($sku_parts) === 5) {
                            // Product is configurable
                            $db_mg2 = $this->get('doctrine.dbal.mg2_connection');
                            $parent_sku = $sku_parts[0].'-'.$sku_parts[1].'-'.$sku_parts[2];
                            $query_product = "SELECT * FROM catalog_product_flat_1 WHERE sku = '".$parent_sku."';";
                            $parent_product = $db_mg2->fetchAssoc($query_product);
                            $db_mg2->close();
                            $product_id = intval($parent_product['entity_id']);
                        } else {
                            // Product is simple
                            $product_id = intval($product['mag_id']);
                        }

                        // Add product in items array if found
                        if ($product) {
                            // Calculate discount
                            $discount_amount = 0.0;
                            $base_price = floatval($invoice_line['xprec_venta']);
                            $base_price_tax = round($base_price * 0.21, 4);
                            $amount = floatval($invoice_line['ximporte']);
                            $tax_amount = round($amount * 0.21, 4);

                            $dto1 = floatval($invoice_line['xporc_dto']);
                            $dto2 = floatval($invoice_line['xporc_dto2']);

                            $base_price_with_discount = $base_price;
                            if ($dto1 > 0) {
                                $discount_amount = round(($dto1 / $base_price) * 100, 4);
                                $base_price_with_discount = $base_price - $discount_amount;
                            }

                            if ($dto2 > 0) {
                                $discount_amount2 = round(($dto2 / $base_price_with_discount) * 100, 4);
                                $base_price_with_discount -= $discount_amount2;
                                $discount_amount += $discount_amount2;
                            }

                            // Create items for invoices
                            $items[] = [
                                /*
                                  "additional_data": "string",
                                  "base_cost": 0,
                                  "base_discount_amount": 0,
                                  "base_discount_tax_compensation_amount": 0,
                                  "base_price": 0,
                                  "base_price_incl_tax": 0,
                                  "base_row_total": 0,
                                  "base_row_total_incl_tax": 0,
                                  "base_tax_amount": 0,
                                  "description": "string",
                                  "discount_amount": 0,
                                  "entity_id": 0,
                                  "discount_tax_compensation_amount": 0,
                                  "name": "string",
                                  "parent_id": 0,
                                  "price": 0,
                                  "price_incl_tax": 0,
                                  "product_id": 0,
                                  "row_total": 0,
                                  "row_total_incl_tax": 0,
                                  "sku": "string",
                                  "tax_amount": 0,
                                  "extension_attributes": {},
                                  "order_item_id": 0,
                                  "qty": 0
                                 */
                                "base_price" => $base_price,
                                "base_price_incl_tax" => $base_price + $base_price_tax,
                                "base_discount_amount" => $discount_amount,
                                "base_discount_tax_compensation_amount" => round($discount_amount * 0.21, 4),
                                "base_row_total" => $amount,
                                "base_row_total_incl_tax" => $amount + $tax_amount,
                                "base_tax_amount" => $tax_amount,
                                "product_id" => $product_id,
                                "sku" => $product['sku'],
                                "name" => isset($product['nombre']) ? $product['nombre'] : $product['descripcion'],
                                "description" => $product['descripcion'],
                                "discount_amount" => $discount_amount,
                                "discount_tax_compensation_amount" => round($discount_amount * 0.21, 4),
                                "price" => $base_price,
                                "price_incl_tax" => $base_price + $tax_amount,
                                "order_item_id" => intval($line['magento_item_order_id']),
                                "qty" => intval($invoice_line['xcantidad_prin']),
                                "row_total" => $amount,
                                "row_total_incl_tax"=> $amount + $tax_amount,
                                "tax_amount" => $tax_amount
                            ];
                        }
                    }
                }

                // Get totals
                $total_qty = 0;
                foreach ($items as $item) {
                    $total_qty += $item['qty'];
                }
                $subtotal = floatval($new_invoice['base_imp']);
                $total_price = floatval($new_invoice['total']);
                $total_tax = floatval($new_invoice['impuestos']);

                // Get magento addresses ids
                $query = "SELECT * FROM sync_direccions WHERE id = ".$new_invoice['fac_id'];
                $billing_address = $db->fetchAssoc($query);
                $shipping_address = $billing_address;
                if ($new_invoice['fac_id'] !== $new_invoice['env_id']) {
                    $query = "SELECT * FROM sync_direccions WHERE id = ".$new_invoice['env_id'];
                    $shipping_address = $db->fetchAssoc($query);
                }
                $db->close();

                // Post invoice to magento
                $options = [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $status->getToken()
                    ],
                    'json' => [
                        'entity' => [
                            /*
                              "base_currency_code": "string",
                              "base_discount_amount": 0,
                              "base_grand_total": 0,
                              "base_discount_tax_compensation_amount": 0,
                              "base_shipping_amount": 0,
                              "base_shipping_discount_tax_compensation_amnt": 0,
                              "base_shipping_incl_tax": 0,
                              "base_shipping_tax_amount": 0,
                              "base_subtotal": 0,
                              "base_subtotal_incl_tax": 0,
                              "base_tax_amount": 0,
                              "base_total_refunded": 0,
                              "base_to_global_rate": 0,
                              "base_to_order_rate": 0,
                              "billing_address_id": 0,
                              "can_void_flag": 0,
                              "created_at": "string",
                              "discount_amount": 0,
                              "discount_description": "string",
                              "email_sent": 0,
                              "entity_id": 0,
                              "global_currency_code": "string",
                              "grand_total": 0,
                              "discount_tax_compensation_amount": 0,
                              "increment_id": "string",
                              "is_used_for_refund": 0,
                              "items": [],
                              "order_currency_code": "string",
                              "order_id": 0,
                              "shipping_address_id": 0,
                              "shipping_amount": 0,
                              "shipping_discount_tax_compensation_amount": 0,
                              "shipping_incl_tax": 0,
                              "shipping_tax_amount": 0,
                              "state": 0,
                              "store_currency_code": "string",
                              "store_id": 0,
                              "store_to_base_rate": 0,
                              "store_to_order_rate": 0,
                              "subtotal": 0,
                              "subtotal_incl_tax": 0,
                              "tax_amount": 0,
                              "total_qty": 0,
                              "transaction_id": "string",
                              "updated_at": "string",
                             */
                            "base_currency_code" => 'EUR',
                            "base_grand_total" => $total_price,
                            "base_discount_tax_compensation_amount" => 0,
                            "base_shipping_incl_tax" => 0,
                            "base_shipping_tax_amount" => 0,
                            "base_subtotal" => $subtotal,
                            "base_tax_amount" => $total_tax,
                            "base_to_global_rate" => 0,
                            "base_to_order_rate" => 1,
                            "billing_address_id" => intval($billing_address['magento_direccion_id']),
                            "can_void_flag" => 1,
                            "created_at" => $new_invoice['fecha'],
                            "discount_amount" => 0,
                            "discount_tax_compensation_amount" => 0,
                            "email_sent" => 1,
                            "grand_total" => $total_price,
                            "increment_id" => date("Y").'-'.$new_invoice['numdoc'],
                            "store_to_order_rate" => 0,
                            "order_id" => intval($new_invoice['mpid']),
                            "order_currency_code" => 'EUR',
                            "shipping_address_id" => intval(intval($shipping_address['magento_direccion_id'])),
                            "shipping_discount_tax_compensation_amount" => 0,
                            "shipping_incl_tax" =>  0,
                            "shipping_tax_amount" => 0,
                            "store_id" => 1,
                            "subtotal" => $subtotal,
                            "tax_amount" => $total_tax,
                            "total_qty" => $total_qty,
                            "items" => $items
                        ]
                    ]
                ];

                // Send POST request to create delivery note
                try {
                    $post_results[$new_invoice['numdoc']] = $client->post('invoices', $options);
                } catch (ClientException $e) {
                    $error_response = $e->getResponse();
                    $errors['ERROR creating invoice with numdoc ' . $new_invoice['numdoc']] = $error_response->getBody()->getContents();
                }
            }

            $post_order_status_responses = [];
            foreach ($post_results as $numdoc => $post_response) {
                // Insert invoice in synced invoices
                if ($post_response->getStatusCode() === 200) {
                    $result = json_decode($post_response->getBody()->getContents(), true);

                    $query = "SELECT * FROM sync_pedidos WHERE magento_pedido_id = ".$result['order_id'];
                    $synced_order = $db->fetchAssoc($query);
                    $db->close();

                    $ddl_query .= "INSERT INTO sync_facturas (magento_factura_id, magento_pedido_id, sync_pedido_id, xnumdoc_id) VALUES (?,?,?,?);";
                    array_push($params,
                        $result['entity_id'],
                        $result['order_id'],
                        $synced_order['id'],
                        $numdoc
                    );

                    // Change order status to processing
                    try {
                        $post_order_status_responses[$result['order_id']] = $client->post('orders', [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $status->getToken()
                            ],
                            'json' => [
                                'entity' => [
                                    'entity_id' => intval($result['order_id']),
                                    'increment_id' => $synced_order['magento_orderid'],
                                    'status'    => 'processing'
                                ]
                            ]
                        ]);
                    } catch (ClientException $e) {
                        $error_response = $e->getResponse();
                        $errors['ERROR setting status for Order ID '.$result['order_id'].' related to Invoice ' . $numdoc] = $error_response->getBody()->getContents();
                    }
                }
            }

            // Update orders in ERP database
            foreach ($post_order_status_responses as $order_id => $post_order_status_response) {
                if ($post_order_status_response->getStatusCode() === 200) {
                    $ddl_query .= "UPDATE sync_pedidos SET estado = ? WHERE magento_pedido_id = ".$order_id.";";
                    array_push($params,
                        'processing'
                    );
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'new_invoices' => $new_invoices,
                            'errors' => $errors,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(
                array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'new_invoices' => $new_invoices,
                    'message' => count($errors) === 0 ?
                        'Invoices synchronized successfully' :
                        'Invoices synchronized with errors'
                )
            );
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/ekon/sync-notes")
     */
    public function syncDeliveryNotesAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'ekon') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }


        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Invoices could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'new_notes' => [],
                    'message' => 'Delivery notes could not be synchronized.')
            );
        } else {
            // Get notes data from ERP
            $db = $this->get('doctrine.dbal.integration_connection');
            $query = "SELECT
                    acab.xnumdoc_id AS numdoc,
                    acab.xcliente_id AS cliente_id,
                    acab.xfecha_albaran AS fecha,
                    acab.xseccion_id AS ejercicio,
                    acab.ximporte AS importe,
                    acab.xporc_dto AS dcto,
                    acab.xreferencia AS spid,
                    sa.magento_albaran_id AS maid,
                    sp.magento_pedido_id AS mpid,
                    sp.magento_orderid AS mp_order_id,
                    sp.sync_direccion_envio_id AS env_id,
                    sp.sync_direccion_facturacion_id AS fac_id,
                    sp.metodo_envio AS metodo_envio,
                    sc.magento_cliente_id AS mag_cli_id
                  FROM pl_halbcli_cab acab
                  LEFT JOIN sync_pedidos sp ON acab.xreferencia = sp.magento_orderid
                  LEFT JOIN sync_clientes sc ON sp.sync_cliente_id = sc.id
                  LEFT JOIN sync_albaranes sa ON acab.xnumdoc_id = sa.xnumdoc_id
                  WHERE acab.xreferencia IS NOT NULL;";
            $erp_notes = $db->fetchAll($query);
            $db->close();

            // Get current Magento notes
            $client = new Client(['base_uri' => $uri]);
            // Get delivery notes (shipments)
            $api_response = $client->request('GET', 'shipments?searchCriteria=', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_notes = json_decode($api_response->getBody()->getContents())->items;

            $new_notes = [];
            foreach ($erp_notes as $erp_note) {
                if (!$erp_note['maid']) {
                    $new_notes[] = $erp_note;
                }
            }

            // Sync notes
            $ddl_query = "";
            $params = [];
            $post_results = [];
            foreach ($new_notes as $new_note) {
                $query_note_lines = "SELECT * FROM pl_halbcli_lin WHERE xnumdoc_id = " . $new_note['numdoc'];
                $note_lines = $db->fetchAll($query_note_lines);
                $db->close();

                // Build items array for new note
                $items = [];
                foreach ($note_lines as $note_line) {
                    if (
                        $note_line['xarticulo_id'] &&
                        intval($note_line['xcantidad_prin']) > 0
                    ) {
                        // Get product info
                        $query_product = "SELECT
                            ma.xarticulo_id AS art_id,
                            sp.xarticulo_id AS sync_art_id,
                            sp.sku AS sku,
                            sp.magento_producto_id AS mag_id,
                            sp.category_ids AS mag_category_ids,
                            pa.axdescripcion_web AS nombre,
                            pa.xdescripcion AS descripcion,
                            pa.xproducto AS producto,
                            pap.xprec_venta AS precio_venta,
                            pap.xprec_agrup AS precio_agrup,
                            pap.xprec_ttc AS precio_ttc,
                            palm.xexistencia AS total 
                          FROM mag_articulos ma 
                          JOIN pl_articulos pa ON ma.xarticulo_id = pa.xarticulo_id
                          LEFT JOIN sync_productos sp ON ma.xarticulo_id = sp.xarticulo_id
                          LEFT JOIN pl_artprec pap ON ma.xarticulo_id = pap.xarticulo_id
                          LEFT JOIN adv_existalm2 palm ON ma.xarticulo_id = palm.xarticulo_id
                          LEFT JOIN pl_operations ops ON SUBSTR(ops.valor,4) = ma.xarticulo_id
                          WHERE ma.xarticulo_id = " . $note_line['xarticulo_id'] . ";";
                        $product = $db->fetchAssoc($query_product);
                        // Get order line info
                        $query_line = "SELECT * FROM sync_lineas WHERE sku = '".$product['sku']."' AND sync_pedido_id = ".$new_note['mpid'];
                        $line = $db->fetchAssoc($query_line);
                        $db->close();

                        // Get product id depending on the type of product
                        $product_id = null;
                        $sku_parts = explode('-', $product['sku']);
                        if (count($sku_parts) === 5) {
                            // Product is configurable
                            $db_mg2 = $this->get('doctrine.dbal.mg2_connection');
                            $parent_sku = $sku_parts[0].'-'.$sku_parts[1].'-'.$sku_parts[2];
                            $query_product = "SELECT * FROM catalog_product_flat_1 WHERE sku = '".$parent_sku."';";
                            $parent_product = $db_mg2->fetchAssoc($query_product);
                            $db_mg2->close();
                            $product_id = intval($parent_product['entity_id']);
                        } else {
                            // Product is simple
                            $product_id = intval($product['mag_id']);
                        }

                        // Add product in items array if found
                        if ($product) {
                            $items[] = [
                                /*
                                    "additional_data": "string",
                                    "description": "string",
                                    "entity_id": 0,
                                    "name": "string",
                                    "parent_id": 0,
                                    "price": 0,
                                    "product_id": 0,
                                    "row_total": 0,
                                    "sku": "string",
                                    "weight": 0,
                                    "extension_attributes": {},
                                    "order_item_id": 0,
                                    "qty": 0
                                 */
                                "order_item_id" => intval($line['magento_item_order_id']),
                                "qty" => intval($note_line['xcantidad_prin'])
                            ];
                        }
                    } else {
                        if (!isset($description)) {
                            $description = $note_line['xdescripcion'];
                        }
                    }
                }

                // Get total items
                $total_qty = 0;
                foreach ($items as $item) {
                    $total_qty += $item['qty'];
                }

                // Get magento addresses ids
                $query = "SELECT * FROM sync_direccions WHERE id = ".$new_note['fac_id'];
                $billing_address = $db->fetchAssoc($query);
                $shipping_address = $billing_address;
                if ($new_note['fac_id'] !== $new_note['env_id']) {
                    $query = "SELECT * FROM sync_direccions WHERE id = ".$new_note['env_id'];
                    $shipping_address = $db->fetchAssoc($query);
                }
                $db->close();

                // Create POST request
                $options = [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $status->getToken()
                    ],
                    'json' => [
                        'entity' => [
                            /*
                                "billing_address_id": 0,
                                "created_at": "string",
                                "customer_id": 0,
                                "email_sent": 0,
                                "entity_id": 0,
                                "increment_id": "string",
                                "order_id": 0,
                                "packages": [
                                  {
                                    "extension_attributes": {}
                                  }
                                ],
                                "shipment_status": 0,
                                "shipping_address_id": 0,
                                "shipping_label": "string",
                                "store_id": 0,
                                "total_qty": 0,
                                "total_weight": 0,
                                "updated_at": "string",
                                "items": [],
                                "tracks": [
                                  {
                                    "order_id": 0,
                                    "created_at": "string",
                                    "entity_id": 0,
                                    "parent_id": 0,
                                    "updated_at": "string",
                                    "weight": 0,
                                    "qty": 0,
                                    "description": "string",
                                    "extension_attributes": {},
                                    "track_number": "string",
                                    "title": "string",
                                    "carrier_code": "string"
                                  }
                                ],
                                "comments": [
                                  {
                                    "is_customer_notified": 0,
                                    "parent_id": 0,
                                    "extension_attributes": {},
                                    "comment": "string",
                                    "is_visible_on_front": 0,
                                    "created_at": "string",
                                    "entity_id": 0
                                  }
                                ],
                                "extension_attributes": {}
                             */
                            "billing_address_id" => intval($billing_address['magento_direccion_id']),
                            "created_at" => $new_note['fecha'],
                            "updated_at" => $new_note['fecha'],
                            "customer_id" => $new_note['mag_cli_id'],
                            "increment_id" => date("Y").'-'.$new_note['numdoc'],
                            "order_id" => intval($new_note['mpid']),
                            "packages" => [],
                            "tracks" => [],
                            "comments" => [],
                            "shipping_address_id" => intval(intval($shipping_address['magento_direccion_id'])),
                            "shipping_label" => $new_note['metodo_envio'],
                            "store_id" => 1,
                            "total_qty" => $total_qty,
                            "total_weight" => 0,
                            "items" => $items
                        ]
                    ]
                ];

                // Send POST request to create delivery note
                try {
                    $post_results[$new_note['numdoc']] = $client->post('shipment', $options);
                } catch (ClientException $e) {
                    $error_response = $e->getResponse();
                    $errors['ERROR creating note with numdoc ' . $new_note['numdoc']] = $error_response->getBody()->getContents();
                }
            }

            $post_order_status_responses = [];
            foreach ($post_results as $numdoc => $post_response) {
                // Insert note in synced notes
                if ($post_response->getStatusCode() === 200) {
                    $result = json_decode($post_response->getBody()->getContents(), true);

                    $query = "SELECT * FROM sync_pedidos WHERE magento_pedido_id = ".$result['order_id'];
                    $synced_order = $db->fetchAssoc($query);
                    $db->close();

                    $ddl_query .= "INSERT INTO sync_albaranes (magento_albaran_id, magento_pedido_id, sync_pedido_id, xnumdoc_id) VALUES (?,?,?,?);";
                    array_push($params,
                        $result['entity_id'],
                        $result['order_id'],
                        $synced_order['id'],
                        $numdoc
                    );

                    // Change order status to processing
                    try {
                        $post_order_status_responses[$result['order_id']] = $client->post('orders', [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $status->getToken()
                            ],
                            'json' => [
                                'entity' => [
                                    'entity_id' => intval($result['order_id']),
                                    'increment_id' => $synced_order['magento_orderid'],
                                    'status'    => 'processing'
                                ]
                            ]
                        ]);
                    } catch (ClientException $e) {
                        $error_response = $e->getResponse();
                        $errors['ERROR setting status for Order ID '.$result['order_id'].' related to Delivery Note ' . $numdoc] = $error_response->getBody()->getContents();
                    }
                }
            }

            // Update orders in ERP database
            foreach ($post_order_status_responses as $order_id => $post_order_status_response) {
                if ($post_order_status_response->getStatusCode() === 200) {
                    $ddl_query .= "UPDATE sync_pedidos SET estado = ? WHERE magento_pedido_id = ".$order_id;
                    array_push($params,
                        'processing'
                    );
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                        'success' => false,
                        'errors' => $errors,
                        'new_notes' => $new_notes,
                        'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(
                array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'new_notes' => $new_notes,
                    'message' => count($errors) === 0 ?
                        'Delivery notes synchronized successfully' :
                        'Delivery notes synchronized with errors'
                )
            );
        }

        return $response;
    }
}
