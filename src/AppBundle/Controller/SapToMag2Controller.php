<?php

namespace AppBundle\Controller;

use Doctrine\DBAL\DBALException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EkonToMag2Controller
 * @package AppBundle\Controller
 */
class SapToMag2Controller extends Controller
{
    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/sap/sync-attributes")
     */
    public function syncAttributesAction() {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'sap') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Attributes could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success'           => false,
                    'errors'            => $errors,
                    'new_colors'            => false,
                    'new_uses'              => false,
                    'new_steel_types'       => false,
                    'new_handgrip_types'    => false,
                    'message'           => 'Attributes could not be synchronized')
            );
        } else {
            $db = $this->get('doctrine.dbal.integration_connection');
            $client = new Client(['base_uri' => $uri]);
            $post_promises = [];

            // Get current Magento colors
            $api_response = $client->request('GET', 'products/attributes/color/options', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_color_options = json_decode($api_response->getBody()->getContents(), true);

            // Get ERP colors
            $query = "SELECT * FROM ADVTIC_COLORES";
            $erp_colors = $db->fetchAll($query);

            // Get new colors
            $new_colors = [];
            foreach ($erp_colors as $erp_color) {
                $found = false;
                foreach ($api_color_options as $option) {
                    if (mb_strtolower(trim($erp_color['U_Nombre'])) === mb_strtolower(trim($option['label']))) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $new_color = ucwords(strtolower(trim($erp_color['U_Nombre'])));
                    $new_colors[] = $new_color;
                    $post_promises['color:'.$new_color] = $client->postAsync('products/attributes/color/options', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            "option" => [
                                "label" => $new_color,
                                "is_default" => false
                            ]
                        ]
                    ]);
                }
            }

            // Get current Magento uses
            $api_response = $client->request('GET', 'products/attributes/usos/options', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_uses_options = json_decode($api_response->getBody()->getContents(), true);

            // Get ERP uses
            $query = "SELECT * FROM ADVTIC_USOS";
            $erp_uses = $db->fetchAll($query);

            // Get new uses
            $new_uses = [];
            foreach ($erp_uses as $erp_use) {
                $found = false;
                foreach ($api_uses_options as $option) {
                    if (mb_strtolower(trim($erp_use['U_Nombre'])) === mb_strtolower(trim($option['label']))) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $new_use = ucwords(strtolower(trim($erp_use['U_Nombre'])));
                    $new_uses[] = $new_use;
                    $post_promises['usos:'.$new_use] = $client->postAsync('products/attributes/usos/options', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            "option" => [
                                "label" => $new_use,
                                "is_default" => false
                            ]
                        ]
                    ]);
                }
            }

            // Get current Magento handgrips
            $api_response = $client->request('GET', 'products/attributes/tipo_mango/options', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_handgrip_options = json_decode($api_response->getBody()->getContents(), true);

            // Get ERP handgrips
            $query = "SELECT * FROM ADVTIC_TIPO_MANGO";
            $erp_handgrips = $db->fetchAll($query);

            // Get new handgrips
            $new_handgrips = [];
            foreach ($erp_handgrips as $erp_handgrip) {
                $found = false;
                foreach ($api_handgrip_options as $option) {
                    if (mb_strtolower(trim($erp_handgrip['U_Nombre'])) === mb_strtolower(trim($option['label']))) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $new_handgrip = ucwords(strtolower(trim($erp_handgrip['U_Nombre'])));
                    $new_handgrips[] = $new_handgrip;
                    $post_promises['tipo_mango:'.$new_handgrip] = $client->postAsync('products/attributes/tipo_mango/options', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            "option" => [
                                "label" => $new_handgrip,
                                "is_default" => false
                            ]
                        ]
                    ]);
                }
            }

            // Send post promises
            $post_results = Promise\settle($post_promises)->wait();

            foreach ($post_results as $attr_option => $post_response) {
                if ($post_response['state'] !== "fulfilled") {
                    $errors['ERROR adding attribute option ' . $attr_option] = $post_response['reason']->getMessage();
                }
            }

            $response->setData(array(
                    'success'           => count($errors) === 0 ? true : false,
                    'errors'            => $errors,
                    'new_colors'            => $new_colors,
                    'new_uses'              => $new_uses,
                    'new_steel_types'       => false,
                    'new_handgrip_types'    => $new_handgrips,
                    'message'           => 'Attributes synchronized succesfully')
            );
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/sap/sync-categories")
     */
    public function syncCategoriesAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'sap') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Categories could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'erp_categories_data' => false,
                    'mg2_categories' => false,
                    'message' => 'Categories could not be synchronized')
            );
        } else {
            $db = $this->get('doctrine.dbal.integration_connection');
            $client = new Client(['base_uri' => $uri]);

            /**
             * DELETE CATEGORIES **********************************************
             */

            // Get ERP categories to delete
            $query = "SELECT
                        sc.id AS id,
                        sc.magento_categoria_id AS mag_id,
                        sc.xcategoria_id AS erp_id,
                        sc.name AS name,
                        mc.U_CategoriaId AS erp_real_id
                      FROM sync_categorias sc
                      LEFT JOIN ADVTIC_MAG_CATEG mc ON sc.xcategoria_id = mc.U_CategoriaId;";
            $erp_delete_categories = $db->fetchAll($query);

            $ddl_query = "";
            $params = [];
            $delete_promises = [];
            foreach ($erp_delete_categories as $erp_delete_category) {
                if (($erp_delete_category['erp_id'] !== null) && ($erp_delete_category['erp_real_id'] === null)) {
                    // Remove category from Magento
                    $delete_promises[strval($erp_delete_category['mag_id'])] = $client->deleteAsync('categories/' . $erp_delete_category['mag_id'], [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ]
                    ]);
                }
            }

            // Send delete promises
            $delete_results = Promise\settle($delete_promises)->wait();

            foreach ($delete_results as $mag_id => $delete_response) {
                if ($delete_response->getStatusCode() === 200) {
                    // Remove category from sync_categorias
                    $ddl_query .= "DELETE FROM sync_categorias WHERE magento_categoria_id = ?";
                    $params[] = $mag_id;
                }
            }

            // Execute SQL delete
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'erp_categories_data' => [],
                            'mg2_categories' => [],
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Get current Magento categories
            $api_response = $client->request('GET', 'categories', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_categories = json_decode($api_response->getBody()->getContents(), true);
            $root_id_category = $api_categories['id'];

            /**
             * NEW MAIN CATEGORIES **********************************************
             */

            $para_el_motorista_id = 3;
            $para_tu_moto_id = 4;

            // Get ERP categories to sync
            $query = "SELECT
                        mc.U_CategoriaId AS xcategoria_id,
                        mc.U_Descripcion AS xdescripcion,
                        mc.U_CatPadre AS xcat_padre,
                        sc.xcategoria_id AS erp_id
                      FROM ADVTIC_MAG_CATEG mc
                      LEFT JOIN sync_categorias sc ON mc.xcategoria_id = sc.xcategoria_id;";
            $erp_categories = $db->fetchAll($query);

            // Loop categories from ERP
            $post_promises = [];
            foreach ($erp_categories as $erp_category) {
                // Create new category in Magento
                if (
                    ($erp_category['erp_id'] === null) &&
                    ($erp_category['xcat_padre'] === null)
                ) {

                    $main_parent_id = $root_id_category;

                    $post_promises[$erp_category['xcategoria_id']] = $client->postAsync('categories', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'category' => [
                                'parent_id' => $main_parent_id,
                                'name' => $erp_category['xdescripcion'],
                                'is_active' => true,
                                'custom_attributes' => [
                                    [
                                        'attribute_code' => 'grid_activate',
                                        'value' => 0
                                    ]
                                ]
                            ],
                            'saveOptions' => true
                        ]
                    ]);
                }
            }

            // Send post promises
            $post_results = Promise\settle($post_promises)->wait();

            foreach ($post_results as $xcategoria_id => $post_response) {
                if ($post_response['state'] === "fulfilled") {
                    $result = json_decode($post_response['value']->getBody()->getContents());

                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_categorias (name, is_active, magento_parent_id, descripcion, xcategoria_id, magento_categoria_id, magento_categoria_id_outlet) 
                                        VALUES (?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $result->name,
                        true,
                        $result->parent_id,
                        $result->name,
                        $xcategoria_id,
                        $result->id,
                        0);
                } else {
                    $errors['ERROR creating category with ID ' . $xcategoria_id] = $post_response['reason']->getMessage();
                }
            }

            // Execute SQL delete
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'erp_categories_data' => $erp_categories,
                            'mg2_categories' => $api_categories,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            /**
             * NEW SUBCATEGORIES *********************************************
             */
            $finished = false;
            do {
                // Get ERP categories to sync
                $query = "SELECT
                            mc.xcategoria_id AS xcategoria_id,
                            mc.xdescripcion AS xdescripcion,
                            mc.xcat_padre AS xcat_padre,
                            sc.xcategoria_id AS erp_id,
                            sc.magento_categoria_id AS mag_id
                          FROM ADVTIC_MAG_CATEG mc
                          LEFT JOIN sync_categorias sc ON mc.xcategoria_id = sc.xcategoria_id;";
                $erp_categories = $db->fetchAll($query);

                // Loop categories from ERP
                $post_promises = [];
                $parent_not_found = 0;
                foreach ($erp_categories as $erp_category) {
                    // Create new category in Magento
                    if (
                        ($erp_category['erp_id'] === null) &&
                        ($erp_category['xcat_padre'] !== null)
                    ) {
                        /**
                         * We suppose that all the categories are already in Magento when we find a category
                         * with a parent.
                         */
                        $mag_parent_id = null;
                        foreach ($erp_categories as $erp_parent_category) {
                            if ($erp_category['xcat_padre'] === $erp_parent_category['xcategoria_id']) {
                                $mag_parent_id = $erp_parent_category['mag_id'];
                                break;
                            }
                        }

                        if ($mag_parent_id === null) {
                            $parent_not_found++;
                        } else {
                            $post_promises[$erp_category['xcategoria_id']] = $client->postAsync('categories', [
                                'headers' => [
                                    'Authorization' => 'Bearer ' . $status->getToken()
                                ],
                                'json' => [
                                    'category' => [
                                        'parent_id' => ($mag_parent_id !== null) ?
                                            $mag_parent_id :
                                            $root_id_category,
                                        'name' => $erp_category['xdescripcion'],
                                        'is_active' => true,
                                        'custom_attributes' => [
                                            [
                                                'attribute_code' => 'grid_activate',
                                                'value' => 0
                                            ]
                                        ]
                                    ],
                                    'saveOptions' => true
                                ]
                            ]);
                        }
                    }
                }

                // Send post promises
                $post_results = Promise\settle($post_promises)->wait();

                foreach ($post_results as $xcategoria_id => $post_response) {
                    if ($post_response['state'] === "fulfilled") {
                        $result = json_decode($post_response['value']->getBody()->getContents());
                        // Add insert query and params
                        $ddl_query .= "INSERT INTO sync_categorias (name, is_active, magento_parent_id, descripcion, xcategoria_id, magento_categoria_id, magento_categoria_id_outlet) 
                                        VALUES (?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $result->name,
                            true,
                            $result->parent_id,
                            $result->name,
                            $xcategoria_id,
                            $result->id,
                            0);
                    } else {
                        $errors['ERROR creating category with ID ' . $xcategoria_id] = $post_response['reason']->getMessage();
                    }
                }

                // Execute SQL delete
                if ($ddl_query !== "") {
                    try {
                        $db->executeQuery($ddl_query, $params);
                    } catch (DBALException $exception) {
                        $response->setData(array(
                                'success' => false,
                                'errors' => $errors,
                                'erp_categories_data' => $erp_categories,
                                'mg2_categories' => $api_categories,
                                'message' => $exception->getMessage())
                        );

                        return $response;
                    }
                }

                // Exit do ... while if parent not found is 0
                if ($parent_not_found === 0) {
                    $finished = true;
                }
            } while (!$finished);

            /**
             * UPDATE CATEGORIES **********************************************
             */

            // Get ERP categories to sync
            $ddl_query = "";
            $params = [];
            $query = "SELECT
                        mc.xcategoria_id AS xcategoria_id,
                        mc.xdescripcion AS xdescripcion,
                        mc.xcat_padre AS xcat_padre,
                        sc.xcategoria_id AS erp_id,
                        sc.magento_categoria_id AS mag_id
                      FROM ADVTIC_MAG_CATEG mc
                      LEFT JOIN sync_categorias sc ON mc.xcategoria_id = sc.xcategoria_id;";
            $erp_categories = $db->fetchAll($query);

            // Loop categories from ERP
            $update_promises = [];
            $mag_parent_id = null;
            foreach ($erp_categories as $erp_category) {
                if ($erp_category['xcategoria_id'] === $erp_category['erp_id']) {
                    // Get parent id
                    if ($erp_category['xcat_padre'] !== null) {
                        foreach ($erp_categories as $erp_parent_category) {
                            if ($erp_category['xcat_padre'] === $erp_parent_category['xcategoria_id']) {
                                $mag_parent_id = intval($erp_parent_category['mag_id']);
                                break;
                            }
                        }
                    } else {
                        $mag_parent_id = $root_id_category;
                    }

                    // Recursive function to find and update changed categories
                    $api_categories_find = function ($api_categories, $erp_categories, $erp_category, $client, $status, $root_id_category, $mag_parent_id) use (&$api_categories_find, &$update_promises) {
                        foreach ($api_categories as $api_category) {
                            if (isset($api_category['children_data'])) {
                                if ((count($api_category['children_data']) > 0)) {
                                    $api_categories_find($api_category['children_data'], $erp_categories, $erp_category, $client, $status, $root_id_category, $mag_parent_id);
                                }

                                if ($api_category['id'] === intval($erp_category['mag_id'])) {

                                    // Check name change
                                    if ($api_category['name'] !== $erp_category['xdescripcion']) {
                                        // Update category in Magento
                                        $update_promises[$erp_category['erp_id'] . ':info'] = $client->postAsync('categories', [
                                            'headers' => [
                                                'Authorization' => 'Bearer ' . $status->getToken()
                                            ],
                                            'json' => [
                                                'category' => [
                                                    'id' => $erp_category['mag_id'],
                                                    'name' => $erp_category['xdescripcion'],
                                                    'is_active' => true,
                                                    'custom_attributes' => [
                                                        [
                                                            'attribute_code' => 'grid_activate',
                                                            'value' => 0
                                                        ]
                                                    ]
                                                ],
                                                'saveOptions' => true
                                            ]
                                        ]);
                                    }

                                    // Check parent change
                                    if ($api_category['parent_id'] !== $mag_parent_id) {
                                        // Update category in Magento
                                        $update_promises[$erp_category['erp_id'] . ':parent_id:' . $mag_parent_id . ':' . $erp_category['mag_id']] = $client->putAsync('categories/' . $erp_category['mag_id'] . '/move', [
                                            'headers' => [
                                                'Authorization' => 'Bearer ' . $status->getToken()
                                            ],
                                            'json' => [
                                                'parentId' => $mag_parent_id,
                                                'afterId' => $erp_category['mag_id']
                                            ],
                                            'saveOptions' => true
                                        ]);
                                    }

                                    return true;
                                }
                            }
                        }

                        return false;
                    };

                    $api_categories_find($api_categories['children_data'], $erp_categories, $erp_category, $client, $status, $root_id_category, $mag_parent_id);
                }
            }

            // Send put promises
            $update_results = Promise\settle($update_promises)->wait();

            foreach ($update_results as $xcategoria_id_action => $update_response) {
                // Get xcategoria id and the update action
                $xcategoria_id_action_array = explode(':', $xcategoria_id_action);
                $xcategoria_id = $xcategoria_id_action_array[0];
                $update_reason = $xcategoria_id_action_array[1];

                if ($update_reason === 'parent_id') {
                    if ($update_response['state'] === "fulfilled") {
                        $new_parent_id = $xcategoria_id_action_array[2];
                        $mag_id = $xcategoria_id_action_array[3];

                        // Add insert query and params
                        $ddl_query .= "UPDATE sync_categorias SET magento_parent_id = ? WHERE magento_categoria_id = ?;";
                        array_push($params,
                            $new_parent_id,
                            $mag_id);
                    } else {
                        $errors['ERROR updating parent of category with ID ' . $xcategoria_id] = $update_response['reason']->getMessage();
                    }
                }

                if ($update_reason === 'info') {
                    if ($update_response['state'] === "fulfilled") {
                        $result = json_decode($update_response['value']->getBody()->getContents());
                        // Add insert query and params
                        $ddl_query .= "UPDATE sync_categorias 
                                        SET 
                                          name = ?, 
                                          is_active = ?,
                                          magento_parent_id = ?,
                                          descripcion = ?,
                                          xcategoria_id = ?,
                                          magento_categoria_id_outlet = ?
                                        WHERE
                                          magento_categoria_id = ?;";
                        array_push($params,
                            $result->name,
                            true,
                            $result->parent_id,
                            $result->name,
                            $xcategoria_id,
                            0,
                            $result->id);
                    } else {
                        $errors['ERROR updating info of category with ID ' . $xcategoria_id] = $update_response['reason']->getMessage();
                    }
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'erp_categories_data' => $erp_categories,
                            'mg2_categories' => $api_categories,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'erp_categories_data' => $erp_categories,
                    'mg2_categories' => $api_categories,
                    'message' => count($errors) === 0 ?
                        'Categories synchronized successfully' :
                        'Categories synchronized with errors')
            );
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @Route("/sap/sync-products")
     */
    public function syncProductsAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'sap') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Products could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'new_products' => 0,
                    'updated_products' => 0,
                    'message' => $errors[0])
            );
        } else {
            // Database and http client
            $db = $this->get('doctrine.dbal.integration_connection');
            $client = new Client(['base_uri' => $uri]);

            // Counters
            $new_count = 0;
            $updated_count = 0;

            // DDL query string and params array
            $ddl_query = "";
            $params = [];

            // Get ERP products
            $query = "SELECT
                        ma.ItemCode AS art_id,
                        ma.U_ADVTIC_Visible AS visible,
                        ma.U_ADVTIC_VisibleCont AS visible_cont,
                        ma.U_ADVTIC_Novedad AS novedad,
                        ma.U_ADVTIC_CategoriaId AS cat_id,
                        ma.U_ADVTIC_Serie AS serie_id,
                        col.U_Nombre AS color,
                        sp.xarticulo_id AS sync_art_id,
                        sp.magento_producto_id AS mag_id,
                        sp.existencias AS mag_existencias,
                        sp.category_ids AS mag_category_ids
                      FROM OITM ma 
                      /* LEFT JOIN mag_art_categorias mac ON ma.xarticulo_id = mac.xarticulo_id */
                      JOIN pl_articulos pa ON ma.xarticulo_id = pa.xarticulo_id
                      LEFT JOIN sync_productos sp ON ma.xarticulo_id = sp.xarticulo_id
                      LEFT JOIN ADVTIC_MAG_CATEG cat ON ma.U_ADVTIC_CategoriaId = cat.U_CategoriaId
                      LEFT JOIN ADVTIC_SERIES series ON ma.U_ADVTIC_Serie = series.U_CodigoId
                      LEFT JOIN ADVTIC_COLORES col ON ma.U_ADVTIC_Color = col.U_CodigoId
                      LEFT JOIN ADVTIC_TIPO_MANGO tmango ON ma.U_ADVTIC_TipoMango = tmango.U_CodigoId
                      LEFT JOIN ADVTIC_TIPO_ACERO tacero ON ma.U_ADVTIC_TipoAcero = tacero.U_CodigoId
                      ";
            $erp_products = $db->fetchAll($query);

            // Get Magento products
            $api_products = [];
            $products_response = $client->get('products?searchCriteria=', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);

            if ($products_response->getStatusCode() === 200) {
                $api_products = json_decode($products_response->getBody()->getContents())->items;
            } else {
                $errors[] = "ERROR: Could not get products from Magento. " . $products_response->getReasonPhrase();
            }

            // Sync products
            $post_promises = [];
            $post_results = [];
            $update_promises = [];
            $update_results = [];
            $count = 0;
            // Default image
            $default_image = $this->getParameter('uri_mg2') . "/pub/media/catalog/category/no_image.png";
            $base64 = base64_encode(file_get_contents($default_image));
            foreach ($erp_products as $erp_product) {
                // Check product is not already registered in Magento
                $product_magento = null;
                foreach ($api_products as $api_product) {
                    if ($api_product->sku === ($erp_product['producto'].'-'.$erp_product['art_id'].'-SKU')) {
                        $product_magento = $api_product;
                        break;
                    }
                }

                // Get product categories from ERP
                $query_categories = "SELECT sc.magento_categoria_id AS mag_id FROM sync_categorias sc JOIN mag_art_categorias mac ON sc.xcategoria_id = mac.xcategoria_id WHERE mac.xarticulo_id = '".$erp_product['art_id']."';";
                $product_categories = $db->fetchAll($query_categories);
                $category_ids_array = [];
                foreach ($product_categories as $product_category) {
                    $category_ids_array[] = $product_category['mag_id'];
                }
                $category_ids = join(',', $category_ids_array);

                // Get stock
                $stock = intval($erp_product["ext1"]) + intval($erp_product["ext2"]);
                $price = isset($erp_product['precio_venta']) ? $erp_product['precio_venta'] : 0.0;

                if (($erp_product['mag_id'] === null) && ($product_magento === null)) {
                    $new_sku = $erp_product['producto'].'-'.$erp_product['art_id'].'-SKU';

                    $post_promises[$new_sku] = $client->postAsync('products', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'product' => [
                                // Add product field data.
                                'sku'               => $new_sku,
                                "name"              => $erp_product['descripcion'],
                                "price"             => $price,
                                "status"            => 1,
                                "visibility"        => $erp_product['visible'],
                                "type_id"           => "simple",
                                "attribute_set_id"  => 4,           // Default Attribute Set is ID 4
                                "weight"            => 0,           // Not defined anywhere
                                "mediaGalleryEntries" => [
                                    [
                                        "mediaType" => "image",
                                        "label"     => "no_image",
                                        "types"     => [
                                            "image",
                                            "small_image",
                                            "thumbnail"
                                        ],
                                        "position"  => 0,
                                        "disabled"  => false,
                                        "file"      => $default_image,
                                        "content"   => [
                                            "base64EncodedData" => $base64,
                                            "type"              => "image/png",
                                            "name"              => "no_image.png"
                                        ]
                                    ]
                                ],
                                "extension_attributes" => [
                                    "stock_item"       => [
                                        "qty"           => $stock,
                                        "is_in_stock"   => ($stock > 0) ? true : false
                                    ]
                                ],
                                "custom_attributes" => [
                                    [
                                        "attribute_code"    => "category_ids",
                                        "value"             => $category_ids_array
                                    ],
                                    [
                                        "attribute_code"    => "url_key",
                                        "value"             => $erp_product['descripcion'].'-'.$erp_product['art_id']
                                    ]
                                ]
                            ],
                            'saveOptions' => true
                        ]
                    ]);
                } elseif (($erp_product['mag_id'] === null) && ($product_magento !== null)) {
                    // Add pending synced products to database
                    $keys = explode('-', $product_magento->sku);

                    $ddl_query .= "INSERT INTO sync_productos (name,short_description,weight,sku,sync_iva_id,existencias,status,visibility,is_in_stock,magento_producto_id,price,cost,descripcion,xarticulo_id,nuevo,promocion,producto_web,ficha_tecnica,category_ids) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $product_magento->name,                                                     // name
                        $product_magento->name,                                                     // short_description
                        0,                                                                          // weight
                        $product_magento->sku,                                                      // sku
                        0,                                                                          // sync_iva_id
                        $product_magento->extension_attributes->stock_item->qty,                    // existencias
                        $product_magento->status,                                                   // status
                        $product_magento->visibility,                                               // visibility
                        $product_magento->extension_attributes->stock_item->is_in_stock ? 1 : 0,    // is_in_stock
                        $product_magento->id,                                                       // magento_producto_id
                        $product_magento->price,                                                    // price
                        0,                                                                          // cost
                        "",                                                                         // descripcion
                        $keys[1],                                                                   // xarticulo_id
                        0,                                                                          // nuevo
                        0,                                                                          // promocion
                        0,                                                                          // producto_web
                        0,                                                                          // ficha_tecnica
                        $category_ids);                                                             // category_ids
                } elseif (($erp_product['mag_id'] !== null) && ($product_magento !== null)) {
                    // Get product category ids from Magento
                    $category_ids = explode(',', $erp_product['mag_category_ids']);

                    // Check if product info has changed
                    if (($erp_product['descripcion'] != $product_magento->name) ||
                        ($stock != $erp_product['mag_existencias']) ||
                        ($erp_product['visible'] != $product_magento->visibility) ||
                        ($price != $product_magento->price) ||
                        ($category_ids != $category_ids_array)) {
                        // Create PUT request
                        $update_promises[$product_magento->sku] = $client->postAsync('products', [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $status->getToken()
                            ],
                            'json' => [
                                'product' => [
                                    // Add product field data.
                                    "sku"               => $product_magento->sku,
                                    "name"              => $erp_product['descripcion'],
                                    "price"             => $price,
                                    "status"            => 1,
                                    "visibility"        => $erp_product['visible'],
                                    "type_id"           => "simple",
                                    "attribute_set_id"  => 4,           // Default Attribute Set is ID 4
                                    "weight"            => 0,           // Not defined anywhere
                                    "extension_attributes" => [
                                        "stock_item"       => [
                                            "qty"           => $stock,
                                            "is_in_stock"   => ($stock > 0) ? true : false
                                        ]
                                    ],
                                    "custom_attributes" => [
                                        [
                                            "attribute_code"    => "category_ids",
                                            "value"             => $category_ids_array
                                        ],
                                        [
                                            "attribute_code"    => "url_key",
                                            "value"             => $erp_product['descripcion'].'-'.$erp_product['art_id']
                                        ]
                                    ]
                                ],
                                'saveOptions' => true
                            ]
                        ]);
                    }
                }

                if ($count < 30) {
                    $count++;
                } else {
                    // Send post promises
                    $partial_results = Promise\settle($post_promises)->wait();
                    $post_results = array_merge($post_results, $partial_results);
                    $post_promises = [];
                    // Send put promises
                    $partial_results = Promise\settle($update_promises)->wait();
                    $update_results = array_merge($update_results, $partial_results);
                    $update_promises = [];
                }
            }

            foreach ($post_results as $sku => $post_response) {
                if ($post_response['state'] === "fulfilled") {
                    $new_count++;
                    $result = json_decode($post_response['value']->getBody()->getContents());
                    $keys = explode('-', $sku);

                    // Get product categories
                    $query_categories = "SELECT sc.magento_categoria_id AS mag_id FROM sync_categorias sc JOIN mag_art_categorias mac ON sc.xcategoria_id = mac.xcategoria_id WHERE mac.xarticulo_id = '".$keys[1]."';";
                    $product_categories = $db->fetchAll($query_categories);
                    $category_ids_array = [];
                    foreach ($product_categories as $product_category) {
                        $category_ids_array[] = $product_category['mag_id'];
                    }
                    $category_ids = join(',', $category_ids_array);

                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_productos (name,short_description,weight,sku,sync_iva_id,existencias,status,visibility,is_in_stock,magento_producto_id,price,cost,descripcion,xarticulo_id,nuevo,promocion,producto_web,ficha_tecnica,category_ids) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $result->name,                                                          // name
                        $result->name,                                                          // short_description
                        0,                                                                      // weight
                        $sku,                                                                   // sku
                        0,                                                                      // sync_iva_id
                        $result->extension_attributes->stock_item->qty,                         // existencias
                        $result->status,                                                        // status
                        $result->visibility,                                                    // visibility
                        $result->extension_attributes->stock_item->is_in_stock ? 1 : 0,         // is_in_stock
                        $result->id,                                                            // magento_producto_id
                        $result->price,                                                         // price
                        0,                                                                      // cost
                        "",                                                                     // descripcion
                        $keys[1],                                                               // xarticulo_id
                        0,                                                                      // nuevo
                        0,                                                                      // promocion
                        0,                                                                      // producto_web
                        0,                                                                      // ficha_tecnica
                        $category_ids);                                                         // category_ids
                } else {
                    $errors['ERROR creating product with SKU ' . $sku] = $post_response['reason']->getMessage();
                }
            }

            foreach ($update_results as $sku => $update_response) {
                if ($update_response['state'] === "fulfilled") {
                    $updated_count++;
                    $result = json_decode($update_response['value']->getBody()->getContents());

                    $keys = explode('-', $sku);

                    // Get product categories
                    $query_categories = "SELECT sc.magento_categoria_id AS mag_id FROM sync_categorias sc JOIN mag_art_categorias mac ON sc.xcategoria_id = mac.xcategoria_id WHERE mac.xarticulo_id = '".$keys[1]."';";
                    $product_categories = $db->fetchAll($query_categories);
                    $category_ids_array = [];
                    foreach ($product_categories as $product_category) {
                        $category_ids_array[] = $product_category['mag_id'];
                    }
                    $category_ids = join(',', $category_ids_array);

                    // Add insert query and params
                    $ddl_query .= "UPDATE sync_productos SET 
                                    name = ?,
                                    short_description = ?,
                                    sku = ?,
                                    existencias = ?,
                                    status = ?,
                                    visibility = ?,
                                    is_in_stock = ?,
                                    price = ?,
                                    descripcion = ?,
                                    category_ids = ?
                                    WHERE magento_producto_id = ?;";
                    array_push($params,
                        $result->name,                                                          // name
                        $result->name,                                                          // short_description
                        $sku,                                                                   // sku
                        $result->extension_attributes->stock_item->qty,                         // existencias
                        $result->status,                                                        // status
                        $result->visibility,                                                    // visibility
                        $result->extension_attributes->stock_item->is_in_stock ? 1 : 0,         // is_in_stock
                        $result->price,                                                         // price
                        "",                                                                     // descripcion
                        $category_ids,                                                          // category_ids
                        $result->id);                                                           // magento_producto_id
                } else {
                    $errors['ERROR updating product with SKU ' . $sku] = $update_response['reason']->getMessage();
                }
            }

            // Execute SQL delete
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'errors' => $errors,
                            'new_products' => $new_count,
                            'updated_products' => $updated_count,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'new_products' => $new_count,
                    'updated_products' => $updated_count,
                    'message' => count($errors) === 0 ?
                        'Products synchronized successfully' :
                        'Products synchronized with errors')
            );
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/sap/sync-customers")
     */
    public function syncCustomersAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'sap') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get customers and related data from ERP
        $db = $this->get('doctrine.dbal.integration_connection');
        $query = "SELECT DISTINCT 
                      mu.U_UsuarioId AS email,
                      mu.U_Password AS pass,
                      mu.U_Nombre AS nombre,
                      mu.U_Nombre AS apellidos,
                      pc.LicTradNum AS nif,
                      ple.Address AS env_domicilio,
                      ple.City AS env_poblacion,
                      ple.State AS env_provincia,
                      ple.Country AS env_country_code,
                      ple.ZipCode AS env_cpostal,
                      plf.Address AS fac_domicilio,
                      plf.City AS fac_poblacion,
                      plf.State AS fac_provincia,
                      plf.Country AS fac_country_code,
                      plf.ZipCode AS fac_cpostal,
                      sync.dni AS sync_nif,
                      sync.magento_cliente_id AS mag_id
                  FROM ADVTIC_MAG_USUARIOS mu
                  LEFT JOIN OCRD pc ON mu.U_ClienteId = pc.CardCode
                  LEFT JOIN CRD1 ple ON mu.U_ClienteId = ple.CardCode AND ple.AdresType LIKE 'S'
                  LEFT JOIN CRD1 plf ON mu.U_ClienteId = plf.CardCode AND plf.AdresType LIKE 'B'
                  LEFT JOIN sync_clientes sync ON mu.U_UsuarioId = sync.email;";
        $erp_customers = $db->fetchAll($query);

        // Get synced customers
        $query_sync_customers = "SELECT * FROM sync_clientes";
        $sync_customers = $db->fetchAll($query_sync_customers);

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Customers could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'errors' => $errors,
                    'erp_customers' => false,
                    'mg2_customers' => false,
                    'message' => $errors[0])
            );
        } else {
            // DDL query string and params array
            $ddl_query = "";
            $params = [];

            // Get current Magento customers
            $client = new Client(['base_uri' => $uri]);
            $api_response = $client->request('GET', 'customers/search?searchCriteria[sortOrders][0][field]=email&searchCriteria[sortOrders][0][direction]=asc', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_customers = json_decode($api_response->getBody()->getContents());

            // Get countries info
            $api_response = $client->request('GET', 'directory/countries', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_countries = [];
            if ($api_response->getStatusCode() === 200) {
                $api_countries = json_decode($api_response->getBody()->getContents());
            }

            // Sync new customers from ERP ###########################################################################
            $post_promises = [];
            foreach ($erp_customers as $erp_customer) {
                // Check user is not already registered in Magento
                $customer_magento = null;
                foreach ($api_customers->items as $api_customer) {
                    if ($api_customer->email === $erp_customer['email']) {
                        $customer_magento = $api_customer;
                        break;
                    }
                }

                if (($erp_customer['mag_id'] === null) && ($customer_magento === null)) {
                    // Get regions for each address
                    $customer_region = [];
                    $customer_region_env = [];
                    $customer_region_fac = [];

                    foreach ($api_countries as $country) {
                        if (strtolower($country->id) === strtolower($erp_customer['pais'])) {
                            if (isset($country->available_regions)) {
                                foreach ($country->available_regions as $region) {
                                    if ($region->name === $erp_customer['provincia']) {
                                        $customer_region['region'] = $region->name;
                                        $customer_region['region_id'] = $region->id;
                                        $customer_region['region_code'] = $region->code;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }

                    if (isset($erp_customer['env_provincia'])) {
                        foreach ($api_countries as $country) {
                            if (strtolower($country->id) === strtolower($erp_customer['pais'])) {
                                if (isset($country->available_regions)) {
                                    foreach ($country->available_regions as $region) {
                                        if ($region->name === $erp_customer['env_provincia']) {
                                            $customer_region_env['region'] = $region->name;
                                            $customer_region_env['region_id'] = $region->id;
                                            $customer_region_env['region_code'] = $region->code;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        $customer_region_env = $customer_region;
                    }

                    if (isset($erp_customer['fac_provincia'])) {
                        foreach ($api_countries as $country) {
                            if (strtolower($country->id) === strtolower($erp_customer['pais'])) {
                                if (isset($country->available_regions)) {
                                    foreach ($country->available_regions as $region) {
                                        if ($region->name === $erp_customer['fac_provincia']) {
                                            $customer_region_fac['region'] = $region->name;
                                            $customer_region_fac['region_id'] = $region->id;
                                            $customer_region_fac['region_code'] = $region->code;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        $customer_region_fac = $customer_region;
                    }

                    // Fix empty fields from database
                    if (!isset($erp_customer['apellidos']) || $erp_customer['apellidos'] === '') {
                        $erp_customer['apellidos'] = 'No info ERP';
                    }

                    $post_promises[$erp_customer['email'].':'.$erp_customer['pass']] = $client->postAsync('customers', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $status->getToken()
                        ],
                        'json' => [
                            'customer' => [
                                // Add customer field data.
                                'email' => $erp_customer['email'],
                                'firstname' => $erp_customer['nombre'],
                                'lastname' => $erp_customer['apellidos'],
                                'addresses' => [
                                    [
                                        'defaultShipping' => false,
                                        'defaultBilling' => false,
                                        'firstname' => $erp_customer['nombre'],
                                        'lastname' => $erp_customer['apellidos'],
                                        'postcode' => $erp_customer['cpostal'],
                                        'street' => [
                                            $erp_customer['domicilio']
                                        ],
                                        'city' => $erp_customer['poblacion'],
                                        'telephone' => $erp_customer['telefono'],
                                        'fax' => $erp_customer['fax'],
                                        'countryId' => $erp_customer['pais'],
                                        'region' => (count($customer_region) > 0) ? $customer_region : null
                                    ],
                                    [
                                        'defaultShipping' => true,
                                        'defaultBilling' => false,
                                        'firstname' => isset($erp_customer['env_nombre']) ? $erp_customer['env_nombre'] : $erp_customer['nombre'],
                                        'lastname' => isset($erp_customer['env_apellidos']) ? $erp_customer['env_apellidos'] : $erp_customer['apellidos'],
                                        'postcode' => isset($erp_customer['env_cpostal']) ? $erp_customer['env_cpostal'] : $erp_customer['cpostal'],
                                        'street' => [
                                            isset($erp_customer['env_domicilio']) ? $erp_customer['env_domicilio'] : $erp_customer['domicilio']
                                        ],
                                        'city' => isset($erp_customer['env_poblacion']) ? $erp_customer['env_poblacion'] : $erp_customer['poblacion'],
                                        'telephone' => isset($erp_customer['env_telefono']) ? $erp_customer['env_telefono'] : $erp_customer['telefono'],
                                        'fax' => isset($erp_customer['env_fax']) ? $erp_customer['env_fax'] : $erp_customer['fax'],
                                        'countryId' => isset($erp_customer['env_pais']) ? $erp_customer['env_pais'] : $erp_customer['pais'],
                                        'region' => (count($customer_region_env) > 0) ? $customer_region_env : null
                                    ],
                                    [
                                        'defaultShipping' => false,
                                        'defaultBilling' => true,
                                        'firstname' => isset($erp_customer['fac_nombre']) ? $erp_customer['fac_nombre'] : $erp_customer['nombre'],
                                        'lastname' => isset($erp_customer['fac_apellidos']) ? $erp_customer['fac_apellidos'] : $erp_customer['apellidos'],
                                        'postcode' => isset($erp_customer['fac_cpostal']) ? $erp_customer['fac_cpostal'] : $erp_customer['cpostal'],
                                        'street' => [
                                            isset($erp_customer['fac_domicilio']) ? $erp_customer['fac_domicilio'] : $erp_customer['domicilio']
                                        ],
                                        'city' => isset($erp_customer['fac_poblacion']) ? $erp_customer['fac_poblacion'] : $erp_customer['poblacion'],
                                        'telephone' => isset($erp_customer['fac_telefono']) ? $erp_customer['fac_telefono'] : $erp_customer['telefono'],
                                        'fax' => isset($erp_customer['fac_fax']) ? $erp_customer['fac_fax'] : $erp_customer['fax'],
                                        'countryId' => isset($erp_customer['fac_pais']) ? $erp_customer['fac_pais'] : $erp_customer['pais'],
                                        'region' => (count($customer_region_fac) > 0) ? $customer_region_fac : null
                                    ]
                                ],
                                'custom_attributes' => [
                                    [
                                        'attribute_code' => 'customer_points',
                                        'value' => isset($erp_customer['puntos']) ? $erp_customer['puntos'] : '0'
                                    ],
                                    [
                                        'attribute_code' => 'dni',
                                        'value' => isset($erp_customer['nif']) ? $erp_customer['nif'] : ''
                                    ]
                                ]
                            ],
                            'password' => $erp_customer['pass']
                        ]
                    ]);
                } elseif (($erp_customer['mag_id'] === null) && ($customer_magento !== null)) {
                    $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params, $customer_magento->firstname.' '.$customer_magento->lastname, $customer_magento->email, $erp_customer['pass'], $erp_customer['nif'], $customer_magento->group_id, $customer_magento->id);
                }
            }

            // Send post promises
            $post_results = Promise\settle($post_promises)->wait();
            $fulfilled_responses = [];

            foreach ($post_results as $email_pass => $post_response) {
                $email_pass = explode(':', $email_pass);
                $email = $email_pass[0];
                $password = $email_pass[1];
                if ($post_response['state'] === "fulfilled") {
                    $result = json_decode($post_response['value']->getBody()->getContents());
                    $fulfilled_responses[$email] = $result;
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                    $dni = "";
                    foreach ($erp_customers as $erp_customer) {
                        if ($erp_customer['email'] === $result->email) {
                            $dni = $erp_customer['nif'];
                            break;
                        }
                    }
                    array_push($params, $result->firstname.' '.$result->lastname, $result->email, $password, $dni, $result->group_id, $result->id);
                } else {
                    $errors['ERROR creating customer with email address ' . $email] = $post_response['reason']->getMessage();
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'erp_customers' => $erp_customers,
                            'mg2_customers' => $api_customers->items,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new addresses
            foreach ($fulfilled_responses as $email => $result) {
                $addresses = $result->addresses;
                if (count($addresses) > 0) {
                    $id_default_shipping = intval($result->default_shipping);
                    $id_default_billing = intval($result->default_billing);

                    // Get customer synchronized
                    $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = " . $result->id;
                    $sync_customer = $db->fetchAssoc($query_sync_customer);

                    // Create SQL INSERTS for addresses
                    foreach ($addresses as $address) {
                        // Add insert query and params
                        $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $address->firstname,
                            $address->street[0],
                            $address->city,
                            $address->region->region,
                            $address->country_id,
                            $address->postcode,
                            $address->telephone,
                            $sync_customer['id'],
                            ($id_default_shipping === $address->id) ? 2 : (($id_default_billing === $address->id) ? 1 : 0), // 0 -> none, 1 -> billing, 2 -> shipping
                            $address->id);
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'erp_customers' => $erp_customers,
                            'mg2_customers' => $api_customers->items,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync customers from Magento 2 ###########################################################################
            $magento_api_customers_not_found = [];
            $magento_api_customers_found = [];
            foreach ($api_customers->items as $api_customer) {
                $found = false;
                foreach ($sync_customers as $sync_customer) {
                    if ($api_customer->id === intval($sync_customer['magento_cliente_id'])) {
                        $magento_api_customers_found[] = $api_customer;
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $magento_api_customers_not_found[] = $api_customer;
                }
            }

            foreach ($magento_api_customers_not_found as $api_customer_not_found) {
                // Get DNI
                $dni = "";
                if (isset($api_customer_not_found->custom_attributes)) {
                    foreach ($api_customer_not_found->custom_attributes as $custom_attribute) {
                        if ($custom_attribute->attribute_code === "dni") {
                            $dni = $custom_attribute->value;
                            break;
                        }
                    }
                }

                // INSERT new customer
                $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params, $api_customer_not_found->firstname.' '.$api_customer_not_found->lastname, $api_customer_not_found->email, "-", $dni, $api_customer_not_found->group_id, $api_customer_not_found->id);
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'erp_customers' => $erp_customers,
                            'mg2_customers' => $api_customers->items,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new addresses from Magento users
            foreach ($magento_api_customers_not_found as $api_customer_not_found) {
                $addresses = $api_customer_not_found->addresses;
                if (count($addresses) > 0) {
                    $id_default_shipping = intval($api_customer_not_found->default_shipping);
                    $id_default_billing = intval($api_customer_not_found->default_billing);

                    // Get customer synchronized
                    $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$api_customer_not_found->id;
                    $sync_customer = $db->fetchAssoc($query_sync_customer);

                    // Create SQL INSERTS for addresses
                    foreach ($addresses as $address) {
                        // Add insert query and params
                        $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $address->firstname,
                            $address->street[0],
                            $address->city,
                            $address->region->region,
                            $address->country_id,
                            $address->postcode,
                            $address->telephone,
                            $sync_customer['id'],
                            ($id_default_shipping === $id_default_billing) ? 3 :
                                (($id_default_shipping === $address->id) ? 2 :
                                    ($id_default_billing === $address->id ? 1 : 0)), // 0 -> none, 1 -> billing, 2 -> shipping, 3 -> both
                            $address->id);
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'erp_customers' => $erp_customers,
                            'mg2_customers' => $api_customers->items,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new customers and their addresses in sync_operations
            foreach ($magento_api_customers_not_found as $api_customer_not_found) {
                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes sc
                                        LEFT JOIN mag_usuarios mu ON mu.xusuario_id = sc.email
                                        WHERE magento_cliente_id = ".$api_customer_not_found->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Check if customer already exists in ERP
                if (!$sync_customer['xusuario_id']) {
                    // Get customer addresses
                    $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = " . $sync_customer['id'];
                    $sync_addresses = $db->fetchAll($query_customer_addresses);

                    // Insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_clientes',
                        'id',
                        $sync_customer['id'],
                        1,                  // New
                        0,
                        0);

                    // New addresses in sync_operations
                    foreach ($sync_addresses as $sync_address) {
                        // Insert the new operations into sync_operations
                        $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            'sync_direccions',
                            'id',
                            $sync_address['id'],
                            1,                  // New
                            0,
                            0);
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'erp_customers' => $erp_customers,
                            'mg2_customers' => $api_customers->items,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Update customers from Magento 2 ###################################################################

            // Arrays for insert customer's addresses operations into sync_operations
            $updated_customers_ids = [];
            $updated_customer_new_addresses_ids = [];
            $updated_customer_updated_addresses_ids = [];

            foreach ($magento_api_customers_found as $api_customer_found) {
                $addresses = $api_customer_found->addresses;
                $id_default_shipping = 0;
                $id_default_billing = 0;
                if (count($addresses) > 0) {
                    $id_default_shipping = intval($api_customer_found->default_shipping);
                    $id_default_billing = intval($api_customer_found->default_billing);
                }

                // Get DNI
                $dni = "";
                if (isset($api_customer_found->custom_attributes)) {
                    foreach ($api_customer_found->custom_attributes as $custom_attribute) {
                        if ($custom_attribute->attribute_code === "dni") {
                            $dni = $custom_attribute->value;
                            break;
                        }
                    }
                }

                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$api_customer_found->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                if (
                    $sync_customer['name'] !== $api_customer_found->firstname.' '.$api_customer_found->lastname ||
                    $sync_customer['email'] !== $api_customer_found->email ||
                    $sync_customer['dni'] !== $dni ||
                    intval($sync_customer['sync_grupo_id']) !== $api_customer_found->group_id
                ) {
                    // Update customer synced info
                    $ddl_query .= "UPDATE sync_clientes SET 
                                    name = ?,
                                    email = ?,
                                    dni = ?,
                                    sync_grupo_id = ? 
                                    WHERE magento_cliente_id = ?;";
                    array_push($params,
                        $api_customer_found->firstname.' '.$api_customer_found->lastname,
                        $api_customer_found->email,
                        $dni,
                        $api_customer_found->group_id,
                        $api_customer_found->id);

                    $updated_customers_ids[] = $sync_customer['id'];
                }

                // Create or update addresses
                foreach ($addresses as $address) {
                    $query_sync_address = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$address->id;
                    $sync_address = $db->fetchAssoc($query_sync_address);

                    $defecto = ($id_default_shipping === $id_default_billing) ? 3 :
                        (($id_default_shipping === $address->id) ? 2 :
                            ($id_default_billing === $address->id ? 1 : 0)); // 0 -> none, 1 -> billing, 2 -> shipping, 3 -> both

                    if (!$sync_address) {
                        // Insert new address query
                        $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        array_push($params,
                            $address->firstname,
                            $address->street[0],
                            $address->city,
                            $address->region->region,
                            $address->country_id,
                            $address->postcode,
                            $address->telephone,
                            $sync_customer['id'],
                            ($id_default_shipping === $id_default_billing) ? 3 :
                                (($id_default_shipping === $address->id) ? 2 :
                                    ($id_default_billing === $address->id ? 1 : 0)), // 0 -> none, 1 -> billing, 2 -> shipping, 3 -> both
                            $address->id);

                        // Array for sync_operations
                        $updated_customer_new_addresses_ids[] = $address->id;
                    } elseif (
                        $sync_address['name'] !== $address->firstname ||
                        $sync_address['street'] !== $address->street[0] ||
                        $sync_address['city'] !== $address->city ||
                        $sync_address['region'] !== $address->region->region ||
                        $sync_address['country'] !== $address->country_id ||
                        $sync_address['zip'] !== $address->postcode ||
                        $sync_address['telephone'] !== $address->telephone ||
                        intval($sync_address['defecto']) !== $defecto
                    ) {
                        // Update address query
                        $ddl_query .= "UPDATE sync_direccions SET
                                        name = ?, 
                                        street = ?,
                                        city = ?,
                                        region = ?,
                                        country = ?,
                                        zip = ?,
                                        telephone = ?,
                                        defecto = ?
                                        WHERE magento_direccion_id = ?;";

                        array_push($params,
                            $address->firstname,
                            $address->street[0],
                            $address->city,
                            $address->region->region,
                            $address->country_id,
                            $address->postcode,
                            $address->telephone,
                            $defecto,
                            $address->id);

                        // Array for sync_operations
                        $updated_customer_updated_addresses_ids[] = $address->id;
                    }
                }
            }

            // Execute SQL customer inserts
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'erp_customers' => $erp_customers,
                            'mg2_customers' => $api_customers->items,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Insert sync_operations with customer updates
            foreach ($updated_customers_ids as $id) {
                // Update customer synced info
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_clientes',
                    'id',
                    $id,
                    2,                  // Update
                    0,
                    0);
            }

            // New addresses in sync_operations
            foreach ($updated_customer_new_addresses_ids as $id) {
                $query_sync_addresses = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$id;
                $sync_address = $db->fetchAssoc($query_sync_addresses);

                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_direccions',
                    'id',
                    $sync_address['id'],
                    1,                  // New
                    0,
                    0);
            }

            // Updated addresses in sync_operations
            foreach ($updated_customer_updated_addresses_ids as $id) {
                $query_sync_addresses = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$id;
                $sync_address = $db->fetchAssoc($query_sync_addresses);

                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_direccions',
                    'id',
                    $sync_address['id'],
                    2,                  // Update
                    0,
                    0);
            }


            // Execute SQL sync_operations
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => count($errors) === 0 ? true : false,
                            'errors' => $errors,
                            'erp_customers' => $erp_customers,
                            'mg2_customers' => $api_customers->items,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(array(
                    'success' => count($errors) === 0 ? true : false,
                    'errors' => $errors,
                    'erp_customers' => $erp_customers,
                    'mg2_customers' => $api_customers->items,
                    'message' => count($errors) === 0 ?
                        'Customers synchronized successfully' :
                        'Customers synchronized with errors')
            );
        }

        return $response;
    }

    /**
     * @return JsonResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/sap/sync-orders")
     */
    public function syncOrdersAction()
    {
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        // API URI
        $uri = $this->getParameter('uri_mg2') . '/index.php/rest/V1/';
        // JSON Response
        $response = new JsonResponse();
        // Errors array
        $errors = array();

        $new_orders = [];               // New orders to sync
        $post_promises = [];
        $pending_orders = [];           // Pending orders to be synced of new customers
        $new_customer_addresses = [];   // New addresses for existing customers

        // Check ERP defined in parameters
        if (ApiAccessController::getERP($this) !== 'sap') {
            $response->setData(array(
                    'success' => false,
                    'message' => 'ERROR: This route is not available with the current parameters.')
            );

            return $response;
        }

        // Get orders data from ERP
        $db = $this->get('doctrine.dbal.integration_connection');
        $query = "SELECT * FROM sync_pedidos";
        $erp_orders = array();
        $erp_orders_db = $db->fetchAll($query);
        if (is_array($erp_orders_db)) {
            foreach ($erp_orders_db as $order) {
                $query = "SELECT * FROM sync_lineas WHERE sync_pedido_id = " . $order['id'];
                $lines = $db->fetchAll($query);
                $order['lines'] = $lines;
                $erp_orders[] = $order;
            }
        }

        // Get token
        $status_code = ApiAccessController::refreshToken($this);
        if ($status_code != 200) {
            $errors['Token error'] = 'ERROR ' . $status_code . ': Orders could not be synchronized due to error getting token access.';
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
        }

        if (count($errors) > 0) {
            $response->setData(array(
                    'success' => false,
                    'erp_orders' => false,
                    'mg2_orders' => false,
                    'message' => $errors[0])
            );
        } else {
            // Get current Magento orders
            $client = new Client(['base_uri' => $uri]);
            // Get orders after someone successfully purchases a product with their credit card
            $api_response = $client->request('GET', 'orders?searchCriteria=', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_orders = json_decode($api_response->getBody()->getContents())->items;

            // Get current Magento customers
            $api_response = $client->request('GET', 'customers/search?searchCriteria[sortOrders][0][field]=email&searchCriteria[sortOrders][0][direction]=asc', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $status->getToken()
                ]
            ]);
            $api_customers = json_decode($api_response->getBody()->getContents());
            $magento_customers = $api_customers->items;
            $pending_registered_customers = [];

            // Sync orders
            $ddl_query = "";
            $params = [];

            foreach ($api_orders as $api_order) {
                $mag_order_id = $api_order->entity_id;
                $query_order = "SELECT * FROM sync_pedidos WHERE magento_pedido_id = ".$mag_order_id;
                $order_synced = $db->fetchAssoc($query_order);
                if (!$order_synced) {
                    // Save new API order
                    $new_orders[] = $api_order;

                    $query_customer = "SELECT * FROM sync_clientes sc 
                                                LEFT JOIN mag_usuarios ma ON ma.xusuario_id = sc.email
                                                WHERE sc.email LIKE '".$api_order->customer_email."'";
                    $customer = $db->fetchAssoc($query_customer);

                    // Get billing and shipping addresses from order
                    $api_order_addresses['billing'] = $api_order->billing_address;
                    $api_order_addresses['shipping'] = $api_order->extension_attributes->shipping_assignments[0]->shipping->address;

                    // Get customer synchronized addresses ids
                    if ($customer) {
                        $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = ".$customer['id'];
                        $customer_addresses = $db->fetchAll($query_customer_addresses);
                        $customer_billing_address_id = 0;
                        $customer_shipping_address_id = 0;

                        $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = ".$customer['id'];
                        $customer_addresses = $db->fetchAll($query_customer_addresses);
                        $customer_billing_address_id = 0;
                        $customer_shipping_address_id = 0;

                        foreach ($customer_addresses as $customer_address) {
                            $mag_id_address = intval($customer_address['magento_direccion_id']);

                            if ($mag_id_address === $api_order_addresses['billing']->entity_id) {
                                $customer_billing_address_id = intval($customer_address['id']);
                            } elseif (
                                $customer_address['name'] === $api_order_addresses['billing']->firstname &&
                                $customer_address['street'] === $api_order_addresses['billing']->street[0] &&
                                $customer_address['city'] === $api_order_addresses['billing']->city &&
                                $customer_address['region'] === $api_order_addresses['billing']->region_code &&
                                $customer_address['country'] === $api_order_addresses['billing']->country_id &&
                                $customer_address['zip'] === $api_order_addresses['billing']->postcode &&
                                $customer_address['telephone'] === $api_order_addresses['billing']->telephone
                            ) {
                                $customer_billing_address_id = intval($customer_address['id']);
                            }

                            if ($mag_id_address === $api_order_addresses['shipping']->entity_id) {
                                $customer_shipping_address_id = intval($customer_address['id']);
                            } elseif (
                                $customer_address['name'] === $api_order_addresses['shipping']->firstname &&
                                $customer_address['street'] === $api_order_addresses['shipping']->street[0] &&
                                $customer_address['city'] === $api_order_addresses['shipping']->city &&
                                $customer_address['region'] === $api_order_addresses['shipping']->region_code &&
                                $customer_address['country'] === $api_order_addresses['shipping']->country_id &&
                                $customer_address['zip'] === $api_order_addresses['shipping']->postcode &&
                                $customer_address['telephone'] === $api_order_addresses['shipping']->telephone
                            ) {
                                $customer_shipping_address_id = intval($customer_address['id']);
                            }
                        }

                        // Sync new addresses
                        $ddl_query_addresses = "";
                        $params_query_addresses = [];
                        if ($customer_billing_address_id === 0) {
                            // Insert new address query
                            $ddl_query_addresses .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            array_push($params_query_addresses,
                                $api_order_addresses['billing']->firstname,
                                $api_order_addresses['billing']->street[0],
                                $api_order_addresses['billing']->city,
                                $api_order_addresses['billing']->region_code,
                                $api_order_addresses['billing']->country_id,
                                $api_order_addresses['billing']->postcode,
                                $api_order_addresses['billing']->telephone,
                                $customer['id'],
                                1,
                                $api_order_addresses['billing']->entity_id);

                            $new_customer_addresses[$customer['email'].':billing'] = $api_order_addresses['billing'];
                        }

                        if ($customer_shipping_address_id === 0) {
                            // Insert new address query
                            $ddl_query_addresses .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            array_push($params_query_addresses,
                                $api_order_addresses['shipping']->firstname,
                                $api_order_addresses['shipping']->street[0],
                                $api_order_addresses['shipping']->city,
                                $api_order_addresses['shipping']->region_code,
                                $api_order_addresses['shipping']->country_id,
                                $api_order_addresses['shipping']->postcode,
                                $api_order_addresses['shipping']->telephone,
                                $customer['id'],
                                2,
                                $api_order_addresses['shipping']->entity_id);

                            $new_customer_addresses[$customer['email'].':shipping'] = $api_order_addresses['shipping'];
                        }

                        // Execute SQL
                        if ($ddl_query_addresses !== "") {
                            try {
                                $db->executeQuery($ddl_query_addresses, $params_query_addresses);
                            } catch (DBALException $exception) {
                                $response->setData(array(
                                        'success' => false,
                                        'erp_orders' => $erp_orders,
                                        'mg2_orders' => $api_orders,
                                        'message' => $exception->getMessage())
                                );

                                return $response;
                            }
                        }

                        if ($customer_billing_address_id === 0) {
                            $query_address = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$api_order_addresses['billing']->entity_id;
                            $address = $db->fetchAssoc($query_address);

                            if ($address) {
                                $customer_billing_address_id = $address['id'];
                            }
                        }

                        if ($customer_shipping_address_id === 0) {
                            $query_address = "SELECT * FROM sync_direccions WHERE magento_direccion_id = ".$api_order_addresses['shipping']->entity_id;
                            $address = $db->fetchAssoc($query_address);

                            if ($address) {
                                $customer_shipping_address_id = $address['id'];
                            }
                        }

                        $ddl_query .= "INSERT INTO sync_pedidos (xcliente_id, email, sync_cliente_id, sync_direccion_facturacion_id, sync_direccion_envio_id, metodo_pago, metodo_envio, estado, gastos_envio, subtotal, subtotal_base, magento_pedido_id, magento_orderid, comentarios, fecha, comentarios_pedido, dni, recargoPaypal) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

                        // Get comments
                        $comments = "";
                        foreach ($api_order->status_histories as $status_history) {
                            $comments .= $status_history->comment." | ";
                        }

                        array_push($params,
                            isset($customer['xcliente_id']) ? $customer['xcliente_id'] : '',
                            $api_order->customer_email,
                            $customer['id'],
                            $customer_billing_address_id,
                            $customer_shipping_address_id,
                            $api_order->payment->method,
                            $api_order->shipping_description,
                            $api_order->status,
                            $api_order->shipping_incl_tax,
                            $api_order->subtotal_incl_tax,
                            $api_order->base_subtotal,
                            $api_order->entity_id,
                            $api_order->increment_id,
                            $comments,
                            $api_order->created_at,
                            $comments,
                            $customer['dni'] ? $customer['dni'] : "",
                            0
                        );
                    } else {
                        // Save pending order to later processing
                        $random_pass = "Rnd_" . Shared::random_code(8);
                        $pending_orders[$api_order->customer_email.':'.$random_pass] = $api_order;

                        // Check if user is already registered in Magento
                        $registered = false;
                        $registered_customer = false;
                        foreach ($magento_customers as $magento_customer) {
                            if ($magento_customer->email == $api_order->customer_email) {
                                $registered = true;
                                $registered_customer = $magento_customer;
                                break;
                            }
                        }

                        // Register customer
                        if (!$registered) {
                            // Check if a promise with the new customer info is already pending
                            // in order not to create it again (imagine multiple orders sent before sync)
                            $already_sync_customer = false;
                            foreach ($post_promises as $email_pass => $promise) {
                                $email_pass = explode(':', $email_pass);
                                $email = $email_pass[0];

                                if ($email == $api_order->customer_email) {
                                    $already_sync_customer = true;
                                }
                            }

                            if (
                                !$already_sync_customer &&
                                isset($api_order_addresses['billing']->region_id) &&
                                isset($api_order_addresses['shipping']->region_id)
                            ) {
                                $post_promises[$api_order->customer_email.':'.$random_pass] = $client->postAsync('customers', [
                                    'headers' => [
                                        'Authorization' => 'Bearer ' . $status->getToken()
                                    ],
                                    'json' => [
                                        'customer' => [
                                            // Add customer field data.
                                            'email' => $api_order->customer_email,
                                            'firstname' => $api_order_addresses['billing']->firstname,
                                            'lastname' => $api_order_addresses['billing']->lastname,
                                            'addresses' => [
                                                [
                                                    'defaultShipping' => false,
                                                    'defaultBilling' => true,
                                                    'firstname' => $api_order_addresses['billing']->firstname,
                                                    'lastname' => $api_order_addresses['billing']->lastname,
                                                    'postcode' => $api_order_addresses['billing']->postcode,
                                                    'street' => [
                                                        $api_order_addresses['billing']->street[0]
                                                    ],
                                                    'city' => $api_order_addresses['billing']->city,
                                                    'telephone' => $api_order_addresses['billing']->telephone,
                                                    'fax' => isset($api_order_addresses['billing']->fax) ? $api_order_addresses['billing']->fax : '',
                                                    'countryId' => $api_order_addresses['billing']->country_id,
                                                    'regionId' => $api_order_addresses['billing']->region_id
                                                ],
                                                [
                                                    'defaultShipping' => true,
                                                    'defaultBilling' => false,
                                                    'firstname' => $api_order_addresses['shipping']->firstname,
                                                    'lastname' => $api_order_addresses['shipping']->lastname,
                                                    'postcode' => $api_order_addresses['shipping']->postcode,
                                                    'street' => [
                                                        $api_order_addresses['shipping']->street[0]
                                                    ],
                                                    'city' => $api_order_addresses['shipping']->city,
                                                    'telephone' => $api_order_addresses['shipping']->telephone,
                                                    'fax' => isset($api_order_addresses['shipping']->fax) ? $api_order_addresses['shipping']->fax : '',
                                                    'countryId' => $api_order_addresses['shipping']->country_id,
                                                    'regionId' => $api_order_addresses['shipping']->region_id
                                                ]
                                            ],
                                            'custom_attributes' => [
                                                [
                                                    'attribute_code' => 'customer_points',
                                                    'value' => '0'
                                                ],
                                                [
                                                    'attribute_code' => 'dni',
                                                    'value' => ''
                                                ]
                                            ]
                                        ],
                                        'password' => $random_pass
                                    ]
                                ]);
                            }
                        } else {
                            // Store registered customer that does not exist in sync_clientes to sync it later
                            $pending_registered_customers[] = $registered_customer;
                        }
                    }
                }
            }

            // Sync registered customers that does not exist in sync_clientes
            foreach ($pending_registered_customers as $pending_registered_customer) {
                $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                $dni = "";
                if (isset($pending_registered_customer->custom_attributes)) {
                    foreach ($pending_registered_customer->custom_attributes as $custom_attribute) {
                        if ($custom_attribute->attribute_code === "dni") {
                            $dni = $custom_attribute->value;
                            break;
                        }
                    }
                }
                array_push(
                    $params,
                    $pending_registered_customer->firstname.' '.$pending_registered_customer->lastname,
                    $pending_registered_customer->email,
                    "",
                    $dni,
                    $pending_registered_customer->group_id,
                    $pending_registered_customer->id
                );
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'erp_orders' => $erp_orders,
                            'mg2_orders' => $api_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync addresses of registered customers that does not exist in sync_clientes
            foreach ($pending_registered_customers as $pending_registered_customer) {
                $addresses = $pending_registered_customer->addresses;
                $id_default_shipping = intval($pending_registered_customer->default_shipping);
                $id_default_billing = intval($pending_registered_customer->default_billing);

                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$pending_registered_customer->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Create SQL INSERTS for addresses
                foreach ($addresses as $address) {
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $address->firstname,
                        $address->street[0],
                        $address->city,
                        $address->region->region,
                        $address->country_id,
                        $address->postcode,
                        $address->telephone,
                        $sync_customer['id'],
                        ($id_default_shipping === $address->id) ? 2 : (($id_default_billing === $address->id) ? 1 : 0), // 0 -> none, 1 -> billing, 2 -> shipping
                        $address->id);
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'erp_orders' => $erp_orders,
                            'mg2_orders' => $api_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Send new customers post promises
            $post_results = Promise\settle($post_promises)->wait();
            $fulfilled_responses = [];

            foreach ($post_results as $email_pass => $post_response) {
                $email_pass = explode(':', $email_pass);
                $email = $email_pass[0];
                $password = $email_pass[1];
                if ($post_response['state'] === "fulfilled") {
                    $result = json_decode($post_response['value']->getBody()->getContents());
                    $fulfilled_responses[$email] = $result;
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_clientes (name, email, password, dni, sync_grupo_id, magento_cliente_id) VALUES (?, ?, ?, ?, ?, ?);";
                    $dni = "";
                    // DNI does not exist in checkout order
                    array_push($params, $result->firstname.' '.$result->lastname, $result->email, $password, $dni, $result->group_id, $result->id);
                } else {
                    $errors['ERROR creating new customer from order with email address ' . $email] = $post_response['reason']->getMessage();
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'erp_orders' => $erp_orders,
                            'mg2_orders' => $api_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new customers addresses
            foreach ($fulfilled_responses as $email => $result) {
                $addresses = $result->addresses;
                $id_default_shipping = intval($result->default_shipping);
                $id_default_billing = intval($result->default_billing);

                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = ".$result->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Create SQL INSERTS for addresses
                foreach ($addresses as $address) {
                    // Add insert query and params
                    $ddl_query .= "INSERT INTO sync_direccions (name, street, city, region, country, zip, telephone, sync_cliente_id, defecto, magento_direccion_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $address->firstname,
                        $address->street[0],
                        $address->city,
                        $address->region->region,
                        $address->country_id,
                        $address->postcode,
                        $address->telephone,
                        $sync_customer['id'],
                        ($id_default_shipping === $address->id) ? 2 : (($id_default_billing === $address->id) ? 1 : 0), // 0 -> none, 1 -> billing, 2 -> shipping
                        $address->id);
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'erp_orders' => $erp_orders,
                            'mg2_orders' => $api_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync new customers orders
            foreach ($pending_orders as $email_pass => $pending_order) {
                $email_pass = explode(':', $email_pass);
                $email = $email_pass[0];

                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE email LIKE '".$email."'";
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Get customer addresses
                if ($sync_customer) {
                    $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = ".$sync_customer['id'];
                    $customer_addresses = $db->fetchAll($query_customer_addresses);
                    $customer_billing_address_id = 0;
                    $customer_shipping_address_id = 0;
                    foreach ($customer_addresses as $customer_address) {
                        $defecto = intval($customer_address['defecto']);
                        if ($defecto === 1) {
                            $customer_billing_address_id = $customer_address['id'];
                        }

                        if ($defecto === 2) {
                            $customer_shipping_address_id = $customer_address['id'];
                        }

                        if ($defecto === 3) {
                            $customer_billing_address_id = $customer_address['id'];
                            $customer_shipping_address_id = $customer_address['id'];
                        }
                    }

                    $ddl_query .= "INSERT INTO sync_pedidos (xcliente_id, email, sync_cliente_id, sync_direccion_facturacion_id, sync_direccion_envio_id, metodo_pago, metodo_envio, estado, gastos_envio, subtotal, subtotal_base, magento_pedido_id, magento_orderid, comentarios, fecha, comentarios_pedido, dni, recargoPaypal) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

                    // Get comments
                    $comments = "";
                    foreach ($pending_order->status_histories as $status_history) {
                        $comments .= $status_history->comment." | ";
                    }

                    array_push($params,
                        '',                         // Not known at this moment
                        $pending_order->customer_email,
                        $sync_customer['id'],
                        $customer_billing_address_id,
                        $customer_shipping_address_id,
                        $pending_order->payment->method,
                        $pending_order->shipping_description,
                        $pending_order->status,
                        $pending_order->shipping_incl_tax,
                        $pending_order->subtotal_incl_tax,
                        $pending_order->base_subtotal,
                        $pending_order->entity_id,
                        $pending_order->increment_id,
                        $comments,
                        $pending_order->created_at,
                        $comments,
                        "",                             // Not known at this moment
                        0
                    );
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'erp_orders' => $erp_orders,
                            'mg2_orders' => $api_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync order lines
            foreach ($new_orders as $new_order) {
                $order_items = $new_order->items;

                // Get order from ERP database
                $query_order = "SELECT * FROM sync_pedidos WHERE magento_pedido_id = ".$new_order->entity_id;
                $order = $db->fetchAssoc($query_order);

                foreach ($order_items as $item) {
                    $ddl_query .= "INSERT INTO sync_lineas (magento_producto_id, sync_pedido_id, cantidad, sku, precio, descuento) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        $item->item_id,
                        $order['id'],
                        $item->qty_ordered,
                        $item->sku,
                        $item->price,
                        $item->discount_percent);
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                    $ddl_query = "";
                    $params = [];
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'erp_orders' => $erp_orders,
                            'mg2_orders' => $api_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            // Sync pending customers into sync_operations
            foreach ($pending_registered_customers as $pending_registered_customer) {
                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = " . $pending_registered_customer->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Get customer addresses
                $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = " . $sync_customer['id'];
                $sync_addresses = $db->fetchAll($query_customer_addresses);

                // Insert the new operations into sync_operations
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_clientes',
                    'id',
                    $sync_customer['id'],
                    1,                  // New
                    0,
                    0);

                // New addresses in sync_operations
                foreach ($sync_addresses as $sync_address) {
                    // Insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_direccions',
                        'id',
                        $sync_address['id'],
                        1,                  // New
                        0,
                        0);
                }
            }

            // Sync new customers into sync_operations
            foreach ($fulfilled_responses as $email => $result) {
                // Get customer synchronized
                $query_sync_customer = "SELECT * FROM sync_clientes WHERE magento_cliente_id = " . $result->id;
                $sync_customer = $db->fetchAssoc($query_sync_customer);

                // Get customer addresses
                $query_customer_addresses = "SELECT * FROM sync_direccions WHERE sync_cliente_id = " . $sync_customer['id'];
                $sync_addresses = $db->fetchAll($query_customer_addresses);

                // Insert the new operations into sync_operations
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_clientes',
                    'id',
                    $sync_customer['id'],
                    1,                  // New
                    0,
                    0);

                // New addresses in sync_operations
                foreach ($sync_addresses as $sync_address) {
                    // Insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_direccions',
                        'id',
                        $sync_address['id'],
                        1,                  // New
                        0,
                        0);
                }
            }

            // Sync new addresses of existing customers into sync_operations
            foreach ($new_customer_addresses as $key => $new_customer_address) {
                // Get customer addresses
                $query_customer_addresses = "SELECT * FROM sync_direccions WHERE magento_direccion_id = " . $new_customer_address->entity_id;
                $sync_address = $db->fetchAssoc($query_customer_addresses);

                // Insert the new operations into sync_operations
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_direccions',
                    'id',
                    $sync_address['id'],
                    1,                  // New
                    0,
                    0);
            }

            // Sync orders and lines into sync_operations
            foreach ($new_orders as $new_order) {
                // Get order from ERP database
                $query_order = "SELECT * FROM sync_pedidos WHERE magento_pedido_id = ".$new_order->entity_id;
                $order = $db->fetchAssoc($query_order);

                // At the same time we insert the new operations into sync_operations
                $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                array_push($params,
                    'sync_pedidos',
                    'id',
                    $order['id'],
                    1,                  // New
                    0,
                    0);

                // Get lines from ERP database
                $query_lines = "SELECT * FROM sync_lineas WHERE sync_pedido_id = ".$order['id'];
                $lines = $db->fetchAll($query_lines);

                foreach ($lines as $line) {
                    // At the same time we insert the new operations into sync_operations
                    $ddl_query .= "INSERT INTO sync_operations (tabla, campo, valor, operacion, prioridad, estado) VALUES (?, ?, ?, ?, ?, ?);";
                    array_push($params,
                        'sync_lineas',
                        'id',
                        $line['id'],
                        1,                  // New
                        0,
                        0);
                }
            }

            // Execute SQL
            if ($ddl_query !== "") {
                try {
                    $db->executeQuery($ddl_query, $params);
                } catch (DBALException $exception) {
                    $response->setData(array(
                            'success' => false,
                            'erp_orders' => $erp_orders,
                            'mg2_orders' => $api_orders,
                            'message' => $exception->getMessage())
                    );

                    return $response;
                }
            }

            $response->setData(array(
                    'success' => true,
                    'erp_orders' => $erp_orders,
                    'mg2_orders' => $api_orders,
                    'message' => 'Orders synchronized successfully')
            );
        }

        return $response;
    }
}
