<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Shared\Shared;
use AppBundle\Entity\Status;

class ApiAccessController extends Controller
{
    /**
     * @param Controller $context
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return mixed
     */
    public static function refreshToken(Controller $context)
    {
        $em = $context->getDoctrine()->getManager();
        // Get params
        $mg2_api_uri = $context->getParameter('uri_mg2') . '/index.php/rest/V1/';
        $mg2_username = $context->getParameter('username_mg2');
        $mg2_password = $context->getParameter('password_mg2');
        
        // Send request
        $api_response = Shared::getToken($mg2_api_uri, $mg2_username, $mg2_password);
        $status_code = $api_response->getStatusCode();

        // Return token 
        if ($status_code === 200) {
            $token = json_decode($api_response->getBody()->getContents());
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
            if (!$status) {
                $status = new Status();
                $status->setToken($token);
            } else {
                $status->setToken($token);
                $expiration = new \DateTime('now');
                $expiration->modify('+5 minutes');
                $status->setTokenExpiration($expiration);
            }
        } else {
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
            if (!$status) {
                $status = new Status();
                $status->setToken('');
            } else {
                $status->setToken('');
                $expiration = new \DateTime('now');
                $status->setTokenExpiration($expiration);
            }
        }

        $em->persist($status);
        $em->flush();

        return $status_code;
    }

    /**
     * @param Controller $context
     * @return mixed
     */
    public static function getERP(Controller $context)
    {
        $erp = $context->getParameter('erp_int');
        return $erp ? $erp : false;
    }

    /**
     * @Route("/auth-token")
     */
    public function authTokenAction()
    {
        $response = new JsonResponse();
        $status_code = $this->refreshToken($this);

        if ($status_code == 200) {
            $em = $this->getDoctrine()->getManager();
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array());
            $response->setData(array(
                'success'    => true,
                'token'      => $status->getToken(),
                'expires'    => $status->getTokenExpiration(),
                'message'    => 'Auth token successfully obtained')
            );
        } else {
            $response->setData(array(
                'success'    => false,
                'token'      => false,
                'expires'    => false,
                'message'    => 'Error obtaining token')
            );
        }

        return $response;
    }

}
