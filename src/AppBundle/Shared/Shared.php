<?php
// src/AppBundle/Shared/Shared.php
namespace AppBundle\Shared;

use GuzzleHttp\Client;
 
class Shared
{
    /**
     * @param $uri
     * @param $username
     * @param $password
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    static public function getToken($uri, $username, $password)
    {
        $client = new Client(['base_uri' => $uri]);
        $response = $client->request('POST', 'integration/admin/token', [
            'json' => [
                'username' => $username,
                'password' => $password
            ]
        ]);
        
        return $response;
    }

    /**
     * @param $limit
     * @return bool|string
     */
    static public function randomCode($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }

    /**
     * @param $options
     * @param $code
     * @return bool|string
     */
    static public function getAttributeOptionsValue($options, $code)
    {
        foreach ($options as $option) {
            if (gettype($option) === 'object') {
                if (
                    isset($option->attribute_code) &&
                    mb_strtolower($option->attribute_code) === mb_strtolower($code)
                ) {
                    return $option->value;
                }
            } elseif (gettype($option) === 'array') {
                // $option has attribute_code field
                if (
                    isset($option['attribute_code']) &&
                    mb_strtolower($option['attribute_code']) === mb_strtolower($code)
                ) {
                    return $option['value'];
                }

                // $option has label field
                if (
                    isset($option['label']) &&
                    mb_strtolower($option['label']) === mb_strtolower($code)
                ) {
                    return $option['value'];
                }
            }
        }

        return false;
    }

    /**
     * @param $options
     * @param $code
     * @return bool|string
     */
    static public function getAttributeValueLabel($options, $code)
    {
        foreach ($options as $option) {
            if (gettype($option) === 'object') {
                if (
                    isset($option->attribute_code) &&
                    mb_strtolower($option->attribute_code) === mb_strtolower($code)
                ) {
                    return $option->label;
                }
            } elseif (gettype($option) === 'array') {
                if (
                    isset($option['value']) &&
                    mb_strtolower($option['value']) === mb_strtolower($code)
                ) {
                    return $option['label'];
                }
            }
        }

        return false;
    }

    /**
     * @param $products
     * @param $sku
     * @return bool|\stdClass
     */
    static public function getProductBySku($products, $sku)
    {
        foreach ($products as $product) {
            if ($product['sku'] === $sku) {
                return $product;
            }
        }

        return false;
    }

    /**
     * @param $uri
     * @param $token
     * @param $sku
     * @return bool|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    static public function getAPIProductBySku($uri, $token, $sku)
    {
        $client = new Client(['base_uri' => $uri]);
        $response = $client->request('GET', 'products/'.$sku, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents());
        } else {
            return false;
        }
    }

    /**
     * @param $products
     * @param $sku
     * @return array
     */
    static public function getChildrenProductsBySku($products, $sku) {
        $output = [];
        foreach ($products as $product) {
            if (
                isset($product->sku) &&
                $product->sku !== $sku &&
                strpos($product->sku, $sku) !== false
            ) {
                $output[] = $product;
            }
        }

        return $output;
    }

    /**
     * @param $value
     * @param string $new_value
     */
    static public function fixNullAttributeValue(&$value, $new_value = '-')
    {
        if (
            $value === null ||
            trim($value) === 'xdescripcion' ||
            trim($value) === '.' ||
            trim($value) === ''
        ) {
            $value = $new_value;
        }
    }

    /**
     * @param $string
     * @param string $delimiter
     * @return null|string|string[]
     */
    static public function getSlug($string, $delimiter = '-')
    {
        $slug = iconv('UTF-8', 'ASCII//IGNORE', $string);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, $delimiter));
        $slug = preg_replace("/[\/_|+ -]+/", $delimiter, $slug);

        return $slug;
    }

    /**
     * @param $api_links
     * @param $sku
     * @return bool
     */
    static public function productLinkExists($api_links, $sku)
    {
        foreach ($api_links as $link) {
            if ($link['sku'] === $sku) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $db
     * @param $erp_id
     * @return array
     */
    static public function getProductCategories($db, $erp_id)
    {
        $query_categories = "SELECT sc.magento_categoria_id AS mag_id 
                                        FROM sync_categorias sc 
                                        JOIN mag_art_categorias mac ON sc.xcategoria_id = mac.xcategoria_id 
                                        WHERE mac.xarticulo_id = '".$erp_id."';";
        $product_categories = $db->fetchAll($query_categories);
        $category_ids_array = [];
        foreach ($product_categories as $product_category) {
            $category_ids_array[] = $product_category['mag_id'];
        }
        return $category_ids_array;
    }

    /**
     * @param $address
     * @param $id_default_shipping
     * @param $id_default_billing
     * @return int
     */
    static public function getAddressType($address, $id_default_shipping, $id_default_billing)
    {
        if (gettype($address) === 'array') {
            if ($address['entity_id'] === $id_default_shipping) {
                if ($address['entity_id'] === $id_default_billing) {
                    return 3;
                } else {
                    return 1;
                }
            } elseif ($address['entity_id'] === $id_default_billing) {
                if ($address['entity_id'] === $id_default_shipping) {
                    return 3;
                } else {
                    return 2;
                }
            } else {
                return 0;
            }
        }

        if (gettype($address) === 'object') {
            if ($address->id === $id_default_shipping) {
                if ($address->id === $id_default_billing) {
                    return 3;
                } else {
                    return 1;
                }
            } elseif ($address->id === $id_default_billing) {
                if ($address->id === $id_default_shipping) {
                    return 3;
                } else {
                    return 2;
                }
            } else {
                return 0;
            }
        }
    }


    /**
     * @param $erp_product
     * @param $options
     * @return array
     */
    static public function getProductGenderIds($options, $erp_product) {
        $gender_ids = [];
        if ($erp_product['hombre'] === '-1') {
            foreach ($options as $option) {
                if ($option['label'] === 'Hombre') {
                    $gender_ids[] = $option['value'];
                }
            }
        }
        if ($erp_product['mujer'] === '-1') {
            foreach ($options as $option) {
                if ($option['label'] === 'Mujer') {
                    $gender_ids[] = $option['value'];
                }
            }
        }
        if ($erp_product['unisex'] === '-1') {
            foreach ($options as $option) {
                if ($option['label'] === 'Unisex') {
                    $gender_ids[] = $option['value'];
                }
            }
        }
        return $gender_ids;
    }
}